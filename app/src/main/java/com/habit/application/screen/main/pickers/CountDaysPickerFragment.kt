package com.habit.application.screen.main.pickers

import android.os.Bundle
import android.view.*
import androidx.fragment.app.DialogFragment
import com.habit.application.R
import kotlinx.android.synthetic.main.bs_count_days_picker.*


/**
 * Created by Roman K. on 17.05.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
private const val PICKER_COUNT_DAYS = "picker_count_days"

private const val MAX_COUNT_DAYS = 100
private const val DEFAULT_COUNT_DAY = 21

class CountDaysPickerFragment : DialogFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.bs_count_days_picker, container, false)
    }

    private var dialogListener: Listener? = null

    companion object {
        fun instance(listener: Listener) = CountDaysPickerFragment().apply {
            dialogListener = listener
        }
    }

    data class Builder(
        private var countDays: Int = DEFAULT_COUNT_DAY,
        private var listener: Listener? = null
    ) {
        fun countDays(countDays: Int) = apply { this.countDays = countDays }
        fun setListener(listener: Listener) = apply { this.listener = listener }
        fun build() = listener?.let {
            instance(it).apply {
                arguments = Bundle(1).apply {
                    putInt(PICKER_COUNT_DAYS, countDays)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.TimePickerDialogTheme)
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.attributes?.windowAnimations = R.style.TimePickerDialogNoAnimation
    }


    override fun onStart() {
        super.onStart()
        configBG()
        initPicker()
        setListeners()
    }

    private fun initPicker() {
        bsCountDaysPickerView.apply {
            displayedValues(generateValues())
            minValue(1)
            maxValue(MAX_COUNT_DAYS)
        }

        bsCountDaysPickerView.postDelayed({
            val startCount = arguments?.getInt(PICKER_COUNT_DAYS)
            bsCountDaysPickerView.smoothScrollToValue(0, startCount ?: DEFAULT_COUNT_DAY)
        }, resources.getInteger(android.R.integer.config_shortAnimTime).toLong())
    }

    private fun generateValues(): Array<String?> {
        return Array(MAX_COUNT_DAYS) { i: Int -> (i + 1).toString() }
    }


    private fun configBG() {
        val window = dialog?.window
        val wlp = window?.attributes
        window?.setBackgroundDrawableResource(android.R.color.transparent)
        wlp?.gravity = Gravity.BOTTOM
        window?.attributes = wlp
    }

    interface Listener {
        fun setCountDays(countDays: Int)
    }

    private fun setListeners() {
        bsCountDaysPickerNegativeBtn.setOnClickListener {
            dialog?.dismiss()
        }
        bsCountDaysPickerPositiveBtn.setOnClickListener {
            dialogListener?.setCountDays(bsCountDaysPickerView.value())
            dialog?.dismiss()
        }
        bsCountDaysPickerView.setOnValueChangeListenerInScrolling { picker, _, _ ->
            picker?.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
        }
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        dialog?.window?.attributes?.windowAnimations = R.style.TimePickerDialogAnimation
    }
}