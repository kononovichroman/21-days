package com.habit.application.screen.base

import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.fragment.app.Fragment
import com.gyf.immersionbar.ktx.statusBarHeight


/**
 * Created by Roman K. on 25.01.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
abstract class BaseFragment : Fragment() {
    fun setTopMarginStatusBar(cl: ConstraintLayout, view: View) {
        val set = ConstraintSet()
        set.clone(cl)
        set.setMargin(view.id, ConstraintSet.TOP, statusBarHeight)
        set.applyTo(cl)
    }

    open fun onBackPressed() {
        activity?.onBackPressed()
    }

    var isBackPressNeed = true

}