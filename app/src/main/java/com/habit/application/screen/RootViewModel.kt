package com.habit.application.screen

import android.app.Application
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.AndroidViewModel
import com.habit.application.core.managers.analytics.AnalyticsManagerAPI
import com.habit.application.core.managers.update.InAppUpdateAPI
import com.habit.application.core.settings.SettingsManagerAPI


/**
 * Created by Roman K. on 09.02.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
class RootViewModel(
    private val context: Application,
    private val settingsManager: SettingsManagerAPI,
    private val router: RouterAPI,
    private val inAppUpdate: InAppUpdateAPI,
    private val analyticsManager: AnalyticsManagerAPI
    ) : AndroidViewModel(context) {
    private val isVisibleScreen = ObservableBoolean()
    private val isNeedShowUpdateDialog = ObservableBoolean()

    init {
        inAppUpdate.run {
            successfullyDownloaded = {
                if (isVisibleScreen.get()) {
                    router.showDialogUpdateDownloaded()
                } else {
                    isNeedShowUpdateDialog.set(true)
                }
            }
        }
        router.run {
            clickInstallUpdate = {
                inAppUpdate.installUpdate()
                analyticsManager.logEvent(AnalyticsManagerAPI.AnalyticsEvent.CLICK_UPDATE_IN_APP)
            }
        }
    }

    fun start() {
        if (settingsManager.countLaunch() == 0) {
            settingsManager.firstLaunchMeta()
        }
        settingsManager.incrementCountLaunch()
        if (settingsManager.isOnboardingShown()) {
            router.startMainScreen()
        } else {
            router.startOnboarding()
        }
        inAppUpdate.checkUpdate()
    }

    fun onResume() {
        if (isNeedShowUpdateDialog.get()) {
            router.showDialogUpdateDownloaded()
            isNeedShowUpdateDialog.set(false)
        }
        isVisibleScreen.set(true)
    }

    fun onPause() {
        isVisibleScreen.set(false)
    }
}