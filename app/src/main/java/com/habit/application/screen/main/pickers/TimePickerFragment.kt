package com.habit.application.screen.main.pickers

import android.os.Bundle
import android.text.format.DateFormat
import android.view.*
import androidx.fragment.app.DialogFragment
import com.habit.application.R
import com.habit.application.core.managers.analytics.AnalyticsManagerAPI
import kotlinx.android.synthetic.main.bs_time_picker.*
import org.koin.android.ext.android.inject
import java.util.*
import kotlin.math.ceil


/**
 * Created by Roman K. on 01.05.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
const val TIME_PICKER_24 = ""
private const val SUNRISE_HOUR = 8
private const val SOLAR_NOON_HOUR = 14
private const val SUNSET_HOUR = 20
class TimePickerFragment : DialogFragment() {
    private val analyticsManager: AnalyticsManagerAPI by inject()

    private var is24HourFormat = false
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.bs_time_picker, container, false)
    }

    private var dialogListener: Listener? = null

    companion object {
        fun instance(listener: Listener) = TimePickerFragment().apply {
            dialogListener = listener
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.TimePickerDialogTheme)
    }

    override fun onResume() {
        super.onResume()
        dialog?.window?.attributes?.windowAnimations = R.style.TimePickerDialogNoAnimation
    }


    override fun onStart() {
        super.onStart()
        configBG()
        initPickers()
        initPresets()
        setListeners()
    }

    private fun initPresets() {
        if (is24HourFormat){
            bsTimePickerTVSunrise.text = resources.getString(R.string.time_picker_sunrise_time)
            bsTimePickerTVSolarNoon.text = resources.getString(R.string.time_picker_solar_noon_time)
            bsTimePickerTVSunset.text = resources.getString(R.string.time_picker_sunset_time)
        } else {
            bsTimePickerTVSunrise.text = resources.getString(R.string.time_picker_sunrise_time_ampm)
            bsTimePickerTVSolarNoon.text = resources.getString(R.string.time_picker_solar_noon_time_ampm)
            bsTimePickerTVSunset.text = resources.getString(R.string.time_picker_sunset_time_ampm)
        }
    }

    private fun configBG() {
        val window = dialog?.window
        val wlp = window?.attributes
        window?.setBackgroundDrawableResource(android.R.color.transparent)
        wlp?.gravity = Gravity.BOTTOM
        window?.attributes = wlp
    }

    private fun initPickers() {
        if (bsTimePickerHours.displayedValues()?.size ?: 0 < 2 ) {
            is24HourFormat = DateFormat.is24HourFormat(requireContext())
            if (is24HourFormat) {
                bsTimePickerAMPM.visibility = View.GONE

                bsTimePickerHours.apply {
                    displayedValues(resources.getStringArray(R.array.time_picker_hours))
                    minValue(0)
                    maxValue(resources.getStringArray(R.array.time_picker_hours).size - 1)
                }

                bsTimePickerMinutes.apply {
                    displayedValues(resources.getStringArray(R.array.time_picker_minutes))
                    minValue(0)
                    maxValue(resources.getStringArray(R.array.time_picker_minutes).size - 1)
                }
                bsTimePickerAMPM.apply {
                    displayedValues(resources.getStringArray(R.array.time_picker_am_pm))
                    minValue(0)
                    maxValue(resources.getStringArray(R.array.time_picker_am_pm).size - 1)
                }
                smoothScrollPickers()
            } else {
                bsTimePickerAMPM.visibility = View.VISIBLE
                bsTimePickerHours.apply {
                    displayedValues(resources.getStringArray(R.array.time_picker_hours_am_pm))
                    minValue(0)
                    maxValue(resources.getStringArray(R.array.time_picker_hours_am_pm).size - 1)
                }

                bsTimePickerMinutes.apply {
                    displayedValues(resources.getStringArray(R.array.time_picker_minutes))
                    minValue(0)
                    maxValue(resources.getStringArray(R.array.time_picker_minutes).size - 1)
                }
                bsTimePickerAMPM.apply {
                    displayedValues(resources.getStringArray(R.array.time_picker_am_pm))
                    minValue(0)
                    maxValue(resources.getStringArray(R.array.time_picker_am_pm).size - 1)
                }
                smoothScrollPickersAMPM()

            }

            bsTimePickerHours.setOnValueChangeListenerInScrolling { picker, _, _ ->
                picker?.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
            }
            bsTimePickerMinutes.setOnValueChangeListenerInScrolling { picker, _, _ ->
                picker?.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
            }
            bsTimePickerAMPM.setOnValueChangeListenerInScrolling { picker, _, _ ->
                picker?.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
            }

        }
    }


    private fun smoothScrollPickersAMPM() {
        val calendar = GregorianCalendar.getInstance()
        val ampm = calendar.get(Calendar.AM_PM)
        val hour = calendar.get(Calendar.HOUR)
        val minute = calendar.get(Calendar.MINUTE)
        if (minute <= 55) {
            bsTimePickerMinutes.postDelayed({
                bsTimePickerMinutes.smoothScrollToValue(0, ceil(minute / 5.00).toInt())
            }, resources.getInteger(android.R.integer.config_shortAnimTime).toLong())
            bsTimePickerHours.postDelayed({
                bsTimePickerHours.smoothScrollToValue(0, hour)
            }, resources.getInteger(android.R.integer.config_shortAnimTime).toLong())
            bsTimePickerAMPM.postDelayed({
                bsTimePickerAMPM.smoothScrollToValue(0, ampm)
            }, resources.getInteger(android.R.integer.config_shortAnimTime).toLong())
        } else {
            bsTimePickerMinutes.postDelayed({
                bsTimePickerMinutes.smoothScrollToValue(0, 0)
            }, resources.getInteger(android.R.integer.config_shortAnimTime).toLong())
            if (hour != 11) {
                bsTimePickerHours.postDelayed({
                    bsTimePickerHours.smoothScrollToValue(0, hour + 1)
                }, resources.getInteger(android.R.integer.config_shortAnimTime).toLong())
                bsTimePickerAMPM.postDelayed({
                    bsTimePickerAMPM.smoothScrollToValue(0, ampm)
                }, resources.getInteger(android.R.integer.config_shortAnimTime).toLong())
            } else {
                if (ampm == Calendar.AM) {
                    bsTimePickerAMPM.postDelayed({
                        bsTimePickerAMPM.smoothScrollToValue(0, Calendar.PM)
                    }, resources.getInteger(android.R.integer.config_shortAnimTime).toLong())
                } else {
                    bsTimePickerAMPM.postDelayed({
                        bsTimePickerAMPM.smoothScrollToValue(0, Calendar.AM)
                    }, resources.getInteger(android.R.integer.config_shortAnimTime).toLong())
                }
                bsTimePickerHours.postDelayed({
                    bsTimePickerHours.smoothScrollToValue(0, hour + 1)
                }, resources.getInteger(android.R.integer.config_shortAnimTime).toLong())
            }


        }
    }

    private fun smoothScrollPickers() {
        val calendar = GregorianCalendar.getInstance()
        val hour = calendar.get(Calendar.HOUR_OF_DAY)
        val minute = calendar.get(Calendar.MINUTE)
        if (minute <= 55) {
            bsTimePickerMinutes.postDelayed({
                bsTimePickerMinutes.smoothScrollToValue(0, ceil(minute / 5.00).toInt())
            }, resources.getInteger(android.R.integer.config_shortAnimTime).toLong())
            bsTimePickerHours.postDelayed({
                bsTimePickerHours.smoothScrollToValue(0, hour)
            }, resources.getInteger(android.R.integer.config_shortAnimTime).toLong())
        } else {
            bsTimePickerMinutes.postDelayed({
                bsTimePickerMinutes.smoothScrollToValue(0, 0)
            }, resources.getInteger(android.R.integer.config_shortAnimTime).toLong())
            if (hour < 23) {
                bsTimePickerHours.postDelayed({
                    bsTimePickerHours.smoothScrollToValue(0, hour + 1)
                }, resources.getInteger(android.R.integer.config_shortAnimTime).toLong())
            } else {
                bsTimePickerHours.postDelayed({
                    bsTimePickerHours.smoothScrollToValue(0, 0)
                }, resources.getInteger(android.R.integer.config_shortAnimTime).toLong())
            }

        }

    }

    interface Listener {
        fun setTime(hour: Int, minute: Int, ampm: String)
    }

    private fun setListeners() {
        bsTimePickerNegativeBtn.setOnClickListener {
            dialog?.dismiss()
        }
        bsTimePickerPositiveBtn.setOnClickListener {
            val amPm = if (bsTimePickerAMPM.visibility == View.GONE) {
                TIME_PICKER_24
            } else {
                resources.getStringArray(R.array.time_picker_am_pm)[bsTimePickerAMPM.value()]
            }
            dialogListener?.setTime(
                if (is24HourFormat) {
                    resources.getStringArray(R.array.time_picker_hours)[bsTimePickerHours.value()].toInt()
                } else {
                    resources.getStringArray(R.array.time_picker_hours_am_pm)[bsTimePickerHours.value()].toInt()
                },
                resources.getStringArray(R.array.time_picker_minutes)[bsTimePickerMinutes.value()].toInt(),
                amPm
            )
            dialog?.dismiss()
        }
        bsTimePickerBlockSunrise.setOnClickListener {
            analyticsManager.logEvent(AnalyticsManagerAPI.AnalyticsEvent.CLICK_PRESET_SUNRISE)
            smoothPreset(SUNRISE_HOUR)
        }
        bsTimePickerBlockSolarNoon.setOnClickListener {
            analyticsManager.logEvent(AnalyticsManagerAPI.AnalyticsEvent.CLICK_PRESET_SOLAR_NOON)
            smoothPreset(SOLAR_NOON_HOUR)
        }
        bsTimePickerBlockSunset.setOnClickListener {
            analyticsManager.logEvent(AnalyticsManagerAPI.AnalyticsEvent.CLICK_PRESET_SUNSET)
            smoothPreset(SUNSET_HOUR)
        }

    }


    private fun smoothPreset(typePreset: Int){
        if (!is24HourFormat) {
            when(typePreset) {
                SUNRISE_HOUR -> {
                    bsTimePickerAMPM.post {
                        bsTimePickerAMPM.smoothScrollToValue(bsTimePickerAMPM.value(), Calendar.AM)
                    }
                }
                SOLAR_NOON_HOUR -> {
                    bsTimePickerAMPM.post {
                        bsTimePickerAMPM.smoothScrollToValue(bsTimePickerAMPM.value(), Calendar.PM)
                    }
                }
                SUNSET_HOUR -> {
                    bsTimePickerAMPM.post {
                        bsTimePickerAMPM.smoothScrollToValue(bsTimePickerAMPM.value(), Calendar.PM)
                    }
                }
            }
        }

        bsTimePickerHours.post {
            bsTimePickerHours.smoothScrollToValue(bsTimePickerHours.value(), typePreset)
        }
        bsTimePickerMinutes.post {
            bsTimePickerMinutes.smoothScrollToValue(bsTimePickerMinutes.value(), 0)
        }
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        dialog?.window?.attributes?.windowAnimations = R.style.TimePickerDialogAnimation
    }
}