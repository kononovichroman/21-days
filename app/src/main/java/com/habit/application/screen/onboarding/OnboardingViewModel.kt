package com.habit.application.screen.onboarding

import android.app.Application
import androidx.databinding.ObservableField
import androidx.lifecycle.AndroidViewModel
import com.habit.application.core.helper.ThemeHelperAPI
import com.habit.application.core.managers.analytics.AnalyticsManagerAPI
import com.habit.application.core.settings.SettingsManagerAPI
import com.habit.application.screen.RouterAPI
import com.habit.application.screen.onboarding.view.OnboardingContent
import com.habit.application.screen.onboarding.view.OnboardingPageView


/**
 * Created by Roman K. on 18.02.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
class OnboardingViewModel(private val context: Application,
                          private val router: RouterAPI,
                          private val themeHelper: ThemeHelperAPI,
                          private val settingsManager: SettingsManagerAPI,
                          private val analyticsManager: AnalyticsManagerAPI
) : AndroidViewModel(context) {
    val data = ObservableField<List<OnboardingPageView>>()

    init {
        themeHelper.themeMain()
    }

    fun start() {
        data.set(
            OnboardingContent
                .values()
                .map { onboardingPageData ->
                    val pageView = OnboardingPageView(context)
                    pageView.setPageData(onboardingPageData)
                    pageView
                }
        )
        router.getScreenForAnalytics().second?.let {
            analyticsManager.currentScreen(router.getScreenForAnalytics().first,
                it
            )
        }
    }

    fun changePage(page: Int){
        when(page){
            1 -> {
                themeHelper.whiteBars()
            }
            2 -> {
                themeHelper.whiteBars()
            }
            3 -> {
               themeHelper.themeMain()
            }
        }
    }
    fun clickStart(){
        settingsManager.setMarkOnboardingShow()
        router.startMainScreen()
    }
}