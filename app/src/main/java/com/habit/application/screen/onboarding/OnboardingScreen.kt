package com.habit.application.screen.onboarding

import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.text.HtmlCompat
import androidx.core.view.ViewCompat
import androidx.databinding.DataBindingUtil
import com.habit.application.App
import com.habit.application.R
import com.habit.application.databinding.OnboardingScreenBinding
import com.habit.application.screen.base.BaseFragment
import com.habit.application.screen.onboarding.view.OnboardingAdapter
import com.habit.application.screen.onboarding.view.OnboardingView
import kotlinx.android.synthetic.main.onboarding_screen.*
import kotlinx.android.synthetic.main.onboarding_view.*
import kotlinx.android.synthetic.main.onboarding_view.view.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf


/**
 * Created by Roman K. on 13.02.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
class OnboardingScreen : BaseFragment() {
    private val viewModel: OnboardingViewModel by viewModel { parametersOf(requireActivity(), this) }
    private val onboardingAdapter = OnboardingAdapter()
    private lateinit var bind: OnboardingScreenBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        bind = DataBindingUtil.inflate(inflater, R.layout.onboarding_screen, container, false)
        setBarsMargin()
        return bind.root
    }

    private fun setBarsMargin() {
        if (App.navbarHeight >= 0) {
            setNavBarSize(
                App.navbarHeight
            )
        }
        ViewCompat.setOnApplyWindowInsetsListener(bind.root) { _, windowInsets ->
            if (App.navbarHeight < 0 || App.navbarHeight != windowInsets.systemWindowInsetBottom) {
                App.navbarHeight = windowInsets.systemWindowInsetBottom
                setNavBarSize(
                    App.navbarHeight
                )
            }
            windowInsets
        }


    }


    private fun setNavBarSize(navbarHeight: Int) {
        bind.onboardingScreenView.onboardingViewRoot.getConstraintSet(R.id.startOnboarding)
            .let { set ->
                set.setMargin(
                    R.id.onboardingViewStartBtn,
                    ConstraintSet.TOP,
                    set.getHeight(R.id.onboardingViewStartBtn) * 2 + navbarHeight
                )
                set.setMargin(
                    R.id.onboardingViewNavBarGuideline,
                    ConstraintSet.BOTTOM,
                    navbarHeight
                )
                set.applyTo(bind.onboardingScreenView.onboardingViewRoot)
            }

        bind.onboardingScreenView.onboardingViewRoot.getConstraintSet(R.id.endOnboarding)
            .let { set ->
                set.constrainHeight(
                    R.id.onboardingViewStartBtn,
                    set.getHeight(R.id.onboardingViewStartBtn) + navbarHeight
                )
                set.setMargin(
                    R.id.onboardingViewNavBarGuideline,
                    ConstraintSet.BOTTOM,
                    navbarHeight
                )
                set.applyTo(bind.onboardingScreenView.onboardingViewRoot)
            }


    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onStart() {
        super.onStart()
        initViewModel()
        initOnBoardingView()
        setListeners()
        configDescriptionPrivacy()
    }
    private fun configDescriptionPrivacy() {
        val html = HtmlCompat.fromHtml(
            resources.getString(R.string.onboarding_privacy),
            HtmlCompat.FROM_HTML_MODE_LEGACY
        )
        bind.onboardingScreenView.onboardingTVPrivacy.text = html
        bind.onboardingScreenView.onboardingTVPrivacy.movementMethod = LinkMovementMethod.getInstance()
    }

    private fun setListeners() {
        onboardingScreenView.setOnListeners(object : OnboardingView.Listener {
            override fun currentPosition(position: Int) {
                viewModel.changePage(position)
            }

        })
        onboardingViewStartBtn.setOnClickListener {
            viewModel.clickStart()
        }
    }

    private fun initViewModel() {
        viewModel.start()
    }


    private fun initOnBoardingView() {
        viewModel.data.get()?.let { onboardingAdapter.setData(it) }
        onboardingScreenView.setAdapter(onboardingAdapter)
    }
}