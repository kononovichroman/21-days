package com.habit.application.screen.main.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Rect
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.view.GestureDetector
import android.view.GestureDetector.SimpleOnGestureListener
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import androidx.core.view.GestureDetectorCompat
import androidx.core.view.ViewCompat
import androidx.customview.widget.ViewDragHelper
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min


/**
 * Created by Roman K. on 07.02.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
class SwipeRevealLayout : ViewGroup {

    private lateinit var mainView: View
    private lateinit var secondaryView: View
    private val rectMainClose = Rect()
    private val rectMainOpen = Rect()
    private val rectSecClose = Rect()
    private val rectSecOpen = Rect()
    private var minDistRequestDisallowParent = 0
    private var isOpenBeforeInit = false
    @Volatile
    private var aborted = false
    @Volatile
    private var isScrolling = false
    @Volatile
    private var lockDrag = false
    private var minFlingVelocity = DEFAULT_MIN_FLING_VELOCITY
    private var mState = STATE_CLOSE
    private var mode = MODE_SAME_LEVEL
    private var lastMainLeft = 0
    private var lastMainTop = 0
    private var dragEdge = DRAG_EDGE_RIGHT
    private var dragDist = 0f
    private var prevX = -1f
    private var prevY = -1f
    private var dragHelper: ViewDragHelper? = null
    private var gestureDetector: GestureDetectorCompat? = null
    private var dragStateChangeListener // only used for ViewBindHelper
            : DragStateChangeListener? = null
    private var onLayoutCount = 0

    interface DragStateChangeListener {
        fun onDragStateChanged(state: Int)
    }

    constructor(context: Context?) : super(context) {
        init(context)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(
        context,
        attrs
    ) {
        init(context)
    }

    constructor(
        context: Context?,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr) {
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        gestureDetector?.onTouchEvent(event)
        event?.let { dragHelper?.processTouchEvent(it) }
        return true
    }

    override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
        if (isDragLocked()) {
            return super.onInterceptTouchEvent(ev)
        }
        dragHelper?.processTouchEvent(ev)
        gestureDetector?.onTouchEvent(ev)
        accumulateDragDist(ev)
        val couldBecomeClick = couldBecomeClick(ev)
        val settling =
            dragHelper?.viewDragState == ViewDragHelper.STATE_SETTLING
        val idleAfterScrolled =
            (dragHelper?.viewDragState == ViewDragHelper.STATE_IDLE
                    && isScrolling)
        // must be placed as the last statement
        prevX = ev.x
        prevY = ev.y
        // return true => intercept, cannot trigger onClick event
        return !couldBecomeClick && (settling || idleAfterScrolled)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        // get views
        if (childCount >= 2) {
            secondaryView = getChildAt(0)
            mainView = getChildAt(1)
        } else if (childCount == 1) {
            mainView = getChildAt(0)
        }
    }

    /**
     * {@inheritDoc}
     */
    override fun onLayout(
        changed: Boolean,
        l: Int,
        t: Int,
        r: Int,
        b: Int
    ) {
        aborted = false
        for (index in 0 until childCount) {
            val child = getChildAt(index)
            var left: Int
            var right: Int
            var top: Int
            var bottom: Int
            val minLeft = paddingLeft
            val maxRight = max(r - paddingRight - l, 0)
            val minTop = paddingTop
            val maxBottom = max(b - paddingBottom - t, 0)
            var measuredChildHeight = child.measuredHeight
            var measuredChildWidth = child.measuredWidth
            // need to take account if child size is match_parent
            val childParams = child.layoutParams
            var matchParentHeight = false
            var matchParentWidth = false
            if (childParams != null) {
                matchParentHeight = childParams.height == MATCH_PARENT
                matchParentWidth = childParams.width == MATCH_PARENT
            }
            if (matchParentHeight) {
                measuredChildHeight = maxBottom - minTop
                childParams?.height = measuredChildHeight
            }
            if (matchParentWidth) {
                measuredChildWidth = maxRight - minLeft
                childParams?.width = measuredChildWidth
            }

            left =
                max(r - measuredChildWidth - paddingRight - l, minLeft)
            top = min(paddingTop, maxBottom)
            right = max(r - paddingRight - l, minLeft)
            bottom = min(measuredChildHeight + paddingTop, maxBottom)

            child.layout(left, top, right, bottom)
        }
        // taking account offset when mode is SAME_LEVEL
        if (mode == MODE_SAME_LEVEL) {
            secondaryView.offsetLeftAndRight(
                secondaryView.width
            )

        }
        initRects()
        if (isOpenBeforeInit) {
            open(false)
        } else {
            close(false)
        }
        lastMainLeft = mainView.left
        lastMainTop = mainView.top
        onLayoutCount++
    }

    /**
     * {@inheritDoc}
     */
    override fun onMeasure(widthMeasure: Int, heightMeasure: Int) {
        var widthMeasureSpec = widthMeasure
        var heightMeasureSpec = heightMeasure
        if (childCount < 2) {
            throw RuntimeException("Layout must have two children")
        }
        val params = layoutParams
        val widthMode = MeasureSpec.getMode(widthMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)
        var desiredWidth = 0
        var desiredHeight = 0
        // first find the largest child
        for (i in 0 until childCount) {
            val child = getChildAt(i)
            measureChild(child, widthMeasureSpec, heightMeasureSpec)
            desiredWidth = max(child.measuredWidth, desiredWidth)
            desiredHeight = max(child.measuredHeight, desiredHeight)
        }
        // create new measure spec using the largest child width
        widthMeasureSpec = MeasureSpec.makeMeasureSpec(desiredWidth, widthMode)
        heightMeasureSpec = MeasureSpec.makeMeasureSpec(desiredHeight, heightMode)
        val measuredWidth = MeasureSpec.getSize(widthMeasureSpec)
        val measuredHeight = MeasureSpec.getSize(heightMeasureSpec)
        for (i in 0 until childCount) {
            val child = getChildAt(i)
            val childParams = child.layoutParams
            if (childParams != null) {
                if (childParams.height == MATCH_PARENT) {
                    child.minimumHeight = measuredHeight
                }
                if (childParams.width == MATCH_PARENT) {
                    child.minimumWidth = measuredWidth
                }
            }
            measureChild(child, widthMeasureSpec, heightMeasureSpec)
            desiredWidth = max(child.measuredWidth, desiredWidth)
            desiredHeight = max(child.measuredHeight, desiredHeight)
        }
        // taking accounts of padding
        desiredWidth += paddingLeft + paddingRight
        desiredHeight += paddingTop + paddingBottom
        // adjust desired width
        if (widthMode == MeasureSpec.EXACTLY) {
            desiredWidth = measuredWidth
        } else {
            if (params.width == MATCH_PARENT) {
                desiredWidth = measuredWidth
            }
            if (widthMode == MeasureSpec.AT_MOST) {
                desiredWidth = if (desiredWidth > measuredWidth) measuredWidth else desiredWidth
            }
        }
        // adjust desired height
        if (heightMode == MeasureSpec.EXACTLY) {
            desiredHeight = measuredHeight
        } else {
            if (params.height == MATCH_PARENT) {
                desiredHeight = measuredHeight
            }
            if (heightMode == MeasureSpec.AT_MOST) {
                desiredHeight =
                    if (desiredHeight > measuredHeight) measuredHeight else desiredHeight
            }
        }
        setMeasuredDimension(desiredWidth, desiredHeight)
    }

    override fun computeScroll() {
        if (dragHelper?.continueSettling(true) == true) {
            ViewCompat.postInvalidateOnAnimation(this)
        }
    }

    /**
     * Open the panel to show the secondary view
     * @param animation true to animate the open motion.
     */
    fun open(animation: Boolean) {
        isOpenBeforeInit = true
        aborted = false
        if (animation) {
            mState = STATE_OPENING
            dragHelper?.smoothSlideViewTo(mainView, rectMainOpen.left, rectMainOpen.top)
            dragStateChangeListener?.onDragStateChanged(mState)
        } else {
            mState = STATE_OPEN
            dragHelper?.abort()
            mainView.layout(
                rectMainOpen.left,
                rectMainOpen.top,
                rectMainOpen.right,
                rectMainOpen.bottom
            )
            secondaryView.layout(
                rectSecOpen.left,
                rectSecOpen.top,
                rectSecOpen.right,
                rectSecOpen.bottom
            )
        }
        ViewCompat.postInvalidateOnAnimation(this@SwipeRevealLayout)
    }

    fun tutorial() {
        isOpenBeforeInit = true
        aborted = false
        mState = STATE_OPENING
        dragHelper?.smoothSlideViewTo(mainView, rectMainOpen.left + 10, rectMainOpen.top)
        dragStateChangeListener?.onDragStateChanged(mState)
        ViewCompat.postInvalidateOnAnimation(this@SwipeRevealLayout)
        Handler(Looper.getMainLooper()).postDelayed({
            close(true)
        }, 1000L)

    }

    /**
     * Close the panel to hide the secondary view
     * @param animation true to animate the close motion.
     */
    fun close(animation: Boolean) {
        isOpenBeforeInit = false
        aborted = false
        if (animation) {
            mState = STATE_CLOSING
            dragHelper?.smoothSlideViewTo(mainView, rectMainClose.left, rectMainClose.top)
            dragStateChangeListener?.onDragStateChanged(mState)
        } else {
            mState = STATE_CLOSE
            dragHelper?.abort()
            mainView.layout(
                rectMainClose.left,
                rectMainClose.top,
                rectMainClose.right,
                rectMainClose.bottom
            )
            secondaryView.layout(
                rectSecClose.left,
                rectSecClose.top,
                rectSecClose.right,
                rectSecClose.bottom
            )
        }
        ViewCompat.postInvalidateOnAnimation(this@SwipeRevealLayout)
    }


    /**
     * Get the edge where the layout can be dragged from.
     * @return Can be one of these
     *
     *  * [.DRAG_EDGE_LEFT]
     *  * [.DRAG_EDGE_TOP]
     *  * [.DRAG_EDGE_RIGHT]
     *  * [.DRAG_EDGE_BOTTOM]
     *
     */
    private fun getDragEdge(): Int {
        return dragEdge
    }


    /**
     * @param lock if set to true, the user cannot drag/swipe the layout.
     */
    fun setLockDrag(lock: Boolean) {
        lockDrag = lock
    }

    /**
     * @return true if the drag/swipe motion is currently locked.
     */
    private fun isDragLocked(): Boolean {
        return lockDrag
    }

    /** Only used for [ViewBinderHelper]  */
    fun setDragStateChangeListener(listener: DragStateChangeListener?) {
        dragStateChangeListener = listener
    }

    /** Abort current motion in progress. Only used for [ViewBinderHelper]  */
    fun abort() {
        aborted = true
        dragHelper?.abort()
    }

    /**
     * In RecyclerView/ListView, onLayout should be called 2 times to display children views correctly.
     * This method check if it've already called onLayout two times.
     * @return true if you should call [.requestLayout].
     */
    fun shouldRequestLayout(): Boolean {
        return onLayoutCount < 2
    }

    private fun getMainOpenLeft(): Int {
        return rectMainClose.left - secondaryView.width
    }

    private fun getMainOpenTop(): Int {
        return rectMainClose.top
    }

    private fun getSecOpenLeft(): Int {
        if (mode == MODE_NORMAL) {
            return rectSecClose.left
        }
        return rectSecClose.left - secondaryView.width
    }

    private fun getSecOpenTop(): Int {
        if (mode == MODE_NORMAL || dragEdge == DRAG_EDGE_RIGHT) {
            return rectSecClose.top
        }
        return rectSecClose.top - secondaryView.height
    }

    private fun initRects() { // close position of main view
        rectMainClose[mainView.left, mainView.top, mainView.right] = mainView.bottom
        // close position of secondary view
        rectSecClose[secondaryView.left, secondaryView.top, secondaryView.right] =
            secondaryView.bottom
        // open position of the main view
        rectMainOpen[getMainOpenLeft(), getMainOpenTop(), getMainOpenLeft() + mainView.width] =
            getMainOpenTop() + mainView.height
        // open position of the secondary view
        rectSecOpen[getSecOpenLeft(), getSecOpenTop(), getSecOpenLeft() + secondaryView.width] =
            getSecOpenTop() + secondaryView.height
    }

    private fun couldBecomeClick(ev: MotionEvent): Boolean {
        return isInMainView(ev) && !shouldInitiateADrag()
    }

    private fun isInMainView(ev: MotionEvent): Boolean {
        val x = ev.x
        val y = ev.y
        val withinVertical = mainView.top <= y && y <= mainView.bottom
        val withinHorizontal = mainView.left <= x && x <= mainView.right
        return withinVertical && withinHorizontal
    }

    private fun shouldInitiateADrag(): Boolean {
        val minDistToInitiateDrag = dragHelper?.touchSlop?.toFloat()
        return dragDist >= minDistToInitiateDrag ?: 0f
    }

    private fun accumulateDragDist(ev: MotionEvent) {
        val action = ev.action
        if (action == MotionEvent.ACTION_DOWN) {
            dragDist = 0f
            return
        }
        val dragHorizontally = getDragEdge() == DRAG_EDGE_RIGHT
        val dragged: Float
        dragged = if (dragHorizontally) {
            abs(ev.x - prevX)
        } else {
            abs(ev.y - prevY)
        }
        dragDist += dragged
    }

    private fun init(
        context: Context?
    ) {
        dragHelper = ViewDragHelper.create(this, 1.0f, mDragHelperCallback)
        dragHelper?.setEdgeTrackingEnabled(ViewDragHelper.EDGE_ALL)
        gestureDetector = GestureDetectorCompat(context, mGestureListener)
    }

    private val mGestureListener: GestureDetector.OnGestureListener =
        object : SimpleOnGestureListener() {
            var hasDisallowed = false
            override fun onDown(e: MotionEvent): Boolean {
                isScrolling = false
                hasDisallowed = false
                return true
            }

            override fun onFling(
                e1: MotionEvent,
                e2: MotionEvent,
                velocityX: Float,
                velocityY: Float
            ): Boolean {
                isScrolling = true
                return false
            }

            override fun onScroll(
                e1: MotionEvent,
                e2: MotionEvent,
                distanceX: Float,
                distanceY: Float
            ): Boolean {
                isScrolling = true
                if (parent != null) {
                    val shouldDisallow: Boolean
                    if (!hasDisallowed) {
                        shouldDisallow = getDistToClosestEdge() >= minDistRequestDisallowParent
                        if (shouldDisallow) {
                            hasDisallowed = true
                        }
                    } else {
                        shouldDisallow = true
                    }
                    // disallow parent to intercept touch event so that the layout will work
// properly on RecyclerView or view that handles scroll gesture.
                    parent.requestDisallowInterceptTouchEvent(shouldDisallow)
                }
                return false
            }
        }

    private fun getDistToClosestEdge(): Int {
        val pivotLeft = rectMainClose.right - secondaryView.width
        return min(
            mainView.right - pivotLeft,
            rectMainClose.right - mainView.right
        )
    }

    private fun getHalfwayPivotHorizontal(): Int {
        return rectMainClose.right - secondaryView.width / 2
    }


    private val mDragHelperCallback: ViewDragHelper.Callback = object : ViewDragHelper.Callback() {
        override fun tryCaptureView(
            child: View,
            pointerId: Int
        ): Boolean {
            aborted = false
            if (lockDrag) return false
            dragHelper?.captureChildView(mainView, pointerId)
            return false
        }

        override fun clampViewPositionVertical(
            child: View,
            top: Int,
            dy: Int
        ): Int {
            return child.top
        }

        override fun clampViewPositionHorizontal(
            child: View,
            left: Int,
            dx: Int
        ): Int {
            return Math.max(
                Math.min(left, rectMainClose.left),
                rectMainClose.left - secondaryView.width
            )
        }

        override fun onViewReleased(
            releasedChild: View,
            xvel: Float,
            yvel: Float
        ) {
            val velRightExceeded = pxToDp(xvel.toInt()) >= minFlingVelocity
            val velLeftExceeded = pxToDp(xvel.toInt()) <= -minFlingVelocity
            val pivotHorizontal = getHalfwayPivotHorizontal()
            if (velRightExceeded) {
                close(true)
            } else if (velLeftExceeded) {
                open(true)
            } else {
                if (mainView.right < pivotHorizontal) {
                    open(true)
                } else {
                    close(true)
                }
            }
        }

        override fun onEdgeDragStarted(edgeFlags: Int, pointerId: Int) {
            super.onEdgeDragStarted(edgeFlags, pointerId)
            if (lockDrag) {
                return
            }
            if ((dragEdge == DRAG_EDGE_RIGHT
                        && edgeFlags == ViewDragHelper.EDGE_LEFT)
            ) {
                dragHelper?.captureChildView(mainView, pointerId)
            }
        }

        override fun onViewPositionChanged(
            changedView: View,
            left: Int,
            top: Int,
            dx: Int,
            dy: Int
        ) {
            super.onViewPositionChanged(changedView, left, top, dx, dy)
            if (mode == MODE_SAME_LEVEL) {
                secondaryView.offsetLeftAndRight(dx)
            }
            val isMoved =
                mainView.left != lastMainLeft || mainView.top != lastMainTop
            if (isMoved) {
//                if (mainView.left == rectMainClose.left && mainView.top == rectMainClose.top) {
//                    mSwipeListener!!.onClosed(this@SwipeRevealLayout)

//                } else
                if (mainView.left == rectMainOpen.left && mainView.top == rectMainOpen.top) {
//                    mSwipeListener!!.onOpened(this@SwipeRevealLayout)
                    close(true)
                    secondaryView.performClick()

//                } else {
//                    mSwipeListener!!.onSlide(this@SwipeRevealLayout, slideOffset)
                }
            }
            lastMainLeft = mainView.left
            lastMainTop = mainView.top
            ViewCompat.postInvalidateOnAnimation(this@SwipeRevealLayout)
        }

        override fun onViewDragStateChanged(state: Int) {
            super.onViewDragStateChanged(state)
            val prevState = mState
            when (state) {
                ViewDragHelper.STATE_DRAGGING -> mState = STATE_DRAGGING
                ViewDragHelper.STATE_IDLE ->  // drag edge is left or right
                    mState = if (mainView.left == rectMainClose.left) {
                        STATE_CLOSE
                    } else {
                        STATE_OPEN
                    }
            }
            if (!aborted && prevState != mState) {
                dragStateChangeListener?.onDragStateChanged(mState)
            }
        }
    }

    private fun pxToDp(px: Int): Int {
        val resources = context.resources
        val metrics = resources.displayMetrics
        return (px / (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)).toInt()
    }

    private fun dpToPx(): Int {
        val resources = context.resources
        val metrics = resources.displayMetrics
        return (DEFAULT_MIN_DIST_REQUEST_DISALLOW_PARENT * (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)).toInt()
    }

    companion object {
        // These states are used only for ViewBindHelper
        const val STATE_CLOSE = 0
        const val STATE_CLOSING = 1
        const val STATE_OPEN = 2
        const val STATE_OPENING = 3
        const val STATE_DRAGGING = 4
        private const val DEFAULT_MIN_FLING_VELOCITY = 300 // dp per second
        private const val DEFAULT_MIN_DIST_REQUEST_DISALLOW_PARENT = 1 // dp
        const val DRAG_EDGE_RIGHT = 0x1 shl 1
        /**
         * The secondary view will be under the main view.
         */
        const val MODE_NORMAL = 0
        /**
         * The secondary view will stick the edge of the main view.
         */
        const val MODE_SAME_LEVEL = 1
    }
}