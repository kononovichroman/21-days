package com.habit.application.screen

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import com.habit.application.R
import com.habit.application.core.database.TargetModel
import com.habit.application.core.reminder.REMINDER_RECEIVER_TARGET_ID
import com.habit.application.core.reminder.notifications.NOTIFICATION_CHANNEL_ID
import com.habit.application.screen.base.BaseFragment
import com.habit.application.screen.detail.DETAIL_SCREEN_TARGET
import com.habit.application.screen.detail.DetailScreen
import com.habit.application.screen.main.MainScreen


/**
 * Created by Roman K. on 25.01.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
class RootActivity : AppCompatActivity() {

    private val viewModel: RootViewModel by viewModel { parametersOf(this@RootActivity) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.container)
        showFragment()
        viewModel.start()
        createNotificationChannel()
        Log.e("HABIT_TAG", "onCreate")
        checkIntent()

    }

    private fun checkIntent() {
        val bundle = intent.extras?.getBundle(REMINDER_RECEIVER_TARGET_ID)
        Log.e("Habit_TAG", "OPENACTIVITY: bundle: $bundle")
        val targetModel = bundle?.getParcelable<TargetModel>(DETAIL_SCREEN_TARGET)
        if ((targetModel != null) && !intent.isLaunchFromHistory()){
            Log.e("Habit_TAG", "OPENACTIVITY: targetModel: $targetModel")
            val fragmentTransaction =
                supportFragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.containerRoot, DetailScreen.instance(targetModel))
                .addToBackStack(null)
            fragmentTransaction.commit()
            intent.removeExtra(REMINDER_RECEIVER_TARGET_ID)
        }
    }

    fun Intent.isLaunchFromHistory(): Boolean = this.flags and Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY == Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY
    fun showFragment(){
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.containerRoot, MainScreen())
        fragmentTransaction.commit()
    }

    private fun createNotificationChannel(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            val name = resources.getString(R.string.notification_channel_name)
            val channel = NotificationChannel(NOTIFICATION_CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT)
            val notificationManager = getSystemService(NotificationManager::class.java)
            notificationManager.createNotificationChannel(channel)
        }
    }

    override fun onPause() {
        super.onPause()
        viewModel.onPause()
    }

    override fun onResume() {
        super.onResume()
        viewModel.onResume()
    }
    override fun onBackPressed() {
        supportFragmentManager.findFragmentById(R.id.containerRoot)?.let {
            if (it is BaseFragment){
                if (it.isBackPressNeed) {
                    super.onBackPressed()
                } else {
                    it.onBackPressed()
                }
            }
        }
    }
}