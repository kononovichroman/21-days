package com.habit.application.screen.onboarding.view

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.habit.application.R


/**
 * Created by Roman K. on 13.02.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
enum class OnboardingContent(
    @StringRes val titleResource: Array<Int>,
    @StringRes val descriptionResource: Array<Int>,
    @DrawableRes val logoResource: Int,
    val isLastElement: Boolean
) {
    FIRST_SCREEN(
        titleResource = arrayOf(R.string.onboarding_first_screen_title),
        descriptionResource = arrayOf(R.string.onboarding_first_screen_description),
        logoResource = R.drawable.ic_onboarding_1,
        isLastElement = false
    ),
    SECOND_SCREEN(
        titleResource = arrayOf(R.string.onboarding_second_screen_title),
        descriptionResource = arrayOf(R.string.onboarding_second_screen_description),
        logoResource = R.drawable.ic_onboarding_2,
        isLastElement = false
    ),
    LAST_SCREEN(
        titleResource = arrayOf(R.string.onboarding_third_screen_title),
        descriptionResource = arrayOf(R.string.onboarding_third_screen_description),
        logoResource = R.drawable.ic_onboarding_3,
        isLastElement = true
    )
}