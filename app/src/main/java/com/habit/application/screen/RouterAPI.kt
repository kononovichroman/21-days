package com.habit.application.screen

import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity


/**
 * Created by Roman K. on 25.01.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
interface RouterAPI {
    fun startMainScreen()
    fun startOnboarding(addToBackStack: Boolean = false)
    fun openPrivacy()
    fun openInfoScreen()
    fun openCreateTarget()
    fun closeInfo()
    fun closeCreateTarget()
    fun showToastNormal(@StringRes resId: Int)
    fun showToastLong(@StringRes resId: Int)
    fun showToastNormal(msg: String)
    fun startAnimProgress(needDays: Int, completedDays: Int, withAnim: Boolean = true)
    fun showMsgTopSnackBar(@StringRes resId: Int)
    fun hideTopSnackBar()
    fun showDialogRestartExpiredTarget()
    fun showDialogRestartCompletedTarget()
    fun showDialogDeleteTarget(nameTarget: String, id: Long)
    fun showRateDialog(successDays: Int)
    fun showDialogUpdateDownloaded()
    fun openGooglePlay()
    fun shareApp()
    fun contactUs()
    fun getScreenForAnalytics(): Pair<AppCompatActivity, String?>
    var clickRestartTarget: () -> Unit
    var clickDeleteTarget: (id: Long) -> Unit
    var clickInstallUpdate: () -> Unit
}