package com.habit.application.screen.onboarding.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import kotlinx.android.synthetic.main.onboarding_view.view.*
import com.habit.application.R


/**
 * Created by Roman K. on 13.02.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
class OnboardingView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr), ViewPager.OnPageChangeListener {

    private val numberOfPages by lazy { OnboardingContent.values().size }


    init {
        initializeUi(context)
    }

    private fun initializeUi(context: Context) {
        LayoutInflater.from(context).inflate(R.layout.onboarding_view, this, true)

        setupListeners()
    }

    private fun setupListeners() {
        onboardingViewViewPager.addOnPageChangeListener(this@OnboardingView)

        onboardingViewNextBtn.setOnClickListener {
            onboardingViewViewPager.setCurrentItem(
                onboardingViewViewPager.currentItem + 1,
                true
            )
        }
        listener?.let { setOnListeners(it) }

    }

    fun setOnListeners(l: Listener) {
        listener = l
        listener?.currentPosition(1)
    }


    interface Listener {
        fun currentPosition(position: Int)
    }

    private var listener: Listener? = null


    fun setAdapter(adapter: PagerAdapter) {
        onboardingViewViewPager.adapter = adapter
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
        onboardingViewRoot.progress = (position + positionOffset) / (numberOfPages - 1)

    }

    override fun onPageScrollStateChanged(state: Int) = Unit
    override fun onPageSelected(position: Int) {
        listener?.currentPosition(position + 1)
    }
}