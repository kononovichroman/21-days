package com.habit.application.screen.main

import android.app.Application
import android.os.Handler
import android.text.Editable
import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.AndroidViewModel
import com.habit.application.BuildConfig
import com.habit.application.R
import com.habit.application.core.database.Status
import com.habit.application.core.database.TargetModel
import com.habit.application.core.database.entity.TargetEntity
import com.habit.application.core.database.repository.TargetRepositoryAPI
import com.habit.application.core.extensions.addOnPropertyChanged
import com.habit.application.core.helper.ThemeHelperAPI
import com.habit.application.core.managers.analytics.AnalyticsManagerAPI
import com.habit.application.core.reminder.ReminderSchedulerAPI
import com.habit.application.core.reminder.TimeModel
import com.habit.application.core.reminder.TimeType
import com.habit.application.core.settings.SettingsManagerAPI
import com.habit.application.screen.RouterAPI
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


/**
 * Created by Roman K. on 25.01.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
private const val DEFAULT_COUNT_DAY = 21

class MainViewModel(
    private val context: Application,
    private val themeHelper: ThemeHelperAPI,
    private val router: RouterAPI,
    private val repository: TargetRepositoryAPI,
    private val analyticsManager: AnalyticsManagerAPI,
    private val settingsManager: SettingsManagerAPI,
    private val reminderScheduler: ReminderSchedulerAPI
) : AndroidViewModel(context) {

    private val isHintRvShown = ObservableBoolean()
    val nameTarget = ObservableField<String>()
    val inputError = ObservableField<String>()
    val listTargets = ObservableField<List<TargetModel>>()
    val emptyTarget = ObservableBoolean()
    val stateScreenState = ObservableField<MainScreenState>(MainScreenState.START)
    val timeNotificationsSelect = ObservableField<TimeModel>()
    val countDays = ObservableInt(DEFAULT_COUNT_DAY)
    val needShowTutorialRvItem = ObservableBoolean(false)

    init {
        router.run {
            clickDeleteTarget = {
                reminderScheduler.hideNotification(it.toInt())
                GlobalScope.launch {
                    repository.delete(it)
                    getListTargets()
                }
            }
        }
        repository.run {
            insertNewTarget = {
                Log.e("Tutorial", "insertNewTarget")
                if (!isHintRvShown.get() && listTargets.get().isNullOrEmpty()) {
                    needShowTutorialRvItem.set(true)
                    isHintRvShown.set(true)
                    settingsManager.setHintRVShow()
                }
                reminderScheduler.schedule(it)
                getListTargets()
            }
        }
        listTargets.addOnPropertyChanged {
            emptyTarget.set(it.get().isNullOrEmpty())
        }
    }

    fun start() {
        themeHelper.themeMain()
        router.getScreenForAnalytics().second?.let {
            analyticsManager.currentScreen(
                router.getScreenForAnalytics().first,
                it
            )
        }
        sendAnalytics()
        isHintRvShown.set(settingsManager.isHintRVShown())
    }

    private fun sendAnalytics() {
        analyticsManager.apply {
            setUserProperty(
                AnalyticsManagerAPI.AnalyticsUserProperty.DAY_FIRST_LAUNCH,
                settingsManager.startUseAppDay()
            )
            setUserProperty(
                AnalyticsManagerAPI.AnalyticsUserProperty.WEEK_FIRST_LAUNCH,
                settingsManager.startUseAppWeek()
            )
            setUserProperty(
                AnalyticsManagerAPI.AnalyticsUserProperty.MONTH_FIRST_LAUNCH,
                settingsManager.startUseAppMonth()
            )
            setUserProperty(
                AnalyticsManagerAPI.AnalyticsUserProperty.START_VERSION_APP,
                settingsManager.startVersion()
            )
            setUserProperty(
                AnalyticsManagerAPI.AnalyticsUserProperty.CURRENT_VERSION_APP,
                BuildConfig.VERSION_CODE
            )
            setUserProperty(
                AnalyticsManagerAPI.AnalyticsUserProperty.COUNT_LAUNCH_APP,
                settingsManager.countLaunch()
            )
        }
    }

    fun onResume() {
        themeHelper.themeMain()
        getListTargets()
    }

    private fun getListTargets() {
        GlobalScope.launch {
            val list = repository.allTargets()
            listTargets.set(list)
            sendTargetsAnalytics(list)
        }
    }

    private fun sendTargetsAnalytics(list: List<TargetModel>) {
        analyticsManager.apply {
            setUserProperty(
                AnalyticsManagerAPI.AnalyticsUserProperty.COUNT_EXPIRED_TARGETS,
                list.filter { it.status == Status.ERROR }.size
            )
            setUserProperty(
                AnalyticsManagerAPI.AnalyticsUserProperty.COUNT_SUCCESS_TARGETS,
                list.filter { it.status == Status.COMPLETED }.size
            )
            setUserProperty(
                AnalyticsManagerAPI.AnalyticsUserProperty.COUNT_ACTIVE_TARGETS,
                list.filter { it.status == Status.ACTIVE }.size
            )
            setUserProperty(AnalyticsManagerAPI.AnalyticsUserProperty.COUNT_ALL_TARGETS, list.size)
        }
    }

    fun showDialogDelete(nameTarget: String, id: Long) {
        router.showDialogDeleteTarget(nameTarget, id)
        analyticsManager.logEvent(AnalyticsManagerAPI.AnalyticsEvent.SHOW_DELETE_TARGET)
    }

    fun clickInfo() {
        when (stateScreenState.get()) {
            MainScreenState.START -> {
                themeHelper.themeMainBottomSheetOpen()
                router.openInfoScreen()
                stateScreenState.set(MainScreenState.INFO)
            }
            MainScreenState.CREATE_TARGET -> {
                if (BuildConfig.DEBUG) {
                    router.showToastNormal("ERROR")
                }
            }
            MainScreenState.INFO -> {
                if (BuildConfig.DEBUG) {
                    router.showToastNormal("ERROR")
                }
            }
        }

    }

    fun openDetailScreen() {
        router.hideTopSnackBar()
    }

    fun clickCreateNotification() {
        router.showMsgTopSnackBar(R.string.main_screen_top_scnackbar_function_under_develop)
        analyticsManager.logEvent(AnalyticsManagerAPI.AnalyticsEvent.CLICK_SHOW_NOTIFICATION)
    }

    fun clickChangeCountDays(){
        analyticsManager.logEvent(AnalyticsManagerAPI.AnalyticsEvent.CLICK_CHANGE_COUNT_DAYS)
    }

    fun clickFab() {
        when (stateScreenState.get()) {
            MainScreenState.START -> {
                themeHelper.themeMainBottomSheetOpen()
                router.openCreateTarget()
                stateScreenState.set(MainScreenState.CREATE_TARGET)
            }
            MainScreenState.CREATE_TARGET -> {
                if (nameTarget.get().isNullOrEmpty() || nameTarget.get()?.trim()?.length == 0) {
                    inputError.set(context.resources.getString(R.string.main_screen_input_error))
                } else {
                    nameTarget.get()?.let {
                        GlobalScope.launch {
                            repository.insert(
                                prepareTargetEntity(it)
                            )
                            updateUIEmptyTarget()
                        }
                    }
                    themeHelper.themeMain()
                    router.closeCreateTarget()
                    stateScreenState.set(MainScreenState.START)
                }
            }
        }
    }

    private fun prepareTargetEntity(nameTarget: String): TargetEntity {
        return TargetEntity(
            name = nameTarget,
            startTime = System.currentTimeMillis(),
            lastCheckTime = System.currentTimeMillis(),
            status = Status.ACTIVE,
            successDays = 0,
            needDays = countDays.get(),
            hourNotification = if (timeNotificationsSelect.get()?.type != TimeType.TIME_TYPE_24) {
                timeNotificationsSelect.get()?.convertAmPmTo24()?.hour?.toInt()
            } else {
                timeNotificationsSelect.get()?.hour?.toInt()
            },
            minuteNotification = timeNotificationsSelect.get()?.minute?.toInt()
        )
    }

    private fun updateUIEmptyTarget() {
        inputError.set(null)
        nameTarget.set("")
        timeNotificationsSelect.set(null)
        countDays.set(DEFAULT_COUNT_DAY)
    }

    fun clickBackPressed() {
        when (stateScreenState.get()) {
            MainScreenState.CREATE_TARGET, MainScreenState.INFO -> {
                router.closeCreateTarget()
                themeHelper.themeMain()
                stateScreenState.set(MainScreenState.START)
            }
            else -> {
            }
        }
    }

    fun clickIconCloseBottomLayout() {
        analyticsManager.logEvent(AnalyticsManagerAPI.AnalyticsEvent.CLOSE_BOTTOM_LAYOUT_ICON)
        closeBottomLayout()
    }

    fun clickBGCloseBottomLayout() {
        analyticsManager.logEvent(AnalyticsManagerAPI.AnalyticsEvent.CLOSE_BOTTOM_LAYOUT_BG)
        closeBottomLayout()
    }

    fun selectTimeNotifications(hour: Int, minute: Int, ampm: String) {
        val type = when (ampm) {
            "AM" -> TimeType.TIME_TYPE_AM
            "PM" -> TimeType.TIME_TYPE_PM
            else -> TimeType.TIME_TYPE_24
        }
        timeNotificationsSelect.set(TimeModel(hour, minute, type))
    }

    private fun closeBottomLayout() {
        when (stateScreenState.get()) {
            MainScreenState.START -> {
//                router.showToastNormal("EMPTY")
            }
            MainScreenState.CREATE_TARGET -> {
                updateUIEmptyTarget()
                router.closeCreateTarget()
                themeHelper.themeMain()
                stateScreenState.set(MainScreenState.START)
            }
            MainScreenState.INFO -> {
                router.closeInfo()
                themeHelper.themeMain()
                stateScreenState.set(MainScreenState.START)
            }
        }
    }

    fun clickPrivacyPolicy() {
        router.closeInfo()
        stateScreenState.set(MainScreenState.START)
        router.openPrivacy()
        analyticsManager.logEvent(AnalyticsManagerAPI.AnalyticsEvent.CLICK_PRIVACY_POLICY)
    }

    fun clickShareApp() {
        router.closeInfo()
        stateScreenState.set(MainScreenState.START)
        router.shareApp()
        analyticsManager.logEvent(AnalyticsManagerAPI.AnalyticsEvent.CLICK_SHARE_APP)
    }

    fun clickContactUs() {
        router.closeInfo()
        stateScreenState.set(MainScreenState.START)
        router.contactUs()
        analyticsManager.logEvent(AnalyticsManagerAPI.AnalyticsEvent.CLICK_CONTACT_US)
    }

    fun clickRateApp() {
        router.closeInfo()
        stateScreenState.set(MainScreenState.START)
        router.openGooglePlay()
        analyticsManager.logEvent(AnalyticsManagerAPI.AnalyticsEvent.CLICK_RATE_APP)
    }

    fun onTextChange(editable: Editable?) {
        inputError.set(null)
    }


    enum class MainScreenState {
        START, CREATE_TARGET, INFO
    }

}