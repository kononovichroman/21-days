package com.habit.application.screen.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.ViewCompat
import androidx.databinding.DataBindingUtil
import com.habit.application.App
import com.habit.application.R
import com.habit.application.core.database.Status
import com.habit.application.core.database.TargetModel
import com.habit.application.databinding.DetailScreenBinding
import com.habit.application.screen.base.BaseFragment
import com.habit.application.screen.main.pickers.TimePickerFragment
import kotlinx.android.synthetic.main.detail_screen.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf


/**
 * Created by Roman K. on 31.01.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
const val DETAIL_SCREEN_TARGET = "detail_screen_target"

class DetailScreen : BaseFragment() {
    private val viewModel: DetailViewModel by viewModel { parametersOf(activity!!, this) }
    private lateinit var bind: DetailScreenBinding

    companion object {
        fun instance(
            target: TargetModel,
            tag: String? = null
        ) =
            DetailScreen().apply {
                arguments = Bundle(1).apply {
                    putParcelable(DETAIL_SCREEN_TARGET, target)
                }
            }
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        bind = DataBindingUtil.inflate(inflater, R.layout.detail_screen, container, false)
        return bind.root
    }

    private fun initViewModel() {
        bind.detail = viewModel
        arguments?.getParcelable<TargetModel>(DETAIL_SCREEN_TARGET)?.let {
            viewModel.start(it)
        }
    }

    override fun onStart() {
        super.onStart()
        setStatusBarMargin()
        initViewModel()
        bind.detailScreenProgressPolygon1.setOnLongClickListener {
            viewModel.longClick()
            return@setOnLongClickListener true
        }
        bind.detailScreenProgressPolygon1.setOnClickListener {
            viewModel.shortClick()
        }
        bind.detailScreenIVClose.setOnClickListener {
            onBackPressed()
        }
        bind.detailScreenIvNotificationTime.setOnClickListener {
            if (viewModel.timeNotificationsSelect.get() == null && viewModel.target.get()?.status != Status.COMPLETED) {
                showTimePicker()
            }
        }
    }

    private fun showTimePicker() {
        val timePicker = TimePickerFragment.instance(object : TimePickerFragment.Listener {
            override fun setTime(hour: Int, minute: Int, ampm: String) {
                viewModel.setTimeNotifications(hour, minute, ampm)
            }
        })
        timePicker.show(requireActivity().supportFragmentManager, timePicker.tag)
    }

    override fun onResume() {
        super.onResume()
        setNavBarSize()
        viewModel.onResume()

    }

    private fun setNavBarSize() {
        if (App.navbarHeight >= 0) {
            setGuidelineMargin(App.navbarHeight)
        }
        ViewCompat.setOnApplyWindowInsetsListener(bind.root) { _, windowInsets ->
            if (App.navbarHeight < 0 || App.navbarHeight != windowInsets.systemWindowInsetBottom) {
                App.navbarHeight = windowInsets.systemWindowInsetBottom
                setGuidelineMargin(App.navbarHeight)
            }
            if (App.statusBarHeight < 0 || App.statusBarHeight != windowInsets.systemWindowInsetTop) {
                App.statusBarHeight = windowInsets.systemWindowInsetTop
            }
            windowInsets
        }
    }

    private fun setGuidelineMargin(margin: Int) {
        val lp = bind.detailScreenNavBarGuideline.layoutParams as ConstraintLayout.LayoutParams
        lp.setMargins(0, 0, 0, margin)
        bind.detailScreenNavBarGuideline.layoutParams = lp
    }

    private fun setStatusBarMargin() {
        setTopMarginStatusBar(bind.detailScreenRoot, detailScreenIVClose)
    }

    override fun onBackPressed() {
        viewModel.onBackPressed()
        super.onBackPressed()
    }

}