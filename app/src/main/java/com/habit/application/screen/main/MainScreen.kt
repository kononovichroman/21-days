package com.habit.application.screen.main

import android.os.Bundle
import android.os.Handler
import android.text.InputFilter
import android.util.Log
import android.view.HapticFeedbackConstants
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.IdRes
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.view.ViewCompat
import androidx.core.view.marginTop
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.habit.application.App
import com.habit.application.R
import com.habit.application.core.database.TargetModel
import com.habit.application.core.extensions.addOnPropertyChanged
import com.habit.application.core.extensions.toDP
import com.habit.application.core.keyboardProvider.KeyboardHeightProvider
import com.habit.application.databinding.MainScreenBinding
import com.habit.application.screen.base.BaseFragment
import com.habit.application.screen.detail.DetailScreen
import com.habit.application.screen.main.adapter.ListTargetRvAdapter
import com.habit.application.screen.main.adapter.SimpleDividerItemDecoration
import com.habit.application.screen.main.adapter.SwipeRevealLayout
import com.habit.application.screen.main.pickers.CountDaysPickerFragment
import com.habit.application.screen.main.pickers.TimePickerFragment
import com.habit.application.utils.rv.itemAnimator.OvershootAnimator
import kotlinx.android.synthetic.main.view_stub_about.*
import kotlinx.android.synthetic.main.view_stub_create_target.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf


/**
 * Created by Roman K. on 25.01.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
private const val MAX_LENGTH_EDIT_TEXT = 50

class MainScreen : BaseFragment() {
    private val viewModel: MainViewModel by viewModel { parametersOf(activity!!, this) }
    private lateinit var bind: MainScreenBinding
    private var keyboardHeightProvider: KeyboardHeightProvider? = null

    override fun onStart() {
        super.onStart()
        initViewModel()
        setListeners()
    }


    private fun getKeyboardListener() = object : KeyboardHeightProvider.KeyboardListener {
        override fun onHeightChanged(height: Int) {
            bind.includeCreateTarget.viewStubCreateTargetRoot.post {
                val editTextHeight =
                    bind.includeCreateTarget.viewStubCreateTargetTextInputLayout.measuredHeight
                val editTextMarginTop =
                    bind.includeCreateTarget.viewStubCreateTargetTextInputLayout.marginTop
                val fabHeight = bind.mainScreenFAB.measuredHeight
                val iconsHeight =
                    80f.toDP().toInt() // 16dp topMargin + 16dp bottomMargin + 48dp iconSize
                if (height > 0) {
                    bind.mainScreenMotionLayout.getConstraintSet(R.id.keyboardEnd)?.let {
                        it.connect(
                            R.id.mainScreenFAB,
                            ConstraintSet.TOP,
                            R.id.includeCreateTarget,
                            ConstraintSet.TOP
                        )
                        it.setMargin(
                            R.id.mainScreenFAB,
                            ConstraintSet.TOP,
                            iconsHeight + editTextHeight + editTextMarginTop + FAB_MARGIN_TOP_OPEN_IME.toDP().toInt()
                        )
                        it.constrainHeight(
                            R.id.includeCreateTarget,
                            iconsHeight + editTextHeight + editTextMarginTop + HEIGHT_FAB_MARGIN_TOP_OPEN_IME.toDP().toInt() + fabHeight + HEIGHT_FAB_MARGIN_BOTTOM_OPEN_IME.toDP().toInt() + App.navbarHeight + height
                        )
                        it.applyTo(bind.mainScreenMotionLayout)
                    }
                    bind.mainScreenMotionLayout.setTransition(R.id.end, R.id.keyboardEnd)
                    bind.mainScreenMotionLayout.transitionToEnd()
                    bind.includeCreateTarget.viewStubCreateTargetTvNotification.visibility =
                        View.GONE
                    bind.includeCreateTarget.viewStubCreateTargetTvCountDaysDesc.visibility =
                        View.GONE
                } else {
                    bind.mainScreenMotionLayout.setTransition(R.id.start, R.id.end)
                    bind.mainScreenMotionLayout.transitionToStart()
                    bind.includeCreateTarget.viewStubCreateTargetTvNotification.visibility =
                        View.VISIBLE
                    bind.includeCreateTarget.viewStubCreateTargetIvNotification.apply {
                        scaleX = 1f
                        scaleY = 1f
                    }
                    bind.includeCreateTarget.viewStubCreateTargetTvCountDays.apply {
                        scaleX = 1f
                        scaleY = 1f
                    }
                    bind.includeCreateTarget.viewStubCreateTargetTvCountDaysDesc.visibility =
                        View.VISIBLE
                    bind.includeCreateTarget.viewStubCreateTargetIvCountDays.apply {
                        scaleX = 1f
                        scaleY = 1f
                    }
                }
            }
        }
    }


    private fun setListeners() {
        viewStubAboutViewReview.setOnClickListener {
            viewModel.clickContactUs()
        }
        viewStubAboutViewPolicy.setOnClickListener {
            viewModel.clickPrivacyPolicy()
        }
        viewStubAboutViewShare.setOnClickListener {
            viewModel.clickShareApp()
        }
        viewStubAboutViewStar.setOnClickListener {
            viewModel.clickRateApp()
        }
        viewStubCreateTargetViewNotification.setOnClickListener {
            showTimePicker()
        }
        viewStubCreateTargetViewCountDays.setOnClickListener {
            showCountDaysPicker()
            viewModel.clickChangeCountDays()
        }
    }

    private fun showCountDaysPicker() {
        val timePicker = CountDaysPickerFragment.Builder().countDays(viewModel.countDays.get())
            .setListener(object : CountDaysPickerFragment.Listener {
                override fun setCountDays(countDays: Int) {
                    viewModel.countDays.set(countDays)
                }
            }).build()
        timePicker?.show(requireActivity().supportFragmentManager, timePicker.tag)
    }

    private fun showTimePicker() {
        val timePicker = TimePickerFragment.instance(object : TimePickerFragment.Listener {
            override fun setTime(hour: Int, minute: Int, ampm: String) {
                viewModel.selectTimeNotifications(hour, minute, ampm)
            }
        })
        timePicker.show(requireActivity().supportFragmentManager, timePicker.tag)
    }

    private fun initViewModel() {
        bind.main = viewModel
        bind.includeCreateTarget.target = viewModel
        bind.includeAbout.about = viewModel
        bind.includeEmpty.empty = viewModel
        viewModel.start()

        viewModel.needShowTutorialRvItem.addOnPropertyChanged {
            if (it.get()) {
                showTutorialInRv()
            }
        }
        viewModel.listTargets.addOnPropertyChanged {
            bind.mainScreenRvListTargets.apply {
                it.get()?.let {list ->
                    requireActivity().runOnUiThread {
                        (bind.mainScreenRvListTargets.adapter as ListTargetRvAdapter).setList(list)
                    }
                }
            }
        }
    }

    private fun showTutorialInRv() {
        bind.mainScreenRvListTargets.let { rv ->
            requireActivity().runOnUiThread {
                Handler().postDelayed({
                    try {
                        (rv.findViewHolderForAdapterPosition((rv.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition())?.itemView as SwipeRevealLayout).tutorial()
                    } catch (e: ExceptionInInitializerError) {
                        Log.e("MainScreen", "showTutorialInRv exception: ${e.exception}")
                    }
                    viewModel.needShowTutorialRvItem.set(false)
                }, 500)
            }

        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.onResume()
        keyboardHeightProvider?.onResume()
        setBarsMargin()
    }

    override fun onPause() {
        super.onPause()
        keyboardHeightProvider?.onPause()
    }

    private fun initRV() {
        bind.mainScreenRvListTargets.apply {
            adapter = ListTargetRvAdapter(activity, object : ListTargetRvAdapter.ItemClickListener {
                override fun onItemClicked(target: TargetModel) {
                    showFragment(target)
                }

                override fun onClickDelete(target: TargetModel) {
                    bind.root.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
                    viewModel.showDialogDelete(target.name, target.id)
                }

            })
            (adapter as ListTargetRvAdapter).registerAdapterDataObserver(object :
                RecyclerView.AdapterDataObserver() {
                override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                    scrollToPosition(0)
                }
            })
            layoutManager = LinearLayoutManager(activity)
            activity?.let { addItemDecoration(SimpleDividerItemDecoration(it)) }
            itemAnimator =
                OvershootAnimator()
            itemAnimator?.addDuration = ITEM_ANIM_DURATION
        }


    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
        keyboardHeightProvider = KeyboardHeightProvider(activity!!)
        keyboardHeightProvider?.addKeyboardListener(getKeyboardListener())
    }

    private fun showFragment(target: TargetModel) {
        val fragmentTransaction =
            activity?.supportFragmentManager?.beginTransaction()
//        fragmentTransaction?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        fragmentTransaction?.setCustomAnimations(
            R.animator.slide_to_top,
            R.animator.slide_to_bottom,
            R.animator.slide_to_top,
            R.animator.slide_to_bottom
        )
        fragmentTransaction?.replace(R.id.containerRoot, DetailScreen.instance(target))
        fragmentTransaction?.addToBackStack(null)
        fragmentTransaction?.commit()
        viewModel.openDetailScreen()

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        bind = DataBindingUtil.inflate(inflater, R.layout.main_screen, container, false)
        initRV()
        setEditTextFilter()
        setBarsMargin()
        return bind.root
    }

    private fun setEditTextFilter() {
        bind.includeCreateTarget.viewStubCreateTargetEditText.filters =
            arrayOf(inputFilerMultipleSpace(), InputFilter.LengthFilter(MAX_LENGTH_EDIT_TEXT))
    }

    private fun setSizeBottomLayout(h: Int) {
        bind.includeCreateTarget.viewStubCreateTargetRoot.setPadding(
            0,
            0,
            0,
            h
        )
        bind.includeAbout.viewStubAboutRoot.setPadding(0, 0, 0, h)
    }

    private fun setNavBarSize(@IdRes idView: IntArray, navbarHeight: Int) {
        idView.forEach {
            bind.mainScreenMotionLayout.getConstraintSet(it)?.let { set ->
                set.setMargin(
                    R.id.mainScreenNavBarGuideline,
                    ConstraintSet.BOTTOM,
                    navbarHeight
                )
                set.applyTo(bind.mainScreenMotionLayout)
            }
        }

    }

    private fun setBarsMargin() {
        setTopMarginStatusBar(bind.mainScreenMotionLayout, bind.mainScreenToolbarGuideline)
        if (App.navbarHeight >= 0) {
            setSizeBottomLayout(App.navbarHeight)
            setNavBarSize(
                intArrayOf(R.id.start, R.id.end, R.id.infoEnd, R.id.keyboardEnd),
                App.navbarHeight
            )
        }

        ViewCompat.setOnApplyWindowInsetsListener(bind.root) { _, windowInsets ->
            if (App.navbarHeight < 0 || App.navbarHeight != windowInsets.systemWindowInsetBottom) {
                App.navbarHeight = windowInsets.systemWindowInsetBottom
                setSizeBottomLayout(App.navbarHeight)
                setNavBarSize(
                    intArrayOf(R.id.start, R.id.end, R.id.infoEnd, R.id.keyboardEnd),
                    App.navbarHeight
                )
            }
            if (App.statusBarHeight < 0 || App.statusBarHeight != windowInsets.systemWindowInsetTop) {
                App.statusBarHeight = windowInsets.systemWindowInsetTop
            }
            windowInsets
        }
    }

    private fun inputFilerMultipleSpace(): InputFilter {
        return InputFilter { source, _, _, dest, dstart, _ ->
            val stringSource = source.toString()
            val stringDest = dest.toString()
            if (stringSource == " ") {
                if (stringDest.isEmpty() || dstart > 1 && dest[dstart - 1] == ' ') {
                    return@InputFilter ""
                }
            }
            return@InputFilter null
        }
    }


    override fun onBackPressed() {
        viewModel.clickBackPressed()
    }

    private companion object {
        const val FAB_MARGIN_TOP_OPEN_IME = 8f
        const val HEIGHT_FAB_MARGIN_TOP_OPEN_IME = 16f
        const val HEIGHT_FAB_MARGIN_BOTTOM_OPEN_IME = 8f
        const val ITEM_ANIM_DURATION = 800L
    }

}