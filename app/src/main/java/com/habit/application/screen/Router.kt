package com.habit.application.screen

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.os.StrictMode
import android.util.Log
import android.view.View
import android.view.animation.BounceInterpolator
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.browser.customtabs.CustomTabsIntent
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator
import com.habit.application.BuildConfig
import com.habit.application.R
import com.habit.application.core.settings.SettingsManagerAPI
import com.habit.application.screen.base.BaseFragment
import com.habit.application.screen.detail.DetailScreen
import com.habit.application.screen.main.MainScreen
import com.habit.application.screen.onboarding.OnboardingScreen
import com.habit.application.utils.ProgressBarAnimation
import com.habit.application.utils.dialogs.DIALOG_TAG
import com.habit.application.utils.dialogs.Dialog
import com.habit.application.utils.dialogs.DialogRate
import com.habit.application.utils.topsnackbar.TopSnackBar
import kotlinx.android.synthetic.main.detail_screen.*
import kotlinx.android.synthetic.main.main_screen.*


/**
 * Created by Roman K. on 25.01.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
const val TYPE_TEXT = "text/plain"
private const val INTENT_CONTACT_US_EMAIL = "support@habitapp.xyz"
private const val URL_PRIVACY_POLICY = "https://habitapp.xyz/privacy.html"

private const val ONBOARDING_FRAGMENT_TAG = "onboarding_fragment_tag"

class Router(private val activity: AppCompatActivity, private val fragment: BaseFragment? = null, private val settingsManager: SettingsManagerAPI) : RouterAPI {
    override fun startMainScreen() {
        val fragmentTransaction = activity.supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.containerRoot, MainScreen())
        fragmentTransaction.commitAllowingStateLoss()
    }

    override fun startOnboarding(addToBackStack: Boolean) {
        val fragmentTransaction = activity.supportFragmentManager.beginTransaction()
        if (addToBackStack) fragmentTransaction.addToBackStack(ONBOARDING_FRAGMENT_TAG)
        fragmentTransaction.replace(R.id.containerRoot, OnboardingScreen())
        fragmentTransaction.commitAllowingStateLoss()
    }

    override fun openPrivacy() {
        CustomTabsIntent.Builder().build().launchUrl(activity, Uri.parse(URL_PRIVACY_POLICY))
    }

    override fun openInfoScreen() {
        activity.supportFragmentManager.fragments.forEach {
            if (it != null && it is MainScreen) {
                activity.mainScreenMotionLayout?.setTransition(R.id.start, R.id.infoEnd)
                activity.mainScreenMotionLayout?.transitionToEnd()
                it.isBackPressNeed = false
            }
        }
    }

    override fun openCreateTarget() {
        activity.supportFragmentManager.fragments.forEach {
            if (it != null && it is MainScreen) {
                activity.mainScreenMotionLayout?.setTransition(R.id.start, R.id.end)
                activity.mainScreenMotionLayout?.transitionToEnd()
                it.isBackPressNeed = false
            }
        }
    }

    override fun closeInfo() {
        hideKeyboard(activity)
        activity.supportFragmentManager.fragments.forEach {
            if (it != null && it is MainScreen) {
                activity.mainScreenMotionLayout?.transitionToStart()
                it.isBackPressNeed = true
            }
        }
    }

    override fun closeCreateTarget() {
        hideKeyboard(activity)
        activity.supportFragmentManager.fragments.forEach {
            if (it != null && it is MainScreen) {
                activity.mainScreenMotionLayout?.transitionToStart()
                it.isBackPressNeed = true
            }
        }


    }

    override fun showToastNormal(resId: Int) {
        activity.runOnUiThread {
            Toast.makeText(activity, resId, Toast.LENGTH_SHORT).show()
        }
    }

    override fun showToastNormal(msg: String) {
        activity.runOnUiThread {
            Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show()
        }
    }

    override fun showToastLong(resId: Int) {
        activity.runOnUiThread {
            Toast.makeText(activity, resId, Toast.LENGTH_LONG).show()
        }
    }

    override fun startAnimProgress(needDays: Int, completedDays: Int, withAnim: Boolean) {
        activity.supportFragmentManager.fragments.forEach {
            if (it != null && it is DetailScreen) {
                val mp = MediaPlayer.create(activity, R.raw.click)
                mp.setOnCompletionListener {
                    mp.release()
                }
                mp.start()

                val startProgress = completedDays
                val endProgress = completedDays + 1
                ProgressBarAnimation(
                        activity.detailScreenProgressBar,
                        startProgress.toFloat(),
                        endProgress.toFloat()
                ).apply {
                    duration = if (withAnim) {
                        200
                    } else {
                        0
                    }
                    interpolator = LinearOutSlowInInterpolator()
                    activity.detailScreenProgressBar.startAnimation(this)
                }

                AnimatorSet().apply {
                    this.play(
                            ObjectAnimator.ofFloat(
                                    activity.detailScreenProgressPolygon2,
                                    "rotation",
                                    180.toFloat()
                            )
                    )
                    this.duration = if (withAnim) {
                        400
                    } else {
                        0
                    }
                    this.interpolator = BounceInterpolator()
                    this.start()
                }

                AnimatorSet().apply {
                    this.play(
                            ObjectAnimator.ofFloat(
                                    activity.detailScreenProgressPolygon3,
                                    "rotation",
                                    (180).toFloat()
                            )
                    )
                    this.duration = if (withAnim) {
                        400
                    } else {
                        0
                    }
                    this.interpolator = BounceInterpolator()
                    this.start()
                }

                activity.detailScreenProgressTS.setInAnimation(
                        activity.baseContext,
                        R.anim.slide_in_top
                )
                activity.detailScreenProgressTS.setOutAnimation(
                        activity.baseContext,
                        R.anim.slide_out_bottom
                )

                if (withAnim) {
                    activity.detailScreenProgressTS.setText((completedDays + 1).toString())
                } else {
                    activity.detailScreenProgressTS.setCurrentText((completedDays + 1).toString())
                }
                Log.e("TEST", (completedDays + 1).toString())

            }
        }
    }

    override fun showMsgTopSnackBar(resId: Int) {
        snackBar = TopSnackBar.make(activity.findViewById(R.id.containerRoot), resId, TopSnackBar.LENGTH_LONG)
        snackBar?.show()
    }

    private var snackBar: TopSnackBar? = null
    override fun hideTopSnackBar() {
        snackBar?.dismiss()
    }

    override fun showDialogRestartExpiredTarget() {
        Dialog.Builder().title(R.string.dialog_restart_target_title)
                .message(R.string.dialog_restart_expired_target_description)
                .btnNeg(R.string.dialog_restart_target_negative_btn)
                .btnPos(R.string.dialog_restart_target_positive_btn)
                .withAnimShow(true)
                .setListener(object : Dialog.Listener {
                    override fun onClickDialogPositiveBtn() {
                        clickRestartTarget.invoke()
                    }

                }).build()
                .show(activity.supportFragmentManager, DIALOG_TAG + "Restart")
    }

    override fun showDialogRestartCompletedTarget() {
        Dialog.Builder().title(R.string.dialog_restart_target_title)
                .message(R.string.dialog_restart_completed_target_description)
                .btnNeg(R.string.dialog_restart_target_negative_btn)
                .btnPos(R.string.dialog_restart_target_positive_btn)
                .withAnimShow(true)
                .setListener(object : Dialog.Listener {
                    override fun onClickDialogPositiveBtn() {
                        clickRestartTarget.invoke()
                    }

                }).build()
                .show(activity.supportFragmentManager, DIALOG_TAG + "Restart")
    }

    override fun showDialogDeleteTarget(nameTarget: String, id: Long) {
        Dialog.Builder().title(R.string.dialog_delete_target_title)
                .message(activity.getString(R.string.dialog_delete_target_description, nameTarget))
                .btnNeg(R.string.dialog_delete_target_negative_btn)
                .btnPos(R.string.dialog_delete_target_positive_btn)
                .setListener(object : Dialog.Listener {
                    override fun onClickDialogPositiveBtn() {
                        clickDeleteTarget.invoke(id)
                    }

                }).build()
                .show(activity.supportFragmentManager, DIALOG_TAG + "Delete")

    }

    override fun showRateDialog(successDays: Int) {
        if (settingsManager.needShowRateApp(successDays) && activity.supportFragmentManager.findFragmentByTag(DIALOG_TAG + "Rate") == null) {
            DialogRate.instance(object : DialogRate.Listener {
                override fun rateNow() {
                    openGooglePlay()
                    settingsManager.blockShowRateApp()
                }

                override fun notLikeApp() {
                    settingsManager.incrementCountShowRateApp()
                    if (settingsManager.countShowRateApp() > 1){
                        settingsManager.blockShowRateApp()
                    }
                }

            }).show(activity.supportFragmentManager, DIALOG_TAG + "Rate")
        }

    }

    override fun showDialogUpdateDownloaded() {
        Dialog.Builder().title(R.string.dialog_update_title).message(R.string.dialog_update_msg).btnNeg(
            R.string.dialog_update_negative_btn).btnPos(R.string.dialog_update_positive_btn).setListener(
            object : Dialog.Listener {
                override fun onClickDialogPositiveBtn() {
                    clickInstallUpdate.invoke()
                }
            }).build().show(activity.supportFragmentManager, DIALOG_TAG + "Update")
    }

    override fun openGooglePlay() {
        val appPackageName =
                activity.packageName // getPackageName() from Context or Activity object
        try {
            activity.startActivity(
                    Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("market://details?id=$appPackageName")
                    )
            )
        } catch (anfe: ActivityNotFoundException) {
            activity.startActivity(
                    Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                    )
            )
        }

    }

    override fun shareApp() {
        val builder =
                StrictMode.VmPolicy.Builder() //https://stackoverflow.com/questions/48117511/exposed-beyond-app-through-clipdata-item-geturi
        StrictMode.setVmPolicy(builder.build())
        val shareIntent = Intent(Intent.ACTION_SEND).apply {
            type = TYPE_TEXT
            putExtra(
                    Intent.EXTRA_TEXT,
                    activity.getString(R.string.screen_about_share_app_text, activity.packageName + "&referrer=utm_source%3Dshare-in-app")
            )
        }
        activity.startActivity(
                Intent.createChooser(
                        shareIntent,
                        activity.getString(R.string.screen_about_intent_chooser_share)
                )
        )
    }

    override fun contactUs() {
        val sbj = "Habit Feedback. [M:${Build.MANUFACTURER + " " + Build.MODEL}, S:${Build.VERSION.SDK_INT}, V:${BuildConfig.VERSION_CODE}]"

        val intent = Intent(Intent.ACTION_SENDTO)
        intent.data = Uri.parse("mailto:")

        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(INTENT_CONTACT_US_EMAIL))
        intent.putExtra(Intent.EXTRA_SUBJECT, sbj)

        try {
            //start email intent
            activity.startActivity(
                    Intent.createChooser(
                            intent,
                            activity.getString(R.string.screen_about_intent_chooser_contact_us)
                    )
            )
        } catch (e: Exception) {
            //if any thing goes wrong for example no email client application or any exception
            //get and show exception message
            e.message?.let { showToastNormal(it) }
        }
    }

    override fun getScreenForAnalytics(): Pair<AppCompatActivity, String?> {
        val nameScreen = when (fragment) {
            is DetailScreen -> {
                "DetailScreen"
            }
            is OnboardingScreen -> {
                "OnboardingScreen"
            }
            is MainScreen -> {
                "MainScreen"
            }
            else -> {
                null
            }
        }
        return Pair(activity, nameScreen)
    }


    override var clickRestartTarget: () -> Unit = {}
    override var clickDeleteTarget: (id: Long) -> Unit = {}
    override var clickInstallUpdate: () -> Unit = {}

    private fun hideKeyboard(activity: Activity) {
        val imm: InputMethodManager =
                activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = activity.currentFocus
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}