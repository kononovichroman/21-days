package com.habit.application.screen.detail

import android.app.Application
import android.text.format.DateFormat
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.AndroidViewModel
import com.habit.application.R
import com.habit.application.core.database.Status
import com.habit.application.core.database.TargetModel
import com.habit.application.core.database.repository.TargetRepositoryAPI
import com.habit.application.core.extensions.addOnPropertyChanged
import com.habit.application.core.helper.ThemeHelperAPI
import com.habit.application.core.managers.analytics.AnalyticsManagerAPI
import com.habit.application.core.reminder.ReminderSchedulerAPI
import com.habit.application.core.reminder.TimeModel
import com.habit.application.core.reminder.TimeType
import com.habit.application.screen.RouterAPI
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


/**
 * Created by Roman K. on 02.02.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */


class DetailViewModel(
    private val context: Application,
    private val themeHelper: ThemeHelperAPI,
    private val router: RouterAPI,
    private val repository: TargetRepositoryAPI,
    private val analyticsManager: AnalyticsManagerAPI,
    private val reminderScheduler: ReminderSchedulerAPI
) : AndroidViewModel(context) {
    val target = ObservableField<TargetModel>()
    val completedDays = ObservableInt()
    val timeNotificationsSelect = ObservableField<TimeModel>()
    private val is24HourFormat by lazy { DateFormat.is24HourFormat(context) }

    init {
        router.run {
            clickRestartTarget = {
                GlobalScope.launch {
                    target.get()?.id?.let { repository.repeat(it) }
                    target.set(target.get()?.id?.let { repository.targetById(it) })
                    target.get()?.completedDays?.let { completedDays.set(it) }
                    target.get()?.let {reminderScheduler.schedule(it) }
                }
            }
        }
        target.addOnPropertyChanged {
            it.get()?.hourNotification?.let { hour ->
                it.get()?.minuteNotification?.let { minute ->
                    if (is24HourFormat) {
                        timeNotificationsSelect.set(TimeModel(hour, minute, TimeType.TIME_TYPE_24))
                    } else {
                        timeNotificationsSelect.set(
                            TimeModel(
                                hour,
                                minute,
                                TimeType.TIME_TYPE_24
                            ).convert24toAmPm()
                        )
                    }
                }
            }
        }
    }

    fun start(targetModel: TargetModel) {
        target.set(targetModel)
        completedDays.set(targetModel.completedDays)
        router.getScreenForAnalytics().second?.let {
            analyticsManager.currentScreen(
                router.getScreenForAnalytics().first,
                it
            )
        }
    }

    fun onResume() {
        themeHelper.whiteBars()
        GlobalScope.launch {
            val targetFromRepo = target.get()?.id?.let { repository.targetById(it) }
            targetFromRepo?.let {
                target.set(it)
                completedDays.set(it.completedDays)
            }

        }
    }


    fun shortClick() {
        if (target.get()?.isCheckToday == false) {
            router.showMsgTopSnackBar(R.string.screen_detail_long_click_notification)
        }
    }

    fun longClick() {
        target.get()?.let {
            when (it.status) {
                Status.ACTIVE -> {
                    if (!it.isCheckToday) {
                        router.apply {
                            startAnimProgress(it.needDays, it.completedDays)
                            showRateDialog(it.completedDays)
                            if (it.completedDays + 1 == it.needDays) {
                                showMsgTopSnackBar(R.string.screen_detail_success_complete)
                            }
                        }

                        GlobalScope.launch {
                            repository.checkNewDay(it.id)
                            target.set(repository.targetById(it.id))
                        }
                        target.get()?.let {
                            reminderScheduler.hideNotification(it.id.toInt())
                        }
                    } else {
                        router.showMsgTopSnackBar(R.string.screen_detail_today_already_marked)
                    }

                }
                Status.ERROR -> {
                    router.showDialogRestartExpiredTarget()
                }
                Status.COMPLETED -> {
                    router.showDialogRestartCompletedTarget()
                }
            }
        }
    }

    fun onBackPressed() {
        router.hideTopSnackBar()
    }

    fun setTimeNotifications(hour: Int, minute: Int, ampm: String) {
        val type = when (ampm) {
            "AM" -> TimeType.TIME_TYPE_AM
            "PM" -> TimeType.TIME_TYPE_PM
            else -> TimeType.TIME_TYPE_24
        }
        timeNotificationsSelect.set(TimeModel(hour, minute, type))
        if (target.get() != null && timeNotificationsSelect.get() != null){
            GlobalScope.launch {
                repository.setNotification(target.get()!!.id, timeNotificationsSelect.get()!!)
                repository.targetById(target.get()!!.id)?.run {
                    target.set(this)
                    reminderScheduler.schedule(this)
                }
            }
        }
    }


}