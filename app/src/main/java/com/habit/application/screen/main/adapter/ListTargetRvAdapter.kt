package com.habit.application.screen.main.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_target.view.*
import com.habit.application.R
import com.habit.application.core.database.Status
import com.habit.application.core.database.TargetModel
import java.util.*


/**
 * Created by Roman K. on 28.01.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
class ListTargetRvAdapter(
    context: Context?,
    private val itemClickListener: ItemClickListener?
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private val list = ArrayList<TargetModel>()
    private val viewBinderHelper = ViewBinderHelper()


    init {
        viewBinderHelper.setOpenOnlyOne(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = inflater.inflate(R.layout.item_target, parent, false)

        return ViewHolder(view, itemClickListener)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun setList(l: List<TargetModel>) {

        when {
            list.size > l.size -> {
                //del
                list.filter { !l.contains(it) }.forEach {
                    notifyItemRemoved(list.indexOf(it))
                    list.clear()
                    list.addAll(l)

                }
            }
            list.isNotEmpty() && list.size < l.size -> {
                //add
                l.filter { !list.contains(it) }.forEach {
                    notifyItemInserted(l.indexOf(it))
                    list.clear()
                    list.addAll(l)
                }
            }
            list != l -> {
                list.clear()
                list.addAll(l)
                notifyDataSetChanged()
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        viewBinderHelper.bind(holder.itemView as SwipeRevealLayout, list[position].id.toString())
        (holder as ViewHolder).bind(list[position])
    }


    class ViewHolder(
        view: View,
        private val itemClickListener: ItemClickListener?
    ) : RecyclerView.ViewHolder(view) {

        fun bind(target: TargetModel) {
            itemView.itemTargetRoot.setOnClickListener { itemClickListener?.onItemClicked(target) }
            itemView.itemTargetDelete.setOnClickListener { itemClickListener?.onClickDelete(target) }

            itemView.itemTargetTitle.text = target.name
            when (target.status) {
                Status.ACTIVE -> {
                    itemView.itemTargetStatusDescription.text =
                        itemView.context.getString(R.string.main_screen_item_target_status_active)
                    itemView.itemTargetProcessActive.background = ContextCompat.getDrawable(
                        itemView.context,
                        R.drawable.active_bg_process_progressbar
                    )
                    itemView.itemTargetMain.alpha = 1f
                }
                Status.ERROR -> {
                    itemView.itemTargetStatusDescription.text =
                        itemView.context.getString(R.string.main_screen_item_target_status_error)
                    itemView.itemTargetProcessActive.background = ContextCompat.getDrawable(
                        itemView.context,
                        R.drawable.error_bg_process_progressbar
                    )
                    itemView.itemTargetMain.alpha = 1f
                }
                Status.COMPLETED -> {
                    itemView.itemTargetStatusDescription.text =
                        itemView.context.getString(R.string.main_screen_item_target_status_complete)
                    itemView.itemTargetProcessActive.background = ContextCompat.getDrawable(
                        itemView.context,
                        R.drawable.active_bg_process_progressbar
                    )
                    itemView.itemTargetMain.alpha = 0.4f
                }
            }

            itemView.itemTargetCountSuccessDay.text = target.completedDays.toString()
            itemView.itemTargetCountNeedDays.text = target.needDays.toString()

            (itemView.itemTargetProcessActive.layoutParams as ConstraintLayout.LayoutParams).matchConstraintPercentWidth =
                when {
                    target.completedDays <= target.needDays -> {
                        val calc = ((target.completedDays.toFloat()) / target.needDays.toFloat())
                        calc
                    }
                    else -> {
                        1f
                    }
                }
        }

    }

    interface ItemClickListener {
        fun onItemClicked(target: TargetModel)
        fun onClickDelete(target: TargetModel)
    }
}