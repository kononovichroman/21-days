package com.habit.application.screen.onboarding.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.annotation.StringRes
import kotlinx.android.synthetic.main.item_onboarding_page.view.*
import com.habit.application.R


/**
 * Created by Roman K. on 13.02.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
class OnboardingPageView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private lateinit var page: OnboardingContent
    private var listener: Listener? = null


    init {
        initUi()
    }

    private fun initUi() {
        LayoutInflater.from(context).inflate(R.layout.item_onboarding_page, this, true)
    }

    interface Listener {
        fun clickOnClose()
        fun clickRestore()
    }

    fun setOnClickListeners(l: Listener) {
        listener = l
    }

    fun setPageData(
        onboardingContent: OnboardingContent
    ) {
        this.page = onboardingContent

        onboardingPageImage.setImageResource(onboardingContent.logoResource)
        onboardingPageTitle.text = stringConcat(page.titleResource)
        onboardingPageDescription.text = stringConcat(page.descriptionResource)
        if (page.isLastElement) {

        }
    }

    private fun stringConcat(@StringRes values: Array<Int>): String {
        var concat = ""
        values.forEachIndexed { index, i ->
            concat += resources.getString(i)
            if (index + 1 != values.size) {
                concat += "\n"
            }
        }
        return concat
    }
}