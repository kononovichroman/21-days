package com.habit.application

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.habit.application.core.di.modulesApp
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin


/**
 * Created by Roman K. on 25.01.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
class App : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(modulesApp)
        }
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
    }

    companion object {
        var navbarHeight = -1
        var statusBarHeight = -1
    }
}