package com.habit.application.utils.rv.itemAnimator

import android.view.View
import androidx.core.view.ViewCompat

/**
 * Created by Roman K. on 12.02.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
object ViewHelper {
    fun clear(v: View) {
        v.alpha = 1f
        v.scaleY = 1f
        v.scaleX = 1f
        v.translationY = 0f
        v.translationX = 0f
        v.rotation = 0f
        v.rotationY = 0f
        v.rotationX = 0f
        v.pivotY = v.measuredHeight / 2.toFloat()
        v.pivotX = v.measuredWidth / 2.toFloat()
        ViewCompat.animate(v).setInterpolator(null).startDelay = 0
    }
}