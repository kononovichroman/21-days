package com.habit.application.utils.dialogs

import android.os.Bundle
import android.view.Gravity
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.habit.application.R
import com.habit.application.core.managers.analytics.AnalyticsManagerAPI
import kotlinx.android.synthetic.main.dialog_rate_layout.*
import org.koin.android.ext.android.inject


/**
 * Created by Roman K. on 09.02.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
class DialogRate : DialogFragment() {
    companion object {
        fun instance() =
            DialogRate()

        fun instance(listener: Listener) =
            DialogRate().apply {
                dialogListener = listener
            }
    }

    var dialogListener: Listener? = null
    private var star = 0
    private val analyticsManager: AnalyticsManagerAPI by inject()
    override fun onCreateDialog(savedInstanceState: Bundle?): android.app.Dialog {
        return activity?.let {
            AlertDialog.Builder(it)
                .setView(R.layout.dialog_rate_layout)
                .create()
        } ?: run {
            super.onCreateDialog(savedInstanceState)
        }
    }

    override fun show(manager: FragmentManager, tag: String?) {
        try {
            val ft: FragmentTransaction = manager.beginTransaction()
            ft.add(this, tag)
            ft.commit()
        } catch (e: IllegalStateException) {
        }
    }

    override fun onStart() {
        super.onStart()
        dialog?.let {
            it.setCanceledOnTouchOutside(false)

            it.dialogRateNegativeBtn?.setOnClickListener {
                analyticsManager.logEvent(AnalyticsManagerAPI.AnalyticsEvent.RATE_APP_LATER)
                dialogListener?.notLikeApp()
                dismiss()
            }
            it.dialogRatePositiveBtn?.setOnClickListener {
                analyticsManager.logEvent(AnalyticsManagerAPI.AnalyticsEvent.RATE_APP_NOW)
                dialogListener?.rateNow()
                dismiss()

            }

            val window = it.window
            val wlp = window?.attributes
            window?.setBackgroundDrawableResource(android.R.color.transparent)
            wlp?.gravity = Gravity.CENTER
            window?.attributes = wlp
        }
    }

    interface Listener {
        fun rateNow()
        fun notLikeApp()
    }


}