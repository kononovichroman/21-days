package com.habit.application.utils.topsnackbar

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.util.AttributeSet
import android.view.*
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.IntDef
import androidx.annotation.StringRes
import androidx.appcompat.widget.Toolbar
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.ViewCompat
import androidx.core.view.ViewPropertyAnimatorListenerAdapter
import com.google.android.material.behavior.SwipeDismissBehavior
import com.habit.application.App
import com.habit.application.R
import com.habit.application.utils.topsnackbar.AnimationUtils.INTERPOLATOR


class TopSnackBar private constructor(private val mParent: ViewGroup?) {
    abstract class Callback {
        @IntDef(
            DISMISS_EVENT_SWIPE,
            DISMISS_EVENT_ACTION,
            DISMISS_EVENT_TIMEOUT,
            DISMISS_EVENT_MANUAL,
            DISMISS_EVENT_CONSECUTIVE
        )
        @kotlin.annotation.Retention(AnnotationRetention.SOURCE)
        annotation class DismissEvent

        fun onDismissed() {}
        fun onShown() {}

        companion object {
            const val DISMISS_EVENT_SWIPE: Int = 0
            const val DISMISS_EVENT_ACTION: Int = 1
            const val DISMISS_EVENT_TIMEOUT: Int = 2
            const val DISMISS_EVENT_MANUAL: Int = 3
            const val DISMISS_EVENT_CONSECUTIVE: Int = 4
        }
    }

    @IntDef(
        LENGTH_INDEFINITE,
        LENGTH_SHORT,
        LENGTH_LONG
    )
    @kotlin.annotation.Retention(AnnotationRetention.SOURCE)
    annotation class Duration

    companion object {
        const val LENGTH_INDEFINITE: Int = -2
        const val LENGTH_SHORT: Int = -1
        const val LENGTH_LONG: Int = 0
        private const val ANIMATION_DURATION: Int = 350
        private const val ANIMATION_FADE_DURATION: Int = 280
        private var sHandler: Handler? = null
        private const val MSG_SHOW: Int = 0
        private const val MSG_DISMISS: Int = 1
        private fun make(
            view: View,
            text: CharSequence, @Duration duration: Int
        ): TopSnackBar {
            val snackBar = TopSnackBar(findSuitableParent(view))
            snackBar.setText(text)
            snackBar.setDuration(duration)
            return snackBar
        }

        fun make(view: View, @StringRes resId: Int, @Duration duration: Int): TopSnackBar {
            return make(
                view, view.resources
                    .getText(resId), duration
            )
        }

        private fun findSuitableParent(v: View): ViewGroup? {
            var view: View? = v
            var fallback: ViewGroup? = null
            do {
                if (view is CoordinatorLayout) {
                    return view
                } else if (view is FrameLayout) {
                    if (view.getId() == R.id.content) {
                        return view
                    } else {
                        fallback = view
                    }
                } else if (view is Toolbar || view is android.widget.Toolbar) { /*
                    If we return the toolbar here, the toast will be attached inside the toolbar.
                    So we need to find a some sibling ViewGroup to the toolbar that comes after the toolbar
                    If we don't find such view, the toast will be attached to the root activity view
                 */
                    if (view.parent is ViewGroup) {
                        val parent: ViewGroup = view.parent as ViewGroup
                        // check if there's something else beside toolbar
                        if (parent.childCount > 1) {
                            val childrenCnt: Int = parent.childCount
                            var toolbarIdx: Int
                            var i = 0
                            while (i < childrenCnt) {
                                // find the index of toolbar in the layout (most likely 0, but who knows)
                                if (parent.getChildAt(i) === view) {
                                    toolbarIdx = i
                                    // check if there's something else after the toolbar in the layout
                                    if (toolbarIdx < childrenCnt - 1) { //try to find some ViewGroup where you can attach the toast
                                        while (i < childrenCnt) {
                                            i++
                                            val vTemp: View = parent.getChildAt(i)
                                            if (vTemp is ViewGroup) return vTemp
                                        }
                                    }
                                    break
                                }
                                i++
                            }
                        }
                    }
                    //                return (ViewGroup) view;
                }
                if (view != null) {
                    val parent: ViewParent = view.parent
                    view = if (parent is View) parent else null
                }
            } while (view != null)
            return fallback
        }

        init {
            sHandler =
                Handler(Looper.getMainLooper(), object : Handler.Callback {
                    override fun handleMessage(message: Message): Boolean {
                        when (message.what) {
                            MSG_SHOW -> {
                                (message.obj as TopSnackBar).showView()
                                return true
                            }
                            MSG_DISMISS -> {
                                (message.obj as TopSnackBar).hideView()
                                return true
                            }
                        }
                        return false
                    }
                })
        }
    }

    private val mContext: Context?
    private val mView: SnackBarLayout
    @get:Duration
    var duration: Int = 0
        private set
    private var mCallback: Callback? = null

    fun setText(message: CharSequence): TopSnackBar {
        mView.messageView?.text = message
        return this
    }

    fun setText(@StringRes resId: Int): TopSnackBar {
        return setText(mContext?.getText(resId) ?: "")
    }

    fun setDuration(@Duration duration: Int): TopSnackBar {
        this.duration = duration
        return this
    }

    val view: View
        get() = mView

    fun show() {
        SnackbarManager.instance
            ?.show(duration, mManagerCallback)
    }

    fun dismiss() {
        dispatchDismiss(Callback.DISMISS_EVENT_MANUAL)
    }

    private fun dispatchDismiss(@Callback.DismissEvent event: Int) {
        SnackbarManager.instance
            ?.dismiss(mManagerCallback, event)
    }

    fun setCallback(callback: Callback?): TopSnackBar {
        mCallback = callback
        return this
    }

    val isShown: Boolean
        get() = SnackbarManager.instance?.isCurrent(mManagerCallback) ?: false

    val isShownOrQueued: Boolean
        get() {
            return SnackbarManager.instance?.isCurrentOrNext(mManagerCallback) ?: false
        }

    private val mManagerCallback: SnackbarManager.Callback = object : SnackbarManager.Callback {
        override fun show() {
            sHandler?.let {
                it.sendMessage(
                    it.obtainMessage(
                        MSG_SHOW,
                        this@TopSnackBar
                    )
                )
            }
        }

        override fun dismiss(event: Int) {
            sHandler?.let {
                it.sendMessage(
                    it.obtainMessage(
                        MSG_DISMISS,
                        event,
                        0,
                        this@TopSnackBar
                    )
                )
            }
        }
    }

    fun showView() {
        if (mView.parent == null) {
            val lp: ViewGroup.LayoutParams = mView.layoutParams
            if (lp is CoordinatorLayout.LayoutParams) {
                val behavior = Behavior()
                behavior.setStartAlphaSwipeDistance(0.1f)
                behavior.setEndAlphaSwipeDistance(0.6f)
                behavior.setSwipeDirection(SwipeDismissBehavior.SWIPE_DIRECTION_START_TO_END)
                behavior.listener = object : SwipeDismissBehavior.OnDismissListener {
                    override fun onDismiss(view: View) {
                        dispatchDismiss(Callback.DISMISS_EVENT_SWIPE)
                    }

                    override fun onDragStateChanged(state: Int) {
                        when (state) {
                            SwipeDismissBehavior.STATE_DRAGGING, SwipeDismissBehavior.STATE_SETTLING -> SnackbarManager.instance
                                ?.cancelTimeout(mManagerCallback)
                            SwipeDismissBehavior.STATE_IDLE -> SnackbarManager.instance
                                ?.restoreTimeout(mManagerCallback)
                        }
                    }
                }
                lp.behavior = behavior
            }
            mParent?.addView(mView)
        }
        mView.setOnAttachStateChangeListener(object :
            SnackBarLayout.OnAttachStateChangeListener {
            override fun onViewAttachedToWindow(v: View?) {}
            override fun onViewDetachedFromWindow(v: View?) {
                if (isShownOrQueued) {
                    sHandler?.post { onViewHidden() }
                }
            }
        })
        mView.setPadding(0, App.statusBarHeight, 0, 0)
        if (ViewCompat.isLaidOut(mView)) {
            animateViewIn()
        } else {
            mView.setOnLayoutChangeListener(object :
                SnackBarLayout.OnLayoutChangeListener {
                override fun onLayoutChange(
                    view: View?,
                    left: Int,
                    top: Int,
                    right: Int,
                    bottom: Int
                ) {
                    animateViewIn()
                    mView.setOnLayoutChangeListener(null)
                }
            })
        }
    }

    private fun animateViewIn() {
        mView.translationY = -mView.height.toFloat()
        ViewCompat.animate(mView)
            .translationY(0f)
            .setInterpolator(INTERPOLATOR)
            .setDuration(ANIMATION_DURATION.toLong())
            .setListener(object : ViewPropertyAnimatorListenerAdapter() {
                override fun onAnimationStart(view: View) {
                    mView.animateChildrenIn(
                        ANIMATION_DURATION - ANIMATION_FADE_DURATION,
                        ANIMATION_FADE_DURATION
                    )
                }

                override fun onAnimationEnd(view: View) {
                    mCallback?.onShown()
                    SnackbarManager.instance
                        ?.onShown(mManagerCallback)
                }
            })
            .start()
    }

    private fun animateViewOut() {
        ViewCompat.animate(mView)
            .translationY(-mView.height.toFloat())
            .setInterpolator(INTERPOLATOR)
            .setDuration(ANIMATION_DURATION.toLong())
            .setListener(object : ViewPropertyAnimatorListenerAdapter() {
                override fun onAnimationStart(view: View) {
                    mView.animateChildrenOut(0, ANIMATION_FADE_DURATION)
                }

                override fun onAnimationEnd(view: View) {
                    onViewHidden()
                }
            })
            .start()
    }

    fun hideView() {
        if (mView.visibility != View.VISIBLE || isBeingDragged) {
            onViewHidden()
        } else {
            animateViewOut()
        }
    }

    private fun onViewHidden() {
        SnackbarManager.instance?.onDismissed(mManagerCallback)
        mCallback?.onDismissed()
        val parent: ViewParent? = mView.parent
        if (parent is ViewGroup) {
            parent.removeView(mView)
        }
    }

    private val isBeingDragged: Boolean
        get() {
            val lp: ViewGroup.LayoutParams = mView.layoutParams
            if (lp is CoordinatorLayout.LayoutParams) {
                val behavior: CoordinatorLayout.Behavior<*>? = lp.behavior
                if (behavior is SwipeDismissBehavior<*>) {
                    return (behavior.dragState
                            != SwipeDismissBehavior.STATE_IDLE)
                }
            }
            return false
        }

    class SnackBarLayout @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null
    ) :
        LinearLayout(context, attrs) {
        var messageView: TextView? = null
            private set

        interface OnLayoutChangeListener {
            fun onLayoutChange(
                view: View?,
                left: Int,
                top: Int,
                right: Int,
                bottom: Int
            )
        }

        interface OnAttachStateChangeListener {
            fun onViewAttachedToWindow(v: View?)
            fun onViewDetachedFromWindow(v: View?)
        }

        private var mOnLayoutChangeListener: OnLayoutChangeListener? = null
        private var mOnAttachStateChangeListener: OnAttachStateChangeListener? =
            null

        override fun onFinishInflate() {
            super.onFinishInflate()
            messageView = findViewById<View>(R.id.snackbar_text) as TextView
        }


        fun animateChildrenIn(delay: Int, duration: Int) {
            messageView?.let {
                alpha = 1f
                ViewCompat.animate((it))
                    .alpha(1f)
                    .setDuration(duration.toLong())
                    .setStartDelay(delay.toLong())
                    .start()
            }
        }

        fun animateChildrenOut(delay: Int, duration: Int) {
            messageView?.let {
                alpha = 1f
                ViewCompat.animate((it))
                    .alpha(0f)
                    .setDuration(duration.toLong())
                    .setStartDelay(delay.toLong())
                    .start()
            }
        }

        override fun onLayout(
            changed: Boolean,
            l: Int,
            t: Int,
            r: Int,
            b: Int
        ) {
            super.onLayout(changed, l, t, r, b)
            mOnLayoutChangeListener?.onLayoutChange(this, l, t, r, b)
        }

        override fun onAttachedToWindow() {
            super.onAttachedToWindow()
            mOnAttachStateChangeListener?.onViewAttachedToWindow(this)
        }

        override fun onDetachedFromWindow() {
            super.onDetachedFromWindow()
            mOnAttachStateChangeListener?.onViewDetachedFromWindow(this)
        }

        fun setOnLayoutChangeListener(onLayoutChangeListener: OnLayoutChangeListener?) {
            mOnLayoutChangeListener = onLayoutChangeListener
        }

        fun setOnAttachStateChangeListener(listener: OnAttachStateChangeListener?) {
            mOnAttachStateChangeListener = listener
        }

        init {
            isClickable = true
            LayoutInflater.from(context)
                .inflate(R.layout.tsnackbar_layout, this)
            ViewCompat.setAccessibilityLiveRegion(
                this,
                ViewCompat.ACCESSIBILITY_LIVE_REGION_POLITE
            )
        }
    }

    internal inner class Behavior : SwipeDismissBehavior<SnackBarLayout>() {
        override fun canSwipeDismissView(child: View): Boolean {
            return child is SnackBarLayout
        }

        override fun onInterceptTouchEvent(
            parent: CoordinatorLayout, child: SnackBarLayout,
            event: MotionEvent
        ): Boolean {
            if (parent.isPointInChildBounds(child, event.x.toInt(), event.y.toInt())) {
                when (event.actionMasked) {
                    MotionEvent.ACTION_DOWN -> SnackbarManager.instance?.cancelTimeout(
                        mManagerCallback
                    )
                    MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> SnackbarManager.instance?.restoreTimeout(
                        mManagerCallback
                    )
                }
            }
            return super.onInterceptTouchEvent(parent, child, event)
        }
    }

    init {
        mContext = mParent?.context
        val inflater: LayoutInflater = LayoutInflater.from(mContext)
        mView =
            inflater.inflate(R.layout.tsnackbar_layout_include, mParent, false) as SnackBarLayout
    }
}