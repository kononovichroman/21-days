package com.habit.application.utils.rv.itemAnimator

import android.view.animation.OvershootInterpolator
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView


/**
 * Created by Roman K. on 12.02.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
class OvershootAnimator :
    BaseItemAnimator {
    private val tension: Float

    constructor() {
        tension = 1.0f
    }

    constructor(mTension: Float) {
        this.tension = mTension
    }

    override fun animateRemoveImpl(holder: RecyclerView.ViewHolder?) {
        ViewCompat.animate(holder!!.itemView)
            .translationX(-holder.itemView.rootView.width.toFloat())
            .setDuration(removeDuration)
            .setListener(DefaultRemoveVpaListener(holder))
            .setStartDelay(getRemoveDelay(holder))
            .start()
    }

    override fun preAnimateAddImpl(holder: RecyclerView.ViewHolder?) {
        holder?.itemView?.let {
            it.translationX = -holder.itemView.rootView.width.toFloat()
        }
    }

    override fun animateAddImpl(holder: RecyclerView.ViewHolder?) {
        ViewCompat.animate(holder!!.itemView)
            .translationX(0f)
            .setDuration(addDuration)
            .setListener(DefaultAddVpaListener(holder))
            .setInterpolator(OvershootInterpolator(tension))
            .setStartDelay(getAddDelay(holder))
            .start()
    }
}