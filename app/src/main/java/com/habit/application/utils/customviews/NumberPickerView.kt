package com.habit.application.utils.customviews

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Paint.Align
import android.graphics.Typeface
import android.os.Handler
import android.os.HandlerThread
import android.os.Message
import android.text.TextPaint
import android.text.TextUtils
import android.text.TextUtils.TruncateAt
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.VelocityTracker
import android.view.View
import android.view.ViewConfiguration
import android.widget.Scroller
import com.habit.application.R
import java.util.concurrent.ConcurrentHashMap
import kotlin.math.abs


/**
 * Created by Roman K. on 04.05.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */

typealias OnValueChangeListenerInScrolling = (NumberPickerView? , Int, Int) -> Unit
@Suppress("unused")
class NumberPickerView : View {
    private var textColorNormal = DEFAULT_TEXT_COLOR_NORMAL
    private var textColorSelected = DEFAULT_TEXT_COLOR_SELECTED
    private var textColorHint = DEFAULT_TEXT_COLOR_SELECTED
    private var textSizeNormal = 0
    private var textSizeSelected = 0
    private var textSizeHint = 0
    private var widthOfHintText = 0
    private var widthOfAlterHint = 0
    private var marginStartOfHint = 0
    private var marginEndOfHint = 0
    private var itemPaddingVertical = 0
    private var itemPaddingHorizontal = 0
    private var dividerColor = DEFAULT_DIVIDER_COLOR
    private var dividerHeight = DEFAULT_DIVIDER_HEIGHT
    private var dividerMarginL = DEFAULT_DIVIDER_MARGIN_HORIZONTAL
    private var dividerMarginR = DEFAULT_DIVIDER_MARGIN_HORIZONTAL
    private var shownCount = DEFAULT_SHOWN_COUNT
    private var dividerIndex0 = 0
    private var dividerIndex1 = 0
    private var minShowIndex = -1
    private var maxShowIndex = -1
    //compat for android.widget.NumberPicker
    private var minValue = 0
    //compat for android.widget.NumberPicker
    private var maxValue = 0
    private var maxWidthOfDisplayedValues = 0
    private var maxHeightOfDisplayedValues = 0
    private var maxWidthOfAlterArrayWithMeasureHint = 0
    private var maxWidthOfAlterArrayWithoutMeasureHint = 0
    private var prevPickedIndex = 0
    private var miniVelocityFling = 150
    private var scaledTouchSlop = 8
    private var hintText: String? = null
    private var textEllipsize: String? = null
    private var emptyItemHint: String? = null
    private var alterHint: String? = null
    //friction used by scroller when fling
    private var friction = 1f
    private var textSizeNormalCenterYOffset = 0f
    private var textSizeSelectedCenterYOffset = 0f
    private var textSizeHintCenterYOffset = 0f
    //true to show the two dividers
    private var showDivider = DEFAULT_SHOW_DIVIDER
    //true to wrap the displayed values
    private var wrapSelectorWheel =
        DEFAULT_WRAP_SELECTOR_WHEEL
    //true to set to the current position, false set position to 0
    private var currentItemIndexEffect =
        DEFAULT_CURRENT_ITEM_INDEX_EFFECT
    //true if NumberPickerView has initialized
    private var hasInit = false
    // if displayed values' number is less than show count, then this value will be false.
    private var wrapSelectorWheelCheck = true
    // if you want you set to linear mode from wrap mode when scrolling, then this value will be true.
    private var pendingWrapToLinear = false
    // if this view is used in same dialog or PopupWindow more than once, and there are several
    // NumberPickerViews linked, such as Gregorian Calendar with MonthPicker and DayPicker linked,
    // set mRespondChangeWhenDetach true to respond onValueChanged callbacks if this view is scrolling
    // when detach from window, but this solution is unlovely and may cause NullPointerException
    // (even i haven't found this NullPointerException),
    // so I highly recommend that every time setting up a reusable dialog with a NumberPickerView in it,
    // please initialize NumberPickerView's data, and in this way, you can set mRespondChangeWhenDetach false.
    private var respondChangeOnDetach =
        DEFAULT_RESPOND_CHANGE_ON_DETACH
    // this is to set which thread to respond onChange... listeners including
    // OnValueChangeListener, OnValueChangeListenerRelativeToRaw and OnScrollListener when view is
    // scrolling or starts to scroll or stops scrolling.
    private var respondChangeInMainThread =
        DEFAULT_RESPOND_CHANGE_IN_MAIN_THREAD
    private lateinit var scroller: Scroller
    private var velocityTracker: VelocityTracker? = null
    private val paintDivider = Paint()
    private val paintText: TextPaint? = TextPaint()
    private val paintHint: Paint? = Paint()
    private var displayedValues: Array<String?>? = null
    private var alterTextArrayWithMeasureHint: Array<CharSequence?>? = null
    private var alterTextArrayWithoutMeasureHint: Array<CharSequence?>? = null
    private lateinit var handlerThread: HandlerThread
    private var handlerInNewThread: Handler? = null
    private var handlerInMainThread: Handler? = null
    private val textWidthCache: MutableMap<String, Int?> =
        ConcurrentHashMap()

    // compatible for NumberPicker
    interface OnValueChangeListener {
        fun onValueChange(picker: NumberPickerView?, oldVal: Int, newVal: Int)
    }

    interface OnValueChangeListenerRelativeToRaw {
        fun onValueChangeRelativeToRaw(
            picker: NumberPickerView?, oldPickedIndex: Int, newPickedIndex: Int,
            displayedValues: Array<String?>?
        )
    }

//    interface OnValueChangeListenerInScrolling {
//        fun onValueChangeInScrolling(
//            picker: NumberPickerView?,
//            oldVal: Int,
//            newVal: Int
//        )
//    }

    // compatible for NumberPicker
    interface OnScrollListener {
        fun onScrollStateChange(view: NumberPickerView?, scrollState: Int)

        companion object {
            const val SCROLL_STATE_IDLE = 0
            const val SCROLL_STATE_TOUCH_SCROLL = 1
            const val SCROLL_STATE_FLING = 2
        }
    }

    private var mOnValueChangeListenerRaw: OnValueChangeListenerRelativeToRaw? = null
    private var mOnValueChangeListener //compatible for NumberPicker
            : OnValueChangeListener? = null
    private var mOnScrollListener //compatible for NumberPicker
            : OnScrollListener? = null
    private var mOnValueChangeListenerInScrolling //response onValueChanged in scrolling
            : OnValueChangeListenerInScrolling? = null
    // The current scroll state of the NumberPickerView.
    private var mScrollState = OnScrollListener.SCROLL_STATE_IDLE

    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?
    ) : super(context, attrs) {
        initAttr(context, attrs)
        init(context)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) : super(context, attrs, defStyleAttr) {
        initAttr(context, attrs)
        init(context)
    }

    private fun initAttr(
        context: Context,
        attrs: AttributeSet?
    ) {
        if (attrs == null) {
            return
        }
        val a = context.obtainStyledAttributes(attrs, R.styleable.NumberPickerView)
        val n = a.indexCount
        for (i in 0 until n) {
            when (val attr = a.getIndex(i)) {
                R.styleable.NumberPickerView_npv_ShownCount -> {
                    shownCount = a.getInt(attr, DEFAULT_SHOWN_COUNT)
                }
                R.styleable.NumberPickerView_npv_DividerColor -> {
                    dividerColor = a.getColor(attr, DEFAULT_DIVIDER_COLOR)
                }
                R.styleable.NumberPickerView_npv_DividerHeight -> {
                    dividerHeight =
                        a.getDimensionPixelSize(attr, DEFAULT_DIVIDER_HEIGHT)
                }
                R.styleable.NumberPickerView_npv_DividerMarginLeft -> {
                    dividerMarginL = a.getDimensionPixelSize(
                        attr,
                        DEFAULT_DIVIDER_MARGIN_HORIZONTAL
                    )
                }
                R.styleable.NumberPickerView_npv_DividerMarginRight -> {
                    dividerMarginR = a.getDimensionPixelSize(
                        attr,
                        DEFAULT_DIVIDER_MARGIN_HORIZONTAL
                    )
                }
                R.styleable.NumberPickerView_npv_TextArray -> {
                    displayedValues = convertCharSequenceArrayToStringArray(a.getTextArray(attr))
                }
                R.styleable.NumberPickerView_npv_TextColorNormal -> {
                    textColorNormal =
                        a.getColor(attr, DEFAULT_TEXT_COLOR_NORMAL)
                }
                R.styleable.NumberPickerView_npv_TextColorSelected -> {
                    textColorSelected =
                        a.getColor(attr, DEFAULT_TEXT_COLOR_SELECTED)
                }
                R.styleable.NumberPickerView_npv_TextColorHint -> {
                    textColorHint =
                        a.getColor(attr, DEFAULT_TEXT_COLOR_SELECTED)
                }
                R.styleable.NumberPickerView_npv_TextSizeNormal -> {
                    textSizeNormal = a.getDimensionPixelSize(
                        attr,
                        sp2px(context, DEFAULT_TEXT_SIZE_NORMAL_SP.toFloat())
                    )
                }
                R.styleable.NumberPickerView_npv_TextSizeSelected -> {
                    textSizeSelected = a.getDimensionPixelSize(
                        attr,
                        sp2px(
                            context,
                            DEFAULT_TEXT_SIZE_SELECTED_SP.toFloat()
                        )
                    )
                }
                R.styleable.NumberPickerView_npv_TextSizeHint -> {
                    textSizeHint = a.getDimensionPixelSize(
                        attr,
                        sp2px(context, DEFAULT_TEXT_SIZE_HINT_SP.toFloat())
                    )
                }
                R.styleable.NumberPickerView_npv_MinValue -> {
                    minShowIndex = a.getInteger(attr, 0)
                }
                R.styleable.NumberPickerView_npv_MaxValue -> {
                    maxShowIndex = a.getInteger(attr, 0)
                }
                R.styleable.NumberPickerView_npv_WrapSelectorWheel -> {
                    wrapSelectorWheel =
                        a.getBoolean(attr, DEFAULT_WRAP_SELECTOR_WHEEL)
                }
                R.styleable.NumberPickerView_npv_ShowDivider -> {
                    showDivider = a.getBoolean(attr, DEFAULT_SHOW_DIVIDER)
                }
                R.styleable.NumberPickerView_npv_HintText -> {
                    hintText = a.getString(attr)
                }
                R.styleable.NumberPickerView_npv_AlternativeHint -> {
                    alterHint = a.getString(attr)
                }
                R.styleable.NumberPickerView_npv_EmptyItemHint -> {
                    emptyItemHint = a.getString(attr)
                }
                R.styleable.NumberPickerView_npv_MarginStartOfHint -> {
                    marginStartOfHint = a.getDimensionPixelSize(
                        attr,
                        dp2px(
                            context,
                            DEFAULT_MARGIN_START_OF_HINT_DP.toFloat()
                        )
                    )
                }
                R.styleable.NumberPickerView_npv_MarginEndOfHint -> {
                    marginEndOfHint = a.getDimensionPixelSize(
                        attr,
                        dp2px(
                            context,
                            DEFAULT_MARGIN_END_OF_HINT_DP.toFloat()
                        )
                    )
                }
                R.styleable.NumberPickerView_npv_ItemPaddingVertical -> {
                    itemPaddingVertical = a.getDimensionPixelSize(
                        attr,
                        dp2px(context, DEFAULT_ITEM_PADDING_DP_V.toFloat())
                    )
                }
                R.styleable.NumberPickerView_npv_ItemPaddingHorizontal -> {
                    itemPaddingHorizontal = a.getDimensionPixelSize(
                        attr,
                        dp2px(context, DEFAULT_ITEM_PADDING_DP_H.toFloat())
                    )
                }
                R.styleable.NumberPickerView_npv_AlternativeTextArrayWithMeasureHint -> {
                    alterTextArrayWithMeasureHint = a.getTextArray(attr)
                }
                R.styleable.NumberPickerView_npv_AlternativeTextArrayWithoutMeasureHint -> {
                    alterTextArrayWithoutMeasureHint = a.getTextArray(attr)
                }
                R.styleable.NumberPickerView_npv_RespondChangeOnDetached -> {
                    respondChangeOnDetach =
                        a.getBoolean(attr, DEFAULT_RESPOND_CHANGE_ON_DETACH)
                }
                R.styleable.NumberPickerView_npv_RespondChangeInMainThread -> {
                    respondChangeInMainThread = a.getBoolean(
                        attr,
                        DEFAULT_RESPOND_CHANGE_IN_MAIN_THREAD
                    )
                }
                R.styleable.NumberPickerView_npv_TextEllipsize -> {
                    textEllipsize = a.getString(attr)
                }
            }
        }
        a.recycle()
    }

    private fun init(context: Context) {
        scroller = Scroller(context)
        miniVelocityFling = ViewConfiguration.get(getContext()).scaledMinimumFlingVelocity
        scaledTouchSlop = ViewConfiguration.get(getContext()).scaledTouchSlop
        if (textSizeNormal == 0) {
            textSizeNormal =
                sp2px(context, DEFAULT_TEXT_SIZE_NORMAL_SP.toFloat())
        }
        if (textSizeSelected == 0) {
            textSizeSelected =
                sp2px(context, DEFAULT_TEXT_SIZE_SELECTED_SP.toFloat())
        }
        if (textSizeHint == 0) {
            textSizeHint =
                sp2px(context, DEFAULT_TEXT_SIZE_HINT_SP.toFloat())
        }
        if (marginStartOfHint == 0) {
            marginStartOfHint =
                dp2px(context, DEFAULT_MARGIN_START_OF_HINT_DP.toFloat())
        }
        if (marginEndOfHint == 0) {
            marginEndOfHint =
                dp2px(context, DEFAULT_MARGIN_END_OF_HINT_DP.toFloat())
        }
        paintDivider.apply {
            color = dividerColor
            isAntiAlias = true
            style = Paint.Style.STROKE
            strokeWidth = dividerHeight.toFloat()
        }
        paintText?.apply {
            color = textColorNormal
            isAntiAlias = true
            textAlign = Align.CENTER
        }

        paintHint?.apply {
            color = textColorHint
            isAntiAlias = true
            textAlign = Align.CENTER
            textSize = textSizeHint.toFloat()
        }
        if (shownCount % 2 == 0) {
            shownCount++
        }
        if (minShowIndex == -1 || maxShowIndex == -1) {
            updateValueForInit()
        }
        initHandler()
    }

    private fun initHandler() {
        handlerThread = HandlerThread("HandlerThread-For-Refreshing")
        handlerThread.start()
        handlerInNewThread = object : Handler(handlerThread.looper) {
            override fun handleMessage(msg: Message) {
                when (msg.what) {
                    HANDLER_WHAT_REFRESH -> if (!scroller.isFinished) {
                        if (mScrollState == OnScrollListener.SCROLL_STATE_IDLE) {
                            onScrollStateChange(OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                        }
                        handlerInNewThread!!.sendMessageDelayed(
                            getMsg(
                                HANDLER_WHAT_REFRESH,
                                0,
                                0,
                                msg.obj
                            ), HANDLER_INTERVAL_REFRESH.toLong()
                        )
                    } else {
                        var duration = 0
                        val willPickIndex: Int
                        //if scroller finished(not scrolling), then adjust the position
                        if (mCurrDrawFirstItemY != 0) { //need to adjust
                            if (mScrollState == OnScrollListener.SCROLL_STATE_IDLE) {
                                onScrollStateChange(OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                            }
                            if (mCurrDrawFirstItemY < -mItemHeight / 2) { //adjust to scroll upward
                                duration =
                                    (DEFAULT_INTERVAL_REVISE_DURATION.toFloat() * (mItemHeight + mCurrDrawFirstItemY) / mItemHeight).toInt()
                                scroller.startScroll(
                                    0,
                                    mCurrDrawGlobalY,
                                    0,
                                    mItemHeight + mCurrDrawFirstItemY,
                                    duration * 3
                                )
                                willPickIndex =
                                    getWillPickIndexByGlobalY(mCurrDrawGlobalY + mItemHeight + mCurrDrawFirstItemY)
                            } else { //adjust to scroll downward
                                duration =
                                    (DEFAULT_INTERVAL_REVISE_DURATION.toFloat() * -mCurrDrawFirstItemY / mItemHeight).toInt()
                                scroller.startScroll(
                                    0,
                                    mCurrDrawGlobalY,
                                    0,
                                    mCurrDrawFirstItemY,
                                    duration * 3
                                )
                                willPickIndex =
                                    getWillPickIndexByGlobalY(mCurrDrawGlobalY + mCurrDrawFirstItemY)
                            }
                            postInvalidate()
                        } else {
                            onScrollStateChange(OnScrollListener.SCROLL_STATE_IDLE)
                            //get the index which will be selected
                            willPickIndex = getWillPickIndexByGlobalY(mCurrDrawGlobalY)
                        }
                        val changeMsg = getMsg(
                            HANDLER_WHAT_LISTENER_VALUE_CHANGED,
                            prevPickedIndex,
                            willPickIndex,
                            msg.obj
                        )
                        if (respondChangeInMainThread) {
                            handlerInMainThread!!.sendMessageDelayed(
                                changeMsg,
                                duration * 2.toLong()
                            )
                        } else {
                            handlerInNewThread!!.sendMessageDelayed(
                                changeMsg,
                                duration * 2.toLong()
                            )
                        }
                    }
                    HANDLER_WHAT_LISTENER_VALUE_CHANGED -> respondPickedValueChanged(
                        msg.arg1,
                        msg.arg2,
                        msg.obj
                    )
                }
            }
        }
        handlerInMainThread = object : Handler() {
            override fun handleMessage(msg: Message) {
                super.handleMessage(msg)
                when (msg.what) {
                    HANDLER_WHAT_REQUEST_LAYOUT -> requestLayout()
                    HANDLER_WHAT_LISTENER_VALUE_CHANGED -> respondPickedValueChanged(
                        msg.arg1,
                        msg.arg2,
                        msg.obj
                    )
                }
            }
        }
    }

    private var mInScrollingPickedOldValue = 0
    private var mInScrollingPickedNewValue = 0
    private fun respondPickedValueChangedInScrolling(oldVal: Int, newVal: Int) {
        mOnValueChangeListenerInScrolling!!.invoke(this, oldVal, newVal)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        updateMaxWHOfDisplayedValues(false)
        setMeasuredDimension(
            measureWidth(widthMeasureSpec),
            measureHeight(heightMeasureSpec)
        )
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        mViewWidth = w
        mViewHeight = h
        mItemHeight = mViewHeight / shownCount
        mViewCenterX = (mViewWidth + paddingLeft - paddingRight).toFloat() / 2
        var defaultValue = 0
        if (getOneRecycleSize() > 1) {
            defaultValue = when {
                hasInit -> {
                    value() - minValue
                }
                currentItemIndexEffect -> {
                    mCurrDrawFirstItemIndex + (shownCount - 1) / 2
                }
                else -> {
                    0
                }
            }
        }
        correctPositionByDefaultValue(defaultValue, wrapSelectorWheel && wrapSelectorWheelCheck)
        updateFontAttr()
        updateNotWrapYLimit()
        updateDividerAttr()
        hasInit = true
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        if (!handlerThread.isAlive) {
            initHandler()
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        handlerThread.quit()
        //These codes are for dialog or PopupWindow which will be used for more than once.
        //Not an elegant solution, if you have any good idea, please let me know, thank you.
        if (mItemHeight == 0) {
            return
        }
        if (!scroller.isFinished) {
            scroller.abortAnimation()
            mCurrDrawGlobalY = scroller.currY
            calculateFirstItemParameterByGlobalY()
            if (mCurrDrawFirstItemY != 0) {
                mCurrDrawGlobalY = if (mCurrDrawFirstItemY < -mItemHeight / 2) {
                    mCurrDrawGlobalY + mItemHeight + mCurrDrawFirstItemY
                } else {
                    mCurrDrawGlobalY + mCurrDrawFirstItemY
                }
                calculateFirstItemParameterByGlobalY()
            }
            onScrollStateChange(OnScrollListener.SCROLL_STATE_IDLE)
        }
        // see the comments on mRespondChangeOnDetach, if mRespondChangeOnDetach is false,
// please initialize NumberPickerView's data every time setting up NumberPickerView,
// set the demo of GregorianLunarCalendar
        val currPickedIndex = getWillPickIndexByGlobalY(mCurrDrawGlobalY)
        if (currPickedIndex != prevPickedIndex && respondChangeOnDetach) {
            try {
                if (mOnValueChangeListener != null) {
                    mOnValueChangeListener!!.onValueChange(
                        this@NumberPickerView,
                        prevPickedIndex + minValue,
                        currPickedIndex + minValue
                    )
                }
                if (mOnValueChangeListenerRaw != null) {
                    mOnValueChangeListenerRaw!!.onValueChangeRelativeToRaw(
                        this@NumberPickerView,
                        prevPickedIndex,
                        currPickedIndex,
                        displayedValues
                    )
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        prevPickedIndex = currPickedIndex
    }

    fun getOneRecycleSize(): Int {
        return maxShowIndex - minShowIndex + 1
    }

    fun getRawContentSize(): Int {
        return if (displayedValues != null) {
            displayedValues!!.size
        } else 0
    }

    fun setDisplayedValuesAndPickedIndex(
        newDisplayedValues: Array<String?>?,
        pickedIndex: Int,
        needRefresh: Boolean
    ) {
        stopScrolling()
        requireNotNull(newDisplayedValues) { "newDisplayedValues should not be null." }
        require(pickedIndex >= 0) { "pickedIndex should not be negative, now pickedIndex is $pickedIndex" }
        updateContent(newDisplayedValues)
        updateMaxWHOfDisplayedValues(true)
        updateNotWrapYLimit()
        updateValue()
        prevPickedIndex = pickedIndex + minShowIndex
        correctPositionByDefaultValue(pickedIndex, wrapSelectorWheel && wrapSelectorWheelCheck)
        if (needRefresh) {
            handlerInNewThread!!.sendMessageDelayed(
                getMsg(HANDLER_WHAT_REFRESH),
                0
            )
            postInvalidate()
        }
    }

    fun displayedValues(
        newDisplayedValues: Array<String?>?,
        needRefresh: Boolean
    ) {
        setDisplayedValuesAndPickedIndex(newDisplayedValues, 0, needRefresh)
    }

    fun displayedValues(newDisplayedValues: Array<String?>?) {
        stopRefreshing()
        stopScrolling()
        requireNotNull(newDisplayedValues) { "newDisplayedValues should not be null." }
        require(maxValue - minValue + 1 <= newDisplayedValues.size) {
            ("mMaxValue - mMinValue + 1 should not be greater than mDisplayedValues.length, now "
                    + "((mMaxValue - mMinValue + 1) is " + (maxValue - minValue + 1)
                    + " newDisplayedValues.length is " + newDisplayedValues.size
                    + ", you need to set MaxValue and MinValue before setDisplayedValues(String[])")
        }
        updateContent(newDisplayedValues)
        updateMaxWHOfDisplayedValues(true)
        prevPickedIndex = 0 + minShowIndex
        correctPositionByDefaultValue(0, wrapSelectorWheel && wrapSelectorWheelCheck)
        postInvalidate()
        handlerInMainThread!!.sendEmptyMessage(HANDLER_WHAT_REQUEST_LAYOUT)
    }

    /**
     * Gets the values to be displayed instead of string values.
     *
     * @return The displayed values.
     */
    fun displayedValues(): Array<String?>? {
        return displayedValues
    }

    fun setWrapSelectorWheel(wrapSW: Boolean) {
        if (wrapSelectorWheel != wrapSW) {
            if (!wrapSW) {
                if (mScrollState == OnScrollListener.SCROLL_STATE_IDLE) {
                    internalSetWrapToLinear()
                } else {
                    pendingWrapToLinear = true
                }
            } else {
                wrapSelectorWheel = wrapSW
                updateWrapStateByContent()
                postInvalidate()
            }
        }
    }

    /**
     * get the "fromValue" by using getValue(), if your picker's minValue is not 0,
     * make sure you can get the accurate value by getValue(), or you can use
     * smoothScrollToValue(int fromValue, int toValue, boolean needRespond)
     *
     * @param toValue the value you want picker to scroll to
     */
    fun smoothScrollToValue(toValue: Int) {
        smoothScrollToValue(value(), toValue, true)
    }

    /**
     * get the "fromValue" by using getValue(), if your picker's minValue is not 0,
     * make sure you can get the accurate value by getValue(), or you can use
     * smoothScrollToValue(int fromValue, int toValue, boolean needRespond)
     *
     * @param toValue     the value you want picker to scroll to
     * @param needRespond set if you want picker to respond onValueChange listener
     */
    fun smoothScrollToValue(toValue: Int, needRespond: Boolean) {
        smoothScrollToValue(value(), toValue, needRespond)
    }

    /**
     * @param fValue   need to set the fromValue, can be greater than mMaxValue or less than mMinValue
     * @param tValue     the value you want picker to scroll to
     * @param needRespond need Respond to the ValueChange callback When Scrolling, default is false
     */
    @JvmOverloads
    fun smoothScrollToValue(
        fValue: Int,
        tValue: Int,
        needRespond: Boolean = true
    ) {
        var fromValue = fValue
        var toValue = tValue
        var deltaIndex: Int
        fromValue = refineValueByLimit(
            fromValue, minValue, maxValue,
            wrapSelectorWheel && wrapSelectorWheelCheck
        )
        toValue = refineValueByLimit(
            toValue, minValue, maxValue,
            wrapSelectorWheel && wrapSelectorWheelCheck
        )
        if (wrapSelectorWheel && wrapSelectorWheelCheck) {
            deltaIndex = toValue - fromValue
            val halfOneRecycleSize = getOneRecycleSize() / 2
            if (deltaIndex < -halfOneRecycleSize || halfOneRecycleSize < deltaIndex) {
                deltaIndex =
                    if (deltaIndex > 0) deltaIndex - getOneRecycleSize() else deltaIndex + getOneRecycleSize()
            }
        } else {
            deltaIndex = toValue - fromValue
        }
        setValue(fromValue)
        if (fromValue == toValue) {
            return
        }
        scrollByIndexSmoothly(deltaIndex, needRespond)
    }

    /**
     * simplify the "setDisplayedValue() + setMinValue() + setMaxValue()" process,
     * default minValue is 0, and make sure you do NOT change the minValue.
     *
     * @param display new values to be displayed
     */
    fun refreshByNewDisplayedValues(display: Array<String?>) {
        val minValue = getMinValue()
        val oldMaxValue = getMaxValue()
        val oldSpan = oldMaxValue - minValue + 1
        val newMaxValue = display.size - 1
        val newSpan = newMaxValue - minValue + 1
        if (newSpan > oldSpan) {
            displayedValues(display)
            maxValue(newMaxValue)
        } else {
            maxValue(newMaxValue)
            displayedValues(display)
        }
    }

    /**
     * used by handlers to respond onchange callbacks
     *
     * @param oldVal        prevPicked value
     * @param newVal        currPicked value
     * @param respondChange if want to respond onchange callbacks
     */
    private fun respondPickedValueChanged(
        oldVal: Int,
        newVal: Int,
        respondChange: Any?
    ) {
        onScrollStateChange(OnScrollListener.SCROLL_STATE_IDLE)
        if (oldVal != newVal) {
            if (respondChange == null || respondChange !is Boolean || respondChange) {
                if (mOnValueChangeListener != null) {
                    mOnValueChangeListener!!.onValueChange(
                        this@NumberPickerView,
                        oldVal + minValue,
                        newVal + minValue
                    )
                }
                if (mOnValueChangeListenerRaw != null) {
                    mOnValueChangeListenerRaw!!.onValueChangeRelativeToRaw(
                        this@NumberPickerView,
                        oldVal,
                        newVal,
                        displayedValues
                    )
                }
            }
        }
        prevPickedIndex = newVal
        if (pendingWrapToLinear) {
            pendingWrapToLinear = false
            internalSetWrapToLinear()
        }
    }

    /**
     * @param dIndex  the delta index it will scroll by
     * @param needRespond need Respond to the ValueChange callback When Scrolling, default is false
     */
    private fun scrollByIndexSmoothly(
        dIndex: Int,
        needRespond: Boolean = true
    ) {
        var deltaIndex = dIndex
        if (!(wrapSelectorWheel && wrapSelectorWheelCheck)) {
            val willPickRawIndex = getPickedIndexRelativeToRaw()
            if (willPickRawIndex + deltaIndex > maxShowIndex) {
                deltaIndex = maxShowIndex - willPickRawIndex
            } else if (willPickRawIndex + deltaIndex < minShowIndex) {
                deltaIndex = minShowIndex - willPickRawIndex
            }
        }
        var duration: Int
        var dy: Int
        if (mCurrDrawFirstItemY < -mItemHeight / 2) { //scroll upwards for a distance of less than mItemHeight
            dy = mItemHeight + mCurrDrawFirstItemY
            duration =
                (DEFAULT_INTERVAL_REVISE_DURATION.toFloat() * (mItemHeight + mCurrDrawFirstItemY) / mItemHeight).toInt()
            duration = if (deltaIndex < 0) {
                -duration - deltaIndex * DEFAULT_INTERVAL_REVISE_DURATION
            } else {
                duration + deltaIndex * DEFAULT_INTERVAL_REVISE_DURATION
            }
        } else { //scroll downwards for a distance of less than mItemHeight
            dy = mCurrDrawFirstItemY
            duration =
                (DEFAULT_INTERVAL_REVISE_DURATION.toFloat() * -mCurrDrawFirstItemY / mItemHeight).toInt()
            duration = if (deltaIndex < 0) {
                duration - deltaIndex * DEFAULT_INTERVAL_REVISE_DURATION
            } else {
                duration + deltaIndex * DEFAULT_INTERVAL_REVISE_DURATION
            }
        }
        dy += deltaIndex * mItemHeight
        if (duration < DEFAULT_MIN_SCROLL_BY_INDEX_DURATION) {
            duration = DEFAULT_MIN_SCROLL_BY_INDEX_DURATION
        }
        if (duration > DEFAULT_MAX_SCROLL_BY_INDEX_DURATION) {
            duration = DEFAULT_MAX_SCROLL_BY_INDEX_DURATION
        }
        scroller.startScroll(0, mCurrDrawGlobalY, 0, dy, duration)
        if (needRespond) {
            handlerInNewThread!!.sendMessageDelayed(
                getMsg(HANDLER_WHAT_REFRESH),
                duration / 4.toLong()
            )
        } else {
            handlerInNewThread!!.sendMessageDelayed(
                getMsg(
                    HANDLER_WHAT_REFRESH,
                    0,
                    0,
                    needRespond
                ), duration / 4.toLong()
            )
        }
        postInvalidate()
    }

    fun getMinValue(): Int {
        return minValue
    }

    fun getMaxValue(): Int {
        return maxValue
    }

    fun minValue(minV: Int) {
        minValue = minV
        minShowIndex = 0
        updateNotWrapYLimit()
    }

    //compatible for android.widget.NumberPicker
    fun maxValue(maxV: Int) {
        if (displayedValues == null) {
            throw NullPointerException("mDisplayedValues should not be null")
        }
        require(maxV - minValue + 1 <= displayedValues!!.size) {
            "(maxValue - mMinValue + 1) should not be greater than mDisplayedValues.length now " +
                    " (maxValue - mMinValue + 1) is " + (maxV - minValue + 1) + " and mDisplayedValues.length is " + displayedValues!!.size
        }
        maxValue = maxV
        maxShowIndex = maxValue - minValue + minShowIndex
        setMinAndMaxShowIndex(minShowIndex, maxShowIndex)
        updateNotWrapYLimit()
    }

    //compatible for android.widget.NumberPicker
    fun setValue(value: Int) {
        require(value >= minValue) { "should not set a value less than mMinValue, value is $value" }
        require(value <= maxValue) { "should not set a value greater than mMaxValue, value is $value" }
        setPickedIndexRelativeToRaw(value - minValue)
    }

    //compatible for android.widget.NumberPicker
    fun value(): Int {
        return getPickedIndexRelativeToRaw() + minValue
    }

    fun getContentByCurrValue(): String? {
        return displayedValues!![value() - minValue]
    }

    fun getWrapSelectorWheel(): Boolean {
        return wrapSelectorWheel
    }

    fun getWrapSelectorWheelAbsolutely(): Boolean {
        return wrapSelectorWheel && wrapSelectorWheelCheck
    }

    fun setHintText(hText: String?) {
        if (isStringEqual(hintText, hText)) {
            return
        }
        hintText = hText
        textSizeHintCenterYOffset = getTextCenterYOffset(paintHint!!.fontMetrics)
        widthOfHintText = getTextWidth(hintText, paintHint)
        handlerInMainThread!!.sendEmptyMessage(HANDLER_WHAT_REQUEST_LAYOUT)
    }

    fun setPickedIndexRelativeToMin(pickedIndexToMin: Int) {
        if (0 <= pickedIndexToMin && pickedIndexToMin < getOneRecycleSize()) {
            prevPickedIndex = pickedIndexToMin + minShowIndex
            correctPositionByDefaultValue(
                pickedIndexToMin,
                wrapSelectorWheel && wrapSelectorWheelCheck
            )
            postInvalidate()
        }
    }

    fun setNormalTextColor(normalTextColor: Int) {
        if (textColorNormal == normalTextColor) {
            return
        }
        textColorNormal = normalTextColor
        postInvalidate()
    }

    fun setSelectedTextColor(selectedTextColor: Int) {
        if (textColorSelected == selectedTextColor) {
            return
        }
        textColorSelected = selectedTextColor
        postInvalidate()
    }

    fun setHintTextColor(hintTextColor: Int) {
        if (textColorHint == hintTextColor) {
            return
        }
        textColorHint = hintTextColor
        paintHint!!.color = textColorHint
        postInvalidate()
    }

    fun setDividerColor(divColor: Int) {
        if (dividerColor == divColor) {
            return
        }
        dividerColor = divColor
        paintDivider.color = dividerColor
        postInvalidate()
    }

    fun setPickedIndexRelativeToRaw(pickedIndexToRaw: Int) {
        if (minShowIndex > -1) {
            if (minShowIndex <= pickedIndexToRaw && pickedIndexToRaw <= maxShowIndex) {
                prevPickedIndex = pickedIndexToRaw
                correctPositionByDefaultValue(
                    pickedIndexToRaw - minShowIndex,
                    wrapSelectorWheel && wrapSelectorWheelCheck
                )
                postInvalidate()
            }
        }
    }

    fun getPickedIndexRelativeToRaw(): Int {
        val willPickIndex: Int
        willPickIndex = if (mCurrDrawFirstItemY != 0) {
            if (mCurrDrawFirstItemY < -mItemHeight / 2) {
                getWillPickIndexByGlobalY(mCurrDrawGlobalY + mItemHeight + mCurrDrawFirstItemY)
            } else {
                getWillPickIndexByGlobalY(mCurrDrawGlobalY + mCurrDrawFirstItemY)
            }
        } else {
            getWillPickIndexByGlobalY(mCurrDrawGlobalY)
        }
        return willPickIndex
    }

    fun setMinAndMaxShowIndex(minShowI: Int, maxShowI: Int) {
        setMinAndMaxShowIndex(minShowI, maxShowI, true)
    }

    fun setMinAndMaxShowIndex(
        minShowI: Int,
        maxShowI: Int,
        needRefresh: Boolean
    ) {
        require(minShowI <= maxShowI) {
            ("minShowIndex should be less than maxShowIndex, minShowIndex is "
                    + minShowI + ", maxShowIndex is " + maxShowI + ".")
        }
        requireNotNull(displayedValues) { "mDisplayedValues should not be null, you need to set mDisplayedValues first." }
        require(minShowI >= 0) { "minShowIndex should not be less than 0, now minShowIndex is $minShowI" }
        require(minShowI <= displayedValues!!.size - 1) {
            "minShowIndex should not be greater than (mDisplayedValues.length - 1), now " +
                    "(mDisplayedValues.length - 1) is " + (displayedValues!!.size - 1) + " minShowIndex is " + minShowI
        }
        require(maxShowI >= 0) { "maxShowIndex should not be less than 0, now maxShowIndex is $maxShowI" }
        require(maxShowI <= displayedValues!!.size - 1) {
            "maxShowIndex should not be greater than (mDisplayedValues.length - 1), now " +
                    "(mDisplayedValues.length - 1) is " + (displayedValues!!.size - 1) + " maxShowIndex is " + maxShowI
        }
        minShowIndex = minShowI
        maxShowIndex = maxShowI
        if (needRefresh) {
            prevPickedIndex = 0 + minShowIndex
            correctPositionByDefaultValue(0, wrapSelectorWheel && wrapSelectorWheelCheck)
            postInvalidate()
        }
    }

    /**
     * set the friction of scroller, it will effect the scroller's acceleration when fling
     *
     * @param f default is ViewConfiguration.get(mContext).getScrollFriction()
     * if setFriction(2 * ViewConfiguration.get(mContext).getScrollFriction()),
     * the friction will be twice as much as before
     */
    fun setFriction(f: Float) {
        require(f > 0) { "you should set a a positive float friction, now friction is $f" }
        friction = ViewConfiguration.getScrollFriction() / f
    }

    //compatible for NumberPicker
    private fun onScrollStateChange(scrollState: Int) {
        if (mScrollState == scrollState) {
            return
        }
        mScrollState = scrollState
        if (mOnScrollListener != null) {
            mOnScrollListener!!.onScrollStateChange(this, scrollState)
        }
    }

    //compatible for NumberPicker
    fun setOnScrollListener(listener: OnScrollListener?) {
        mOnScrollListener = listener
    }

    //compatible for NumberPicker
    fun setOnValueChangedListener(listener: OnValueChangeListener?) {
        mOnValueChangeListener = listener
    }

    fun setOnValueChangedListenerRelativeToRaw(listener: OnValueChangeListenerRelativeToRaw?) {
        mOnValueChangeListenerRaw = listener
    }

    fun setOnValueChangeListenerInScrolling(listener: OnValueChangeListenerInScrolling?) {
        mOnValueChangeListenerInScrolling = listener
    }

    fun setContentTextTypeface(typeface: Typeface?) {
        paintText!!.typeface = typeface
    }

    fun setHintTextTypeface(typeface: Typeface?) {
        paintHint!!.typeface = typeface
    }

    //return index relative to mDisplayedValues from 0.
    private fun getWillPickIndexByGlobalY(globalY: Int): Int {
        if (mItemHeight == 0) {
            return 0
        }
        val willPickIndex = globalY / mItemHeight + shownCount / 2
        val index = getIndexByRawIndex(
            willPickIndex,
            getOneRecycleSize(),
            wrapSelectorWheel && wrapSelectorWheelCheck
        )
        return if (0 <= index && index < getOneRecycleSize()) {
            index + minShowIndex
        } else {
            throw IllegalArgumentException(
                "getWillPickIndexByGlobalY illegal index : " + index
                        + " getOneRecycleSize() : " + getOneRecycleSize() + " mWrapSelectorWheel : " + wrapSelectorWheel
            )
        }
    }

    private fun getIndexByRawIndex(i: Int, size: Int, wrap: Boolean): Int {
        var index = i
        if (size <= 0) {
            return 0
        }
        return if (wrap) {
            index %= size
            if (index < 0) {
                index += size
            }
            index
        } else {
            index
        }
    }

    private fun internalSetWrapToLinear() {
        val rawIndex = getPickedIndexRelativeToRaw()
        correctPositionByDefaultValue(rawIndex - minShowIndex, false)
        wrapSelectorWheel = false
        postInvalidate()
    }

    private fun updateDividerAttr() {
        dividerIndex0 = shownCount / 2
        dividerIndex1 = dividerIndex0 + 1
        dividerY0 = dividerIndex0 * mViewHeight / shownCount.toFloat()
        dividerY1 = dividerIndex1 * mViewHeight / shownCount.toFloat()
        if (dividerMarginL < 0) {
            dividerMarginL = 0
        }
        if (dividerMarginR < 0) {
            dividerMarginR = 0
        }
        if (dividerMarginL + dividerMarginR == 0) {
            return
        }
        if (paddingLeft + dividerMarginL >= mViewWidth - paddingRight - dividerMarginR) {
            val surplusMargin =
                paddingLeft + dividerMarginL + paddingRight + dividerMarginR - mViewWidth
            dividerMarginL =
                (dividerMarginL - surplusMargin.toFloat() * dividerMarginL / (dividerMarginL + dividerMarginR)).toInt()
            dividerMarginR =
                (dividerMarginR - surplusMargin.toFloat() * dividerMarginR / (dividerMarginL + dividerMarginR)).toInt()
        }
    }

    private var mNotWrapLimitYTop = 0
    private var mNotWrapLimitYBottom = 0
    private fun updateFontAttr() {
        if (textSizeNormal > mItemHeight) {
            textSizeNormal = mItemHeight
        }
        if (textSizeSelected > mItemHeight) {
            textSizeSelected = mItemHeight
        }
        requireNotNull(paintHint) { "mPaintHint should not be null." }
        paintHint.textSize = textSizeHint.toFloat()
        textSizeHintCenterYOffset = getTextCenterYOffset(paintHint.fontMetrics)
        widthOfHintText = getTextWidth(hintText, paintHint)
        requireNotNull(paintText) { "mPaintText should not be null." }
        paintText.textSize = textSizeSelected.toFloat()
        textSizeSelectedCenterYOffset = getTextCenterYOffset(paintText.fontMetrics)
        paintText.textSize = textSizeNormal.toFloat()
        textSizeNormalCenterYOffset = getTextCenterYOffset(paintText.fontMetrics)
    }

    private fun updateNotWrapYLimit() {
        mNotWrapLimitYTop = 0
        mNotWrapLimitYBottom = -shownCount * mItemHeight
        if (displayedValues != null) {
            mNotWrapLimitYTop = (getOneRecycleSize() - shownCount / 2 - 1) * mItemHeight
            mNotWrapLimitYBottom = -(shownCount / 2) * mItemHeight
        }
    }

    private var downYGlobal = 0f
    private var downY = 0f
    private var currY = 0f
    private fun limitY(preferred: Int): Int {
        var currDrawGlobalYPreferred = preferred
        if (wrapSelectorWheel && wrapSelectorWheelCheck) {
            return currDrawGlobalYPreferred
        }
        if (currDrawGlobalYPreferred < mNotWrapLimitYBottom) {
            currDrawGlobalYPreferred = mNotWrapLimitYBottom
        } else if (currDrawGlobalYPreferred > mNotWrapLimitYTop) {
            currDrawGlobalYPreferred = mNotWrapLimitYTop
        }
        return currDrawGlobalYPreferred
    }

    private var mFlagMayPress = false
    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (!isEnabled) {
            return false
        }
        if (mItemHeight == 0) {
            return true
        }
        if (velocityTracker == null) {
            velocityTracker = VelocityTracker.obtain()
        }
        velocityTracker!!.addMovement(event)
        currY = event.y
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                mFlagMayPress = true
                handlerInNewThread!!.removeMessages(HANDLER_WHAT_REFRESH)
                stopScrolling()
                downY = currY
                downYGlobal = mCurrDrawGlobalY.toFloat()
                onScrollStateChange(OnScrollListener.SCROLL_STATE_IDLE)
                parent.requestDisallowInterceptTouchEvent(true)
            }
            MotionEvent.ACTION_MOVE -> {
                val spanY = downY - currY
                if (mFlagMayPress && -scaledTouchSlop < spanY && spanY < scaledTouchSlop) {
                } else {
                    mFlagMayPress = false
                    mCurrDrawGlobalY = limitY((downYGlobal + spanY).toInt())
                    calculateFirstItemParameterByGlobalY()
                    invalidate()
                }
                onScrollStateChange(OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
            }
            MotionEvent.ACTION_UP -> if (mFlagMayPress) {
                click(event)
            } else {
                val velTracker = velocityTracker
                velTracker!!.computeCurrentVelocity(1000)
                val velocityY = (velTracker.yVelocity * friction).toInt()
                if (abs(velocityY) > miniVelocityFling) {
                    scroller.fling(
                        0,
                        mCurrDrawGlobalY,
                        0,
                        -velocityY,
                        Int.MIN_VALUE,
                        Int.MAX_VALUE,
                        limitY(Int.MIN_VALUE),
                        limitY(Int.MAX_VALUE)
                    )
                    invalidate()
                    onScrollStateChange(OnScrollListener.SCROLL_STATE_FLING)
                }
                handlerInNewThread!!.sendMessageDelayed(
                    getMsg(HANDLER_WHAT_REFRESH),
                    0
                )
                releaseVelocityTracker()
            }
            MotionEvent.ACTION_CANCEL -> {
                downYGlobal = mCurrDrawGlobalY.toFloat()
                stopScrolling()
                handlerInNewThread!!.sendMessageDelayed(
                    getMsg(HANDLER_WHAT_REFRESH),
                    0
                )
            }
        }
        return true
    }

    private fun click(event: MotionEvent) {
        val y = event.y
        for (i in 0 until shownCount) {
            if (mItemHeight * i <= y && y < mItemHeight * (i + 1)) {
                clickItem(i)
                break
            }
        }
    }

    private fun clickItem(showCountIndex: Int) {
        if (showCountIndex in 0 until shownCount) { //clicked the showCountIndex of the view
            scrollByIndexSmoothly(showCountIndex - shownCount / 2)
        }
    }

    private fun getTextCenterYOffset(fontMetrics: Paint.FontMetrics?): Float {
        return if (fontMetrics == null) {
            0f
        } else Math.abs(fontMetrics.top + fontMetrics.bottom) / 2
    }

    private var mViewWidth = 0
    private var mViewHeight = 0
    private var mItemHeight = 0
    private var dividerY0 = 0f
    private var dividerY1 = 0f
    private var mViewCenterX = 0f
    //defaultPickedIndex relative to the shown part
    private fun correctPositionByDefaultValue(
        defaultPickedIndex: Int,
        wrap: Boolean
    ) {
        mCurrDrawFirstItemIndex = defaultPickedIndex - (shownCount - 1) / 2
        mCurrDrawFirstItemIndex =
            getIndexByRawIndex(mCurrDrawFirstItemIndex, getOneRecycleSize(), wrap)
        if (mItemHeight == 0) {
            currentItemIndexEffect = true
        } else {
            mCurrDrawGlobalY = mCurrDrawFirstItemIndex * mItemHeight
            mInScrollingPickedOldValue = mCurrDrawFirstItemIndex + shownCount / 2
            mInScrollingPickedOldValue = mInScrollingPickedOldValue % getOneRecycleSize()
            if (mInScrollingPickedOldValue < 0) {
                mInScrollingPickedOldValue = mInScrollingPickedOldValue + getOneRecycleSize()
            }
            mInScrollingPickedNewValue = mInScrollingPickedOldValue
            calculateFirstItemParameterByGlobalY()
        }
    }

    //first shown item's content index, corresponding to the Index of mDisplayedValued
    private var mCurrDrawFirstItemIndex = 0
    //the first shown item's Y
    private var mCurrDrawFirstItemY = 0
    //global Y corresponding to scroller
    private var mCurrDrawGlobalY = 0

    override fun computeScroll() {
        if (mItemHeight == 0) {
            return
        }
        if (scroller.computeScrollOffset()) {
            mCurrDrawGlobalY = scroller.currY
            calculateFirstItemParameterByGlobalY()
            postInvalidate()
        }
    }

    private fun calculateFirstItemParameterByGlobalY() {
        mCurrDrawFirstItemIndex =
            Math.floor(mCurrDrawGlobalY.toFloat() / mItemHeight.toDouble()).toInt()
        mCurrDrawFirstItemY = -(mCurrDrawGlobalY - mCurrDrawFirstItemIndex * mItemHeight)
        if (mOnValueChangeListenerInScrolling != null) {
            mInScrollingPickedNewValue = if (-mCurrDrawFirstItemY > mItemHeight / 2) {
                mCurrDrawFirstItemIndex + 1 + shownCount / 2
            } else {
                mCurrDrawFirstItemIndex + shownCount / 2
            }
            mInScrollingPickedNewValue = mInScrollingPickedNewValue % getOneRecycleSize()
            if (mInScrollingPickedNewValue < 0) {
                mInScrollingPickedNewValue = mInScrollingPickedNewValue + getOneRecycleSize()
            }
            if (mInScrollingPickedOldValue != mInScrollingPickedNewValue) {
                respondPickedValueChangedInScrolling(
                    mInScrollingPickedOldValue + minValue,
                    mInScrollingPickedNewValue + minValue
                )
            }
            mInScrollingPickedOldValue = mInScrollingPickedNewValue
        }
    }

    private fun releaseVelocityTracker() {
        if (velocityTracker != null) {
            velocityTracker!!.clear()
            velocityTracker!!.recycle()
            velocityTracker = null
        }
    }

    private fun updateMaxWHOfDisplayedValues(needRequestLayout: Boolean) {
        updateMaxWidthOfDisplayedValues()
        updateMaxHeightOfDisplayedValues()
        if (needRequestLayout &&
            (mSpecModeW == MeasureSpec.AT_MOST || mSpecModeH == MeasureSpec.AT_MOST)
        ) {
            handlerInMainThread!!.sendEmptyMessage(HANDLER_WHAT_REQUEST_LAYOUT)
        }
    }

    private var mSpecModeW = MeasureSpec.UNSPECIFIED
    private var mSpecModeH = MeasureSpec.UNSPECIFIED
    private fun measureWidth(measureSpec: Int): Int {
        var result: Int
        mSpecModeW = MeasureSpec.getMode(measureSpec)
        val specMode = mSpecModeW
        val specSize = MeasureSpec.getSize(measureSpec)
        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize
        } else {
            val marginOfHint = if (Math.max(
                    widthOfHintText,
                    widthOfAlterHint
                ) == 0
            ) 0 else marginEndOfHint
            val gapOfHint = if (Math.max(
                    widthOfHintText,
                    widthOfAlterHint
                ) == 0
            ) 0 else marginStartOfHint
            val maxWidth = Math.max(
                maxWidthOfAlterArrayWithMeasureHint, Math.max(
                    maxWidthOfDisplayedValues,
                    maxWidthOfAlterArrayWithoutMeasureHint
                )
                        + 2 * (gapOfHint + Math.max(
                    widthOfHintText,
                    widthOfAlterHint
                ) + marginOfHint + 2 * itemPaddingHorizontal)
            )
            result =
                this.paddingLeft + this.paddingRight + maxWidth //MeasureSpec.UNSPECIFIED
            if (specMode == MeasureSpec.AT_MOST) {
                result = Math.min(result, specSize)
            }
        }
        return result
    }

    private fun measureHeight(measureSpec: Int): Int {
        var result: Int
        mSpecModeH = MeasureSpec.getMode(measureSpec)
        val specMode = mSpecModeH
        val specSize = MeasureSpec.getSize(measureSpec)
        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize
        } else {
            val maxHeight =
                shownCount * (maxHeightOfDisplayedValues + 2 * itemPaddingVertical)
            result =
                this.paddingTop + this.paddingBottom + maxHeight //MeasureSpec.UNSPECIFIED
            if (specMode == MeasureSpec.AT_MOST) {
                result = Math.min(result, specSize)
            }
        }
        return result
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        drawContent(canvas)
        drawLine(canvas)
        drawHint(canvas)
    }

    private fun drawContent(canvas: Canvas) {
        var index: Int
        var textColor: Int
        var textSize: Float
        var fraction =
            0f // fraction of the item in state between normal and selected, in[0, 1]
        var textSizeCenterYOffset: Float
        for (i in 0 until shownCount + 1) {
            val y = mCurrDrawFirstItemY + mItemHeight * i.toFloat()
            index = getIndexByRawIndex(
                mCurrDrawFirstItemIndex + i,
                getOneRecycleSize(),
                wrapSelectorWheel && wrapSelectorWheelCheck
            )
            if (i == shownCount / 2) { //this will be picked
                fraction = (mItemHeight + mCurrDrawFirstItemY).toFloat() / mItemHeight
                textColor = getEvaluateColor(fraction, textColorNormal, textColorSelected)
                textSize = getEvaluateSize(
                    fraction,
                    textSizeNormal.toFloat(),
                    textSizeSelected.toFloat()
                )
                textSizeCenterYOffset = getEvaluateSize(
                    fraction, textSizeNormalCenterYOffset,
                    textSizeSelectedCenterYOffset
                )
            } else if (i == shownCount / 2 + 1) {
                textColor = getEvaluateColor(1 - fraction, textColorNormal, textColorSelected)
                textSize = getEvaluateSize(
                    1 - fraction,
                    textSizeNormal.toFloat(),
                    textSizeSelected.toFloat()
                )
                textSizeCenterYOffset = getEvaluateSize(
                    1 - fraction, textSizeNormalCenterYOffset,
                    textSizeSelectedCenterYOffset
                )
            } else {
                textColor = textColorNormal
                textSize = textSizeNormal.toFloat()
                textSizeCenterYOffset = textSizeNormalCenterYOffset
            }
            paintText!!.color = textColor
            paintText.textSize = textSize
            if (0 <= index && index < getOneRecycleSize()) {
                var str: CharSequence? = displayedValues!![index + minShowIndex]
                if (textEllipsize != null) {
                    str = TextUtils.ellipsize(
                        str,
                        paintText,
                        width - 2 * itemPaddingHorizontal.toFloat(),
                        getEllipsizeType()
                    )
                }
                canvas.drawText(
                    str.toString(), mViewCenterX,
                    y + mItemHeight / 2 + textSizeCenterYOffset, paintText
                )
            } else if (!TextUtils.isEmpty(emptyItemHint)) {
                canvas.drawText(
                    emptyItemHint!!, mViewCenterX,
                    y + mItemHeight / 2 + textSizeCenterYOffset, paintText
                )
            }
        }
    }

    private fun getEllipsizeType(): TruncateAt {
        return when (textEllipsize) {
            TEXT_ELLIPSIZE_START -> TruncateAt.START
            TEXT_ELLIPSIZE_MIDDLE -> TruncateAt.MIDDLE
            TEXT_ELLIPSIZE_END -> TruncateAt.END
            else -> throw IllegalArgumentException("Illegal text ellipsize type.")
        }
    }

    private fun drawLine(canvas: Canvas) {
        if (showDivider) {
            canvas.drawLine(
                paddingLeft + dividerMarginL.toFloat(),
                dividerY0,
                mViewWidth - paddingRight - dividerMarginR.toFloat(),
                dividerY0,
                paintDivider
            )
            canvas.drawLine(
                paddingLeft + dividerMarginL.toFloat(),
                dividerY1,
                mViewWidth - paddingRight - dividerMarginR.toFloat(),
                dividerY1,
                paintDivider
            )
        }
    }

    private fun drawHint(canvas: Canvas) {
        if (TextUtils.isEmpty(hintText)) {
            return
        }
        canvas.drawText(
            hintText!!,
            mViewCenterX + (maxWidthOfDisplayedValues + widthOfHintText) / 2 + marginStartOfHint,
            (dividerY0 + dividerY1) / 2 + textSizeHintCenterYOffset, paintHint!!
        )
    }

    private fun updateMaxWidthOfDisplayedValues() {
        val savedTextSize = paintText!!.textSize
        paintText.textSize = textSizeSelected.toFloat()
        maxWidthOfDisplayedValues = getMaxWidthOfTextArray(
            displayedValues?.map { s -> s.toString() }?.toTypedArray(),
            paintText
        )
        maxWidthOfAlterArrayWithMeasureHint =
            getMaxWidthOfTextArray(alterTextArrayWithMeasureHint, paintText)
        maxWidthOfAlterArrayWithoutMeasureHint =
            getMaxWidthOfTextArray(alterTextArrayWithoutMeasureHint, paintText)
        paintText.textSize = textSizeHint.toFloat()
        widthOfAlterHint = getTextWidth(alterHint, paintText)
        paintText.textSize = savedTextSize
    }

    private fun getMaxWidthOfTextArray(
        array: Array<CharSequence?>?,
        paint: Paint?
    ): Int {
        if (array == null) {
            return 0
        }
        var maxWidth = 0
        for (item in array) {
            if (item != null) {
                val itemWidth = getTextWidth(item, paint)
                maxWidth = Math.max(itemWidth, maxWidth)
            }
        }
        return maxWidth
    }

    private fun getTextWidth(text: CharSequence?, paint: Paint?): Int {
        if (TextUtils.isEmpty(text)) {
            return 0
        }
        val key = text.toString()
        if (textWidthCache.containsKey(key)) {
            val integer = textWidthCache[key]
            if (integer != null) {
                return integer
            }
        }
        val value = (paint!!.measureText(key) + 0.5f).toInt()
        textWidthCache[key] = value
        return value
    }

    private fun updateMaxHeightOfDisplayedValues() {
        val savedTextSize = paintText!!.textSize
        paintText.textSize = textSizeSelected.toFloat()
        maxHeightOfDisplayedValues =
            (paintText.fontMetrics.bottom - paintText.fontMetrics.top + 0.5).toInt()
        paintText.textSize = savedTextSize
    }

    private fun updateContentAndIndex(newDisplayedValues: Array<String?>) {
        minShowIndex = 0
        maxShowIndex = newDisplayedValues.size - 1
        displayedValues = newDisplayedValues
        updateWrapStateByContent()
    }

    private fun updateContent(newDisplayedValues: Array<String?>) {
        displayedValues = newDisplayedValues
        updateWrapStateByContent()
    }

    //used in setDisplayedValues
    private fun updateValue() {
        inflateDisplayedValuesIfNull()
        updateWrapStateByContent()
        minShowIndex = 0
        maxShowIndex = displayedValues!!.size - 1
    }

    private fun updateValueForInit() {
        inflateDisplayedValuesIfNull()
        updateWrapStateByContent()
        if (minShowIndex == -1) {
            minShowIndex = 0
        }
        if (maxShowIndex == -1) {
            maxShowIndex = displayedValues!!.size - 1
        }
        setMinAndMaxShowIndex(minShowIndex, maxShowIndex, false)
    }

    private fun inflateDisplayedValuesIfNull() {
        if (displayedValues == null) {
            displayedValues = arrayOfNulls(1)
            displayedValues!![0] = "0"
        }
    }

    private fun updateWrapStateByContent() {
        wrapSelectorWheelCheck = displayedValues!!.size > shownCount
    }

    private fun refineValueByLimit(
        v: Int,
        minV: Int,
        maxV: Int,
        wrap: Boolean
    ): Int {
        var value = v
        return if (wrap) {
            if (value > maxV) {
                value = (value - maxV) % getOneRecycleSize() + minV - 1
            } else if (value < minV) {
                value = (value - minV) % getOneRecycleSize() + maxV + 1
            }
            value
        } else {
            if (value > maxV) {
                value = maxV
            } else if (value < minV) {
                value = minV
            }
            value
        }
    }

    private fun stopRefreshing() {
        if (handlerInNewThread != null) {
            handlerInNewThread!!.removeMessages(HANDLER_WHAT_REFRESH)
        }
    }

    fun stopScrolling() {
            if (!scroller.isFinished) {
                scroller.startScroll(0, scroller.currY, 0, 0, 1)
                scroller.abortAnimation()
                postInvalidate()
            }
    }

    fun stopScrollingAndCorrectPosition() {
        stopScrolling()
        if (handlerInNewThread != null) {
            handlerInNewThread!!.sendMessageDelayed(
                getMsg(HANDLER_WHAT_REFRESH),
                0
            )
        }
    }

    @Suppress("SameParameterValue")
    private fun getMsg(what: Int): Message {
        return getMsg(what, 0, 0, null)
    }

    private fun getMsg(what: Int, arg1: Int, arg2: Int, obj: Any?): Message {
        val msg = Message.obtain()
        msg.what = what
        msg.arg1 = arg1
        msg.arg2 = arg2
        msg.obj = obj
        return msg
    }

    //===tool functions===//
    private fun isStringEqual(a: String?, b: String?): Boolean {
        return if (a == null) {
            b == null
        } else {
            a == b
        }
    }

    private fun sp2px(context: Context, spValue: Float): Int {
        val fontScale = context.resources.displayMetrics.scaledDensity
        return (spValue * fontScale + 0.5f).toInt()
    }

    private fun dp2px(context: Context, dpValue: Float): Int {
        val densityScale = context.resources.displayMetrics.density
        return (dpValue * densityScale + 0.5f).toInt()
    }

    private fun getEvaluateColor(fraction: Float, startColor: Int, endColor: Int): Int {
        val a: Int
        val r: Int
        val g: Int
        val b: Int
        val sA = startColor and -0x1000000 ushr 24
        val sR = startColor and 0x00ff0000 ushr 16
        val sG = startColor and 0x0000ff00 ushr 8
        val sB = startColor and 0x000000ff ushr 0
        val eA = endColor and -0x1000000 ushr 24
        val eR = endColor and 0x00ff0000 ushr 16
        val eG = endColor and 0x0000ff00 ushr 8
        val eB = endColor and 0x000000ff ushr 0
        a = (sA + (eA - sA) * fraction).toInt()
        r = (sR + (eR - sR) * fraction).toInt()
        g = (sG + (eG - sG) * fraction).toInt()
        b = (sB + (eB - sB) * fraction).toInt()
        return a shl 24 or (r shl 16) or (g shl 8) or b
    }

    private fun getEvaluateSize(
        fraction: Float,
        startSize: Float,
        endSize: Float
    ): Float {
        return startSize + (endSize - startSize) * fraction
    }

    private fun convertCharSequenceArrayToStringArray(charSequences: Array<CharSequence>?): Array<String?>? {
        if (charSequences == null) {
            return null
        }
        val ret = arrayOfNulls<String>(charSequences.size)
        for (i in charSequences.indices) {
            ret[i] = charSequences[i].toString()
        }
        return ret
    }


    companion object {
        // default text color of not selected item
        private const val DEFAULT_TEXT_COLOR_NORMAL = -0xcccccd
        // default text color of selected item
        private const val DEFAULT_TEXT_COLOR_SELECTED = -0xa9ced
        // default text size of normal item
        private const val DEFAULT_TEXT_SIZE_NORMAL_SP = 14
        // default text size of selected item
        private const val DEFAULT_TEXT_SIZE_SELECTED_SP = 16
        // default text size of hint text, the middle item's right text
        private const val DEFAULT_TEXT_SIZE_HINT_SP = 14
        // distance between selected text and hint text
        private const val DEFAULT_MARGIN_START_OF_HINT_DP = 8
        // distance between hint text and right of this view, used in wrap_content mode
        private const val DEFAULT_MARGIN_END_OF_HINT_DP = 8
        // default divider's color
        private const val DEFAULT_DIVIDER_COLOR = -0xa9ced
        // default divider's height
        private const val DEFAULT_DIVIDER_HEIGHT = 2
        // default divider's margin to the left & right of this view
        private const val DEFAULT_DIVIDER_MARGIN_HORIZONTAL = 0
        // default shown items' count, now we display 3 items, the 2nd one is selected
        private const val DEFAULT_SHOWN_COUNT = 3
        // default items' horizontal padding, left padding and right padding are both 5dp,
// only used in wrap_content mode
        private const val DEFAULT_ITEM_PADDING_DP_H = 5
        // default items' vertical padding, top padding and bottom padding are both 2dp,
// only used in wrap_content mode
        private const val DEFAULT_ITEM_PADDING_DP_V = 2
        // message's what argument to refresh current state, used by mHandler
        private const val HANDLER_WHAT_REFRESH = 1
        // message's what argument to respond value changed event, used by mHandler
        private const val HANDLER_WHAT_LISTENER_VALUE_CHANGED = 2
        // message's what argument to request layout, used by mHandlerInMainThread
        private const val HANDLER_WHAT_REQUEST_LAYOUT = 3
        // interval time to scroll the distance of one item's height
        private const val HANDLER_INTERVAL_REFRESH = 32 //millisecond
        // in millisecond unit, default duration of scrolling an item' distance
        private const val DEFAULT_INTERVAL_REVISE_DURATION = 300
        // max and min durations when scrolling from one value to another
        private const val DEFAULT_MIN_SCROLL_BY_INDEX_DURATION =
            DEFAULT_INTERVAL_REVISE_DURATION * 1
        private const val DEFAULT_MAX_SCROLL_BY_INDEX_DURATION =
            DEFAULT_INTERVAL_REVISE_DURATION * 2
        private const val TEXT_ELLIPSIZE_START = "start"
        private const val TEXT_ELLIPSIZE_MIDDLE = "middle"
        private const val TEXT_ELLIPSIZE_END = "end"
        private const val DEFAULT_SHOW_DIVIDER = true
        private const val DEFAULT_WRAP_SELECTOR_WHEEL = true
        private const val DEFAULT_CURRENT_ITEM_INDEX_EFFECT = false
        private const val DEFAULT_RESPOND_CHANGE_ON_DETACH = false
        private const val DEFAULT_RESPOND_CHANGE_IN_MAIN_THREAD = true
    }
}