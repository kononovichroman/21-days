package com.habit.application.utils.topsnackbar

import android.os.Handler
import android.os.Looper
import android.os.Message
import java.lang.ref.WeakReference


/**
 * Manages [TopSnackBar]s.
 */
internal class SnackbarManager private constructor() {
    private val lock: Any
    private val handler: Handler
    private var currentSnackBar: SnackbarRecord? = null
    private var nextSnackBar: SnackbarRecord? = null

    internal interface Callback {
        fun show()
        fun dismiss(event: Int)
    }

    fun show(duration: Int, callback: Callback) {
        synchronized(lock) {
            when {
                isCurrentSnackBar(callback) -> { // Means that the callback is already in the queue. We'll just update the duration
                    currentSnackBar?.duration = duration
                    // If this is the TSnackbar currently being shown, call re-schedule it's
                    // timeout
                    handler.removeCallbacksAndMessages(currentSnackBar)
                    scheduleTimeoutLocked(currentSnackBar)
                    return
                }
                isNextSnackBar(callback) -> { // We'll just update the duration
                    nextSnackBar?.duration = duration
                }
                else -> { // Else, we need to create a new record and queue it
                    nextSnackBar = SnackbarRecord(duration, callback)
                }
            }
            if (currentSnackBar != null && cancelSnackBarLocked(
                    currentSnackBar,
                    TopSnackBar.Callback.DISMISS_EVENT_CONSECUTIVE
                )
            ) { // If we currently have a TSnackbar, try and cancel it and wait in line
                return
            } else { // Clear out the current snackbar
                currentSnackBar = null
                // Otherwise, just show it now
                showNextSnackBarLocked()
            }
        }
    }

    fun dismiss(callback: Callback, event: Int) {
        synchronized(lock) {
            when {
                isCurrentSnackBar(callback) -> {
                    cancelSnackBarLocked(currentSnackBar, event)
                }
                isNextSnackBar(callback) -> {
                    cancelSnackBarLocked(nextSnackBar, event)
                }
                else -> {
                }
            }
        }
    }

    /**
     * Should be called when a TSnackbar is no longer displayed. This is after any exit
     * animation has finished.
     */
    fun onDismissed(callback: Callback) {
        synchronized(lock) {
            if (isCurrentSnackBar(callback)) { // If the callback is from a TSnackbar currently show, remove it and show a new one
                currentSnackBar = null
                if (nextSnackBar != null) {
                    showNextSnackBarLocked()
                }
            }
        }
    }

    /**
     * Should be called when a TSnackbar is being shown. This is after any entrance animation has
     * finished.
     */
    fun onShown(callback: Callback) {
        synchronized(lock) {
            if (isCurrentSnackBar(callback)) {
                scheduleTimeoutLocked(currentSnackBar)
            }
        }
    }

    fun cancelTimeout(callback: Callback) {
        synchronized(lock) {
            if (isCurrentSnackBar(callback)) {
                handler.removeCallbacksAndMessages(currentSnackBar)
            }
        }
    }

    fun restoreTimeout(callback: Callback) {
        synchronized(lock) {
            if (isCurrentSnackBar(callback)) {
                scheduleTimeoutLocked(currentSnackBar)
            }
        }
    }

    fun isCurrent(callback: Callback): Boolean {
        synchronized(lock) { return isCurrentSnackBar(callback) }
    }

    fun isCurrentOrNext(callback: Callback): Boolean {
        synchronized(lock) { return isCurrentSnackBar(callback) || isNextSnackBar(callback) }
    }

    private class SnackbarRecord internal constructor(
        internal var duration: Int,
        callback: Callback
    ) {
        internal val callback: WeakReference<Callback> = WeakReference(callback)
        fun isSnackBar(callback: Callback?): Boolean {
            return callback != null && this.callback.get() === callback
        }

    }

    private fun showNextSnackBarLocked() {
        if (nextSnackBar != null) {
            currentSnackBar = nextSnackBar
            nextSnackBar = null
            val callback = currentSnackBar!!.callback.get()
            if (callback != null) {
                callback.show()
            } else { // The callback doesn't exist any more, clear out the TSnackbar
                currentSnackBar = null
            }
        }
    }

    private fun cancelSnackBarLocked(
        record: SnackbarRecord?,
        event: Int
    ): Boolean {
        val callback = record!!.callback.get()
        if (callback != null) {
            callback.dismiss(event)
            return true
        }
        return false
    }

    private fun isCurrentSnackBar(callback: Callback): Boolean {
        return currentSnackBar != null && currentSnackBar!!.isSnackBar(callback)
    }

    private fun isNextSnackBar(callback: Callback): Boolean {
        return nextSnackBar != null && nextSnackBar!!.isSnackBar(callback)
    }

    private fun scheduleTimeoutLocked(r: SnackbarRecord?) {
        if (r!!.duration == TopSnackBar.LENGTH_INDEFINITE) { // If we're set to indefinite, we don't want to set a timeout
            return
        }
        var durationMs = LONG_DURATION_MS
        if (r.duration > 0) {
            durationMs = r.duration
        } else if (r.duration == TopSnackBar.LENGTH_SHORT) {
            durationMs = SHORT_DURATION_MS
        }
        handler.removeCallbacksAndMessages(r)
        handler.sendMessageDelayed(
            Message.obtain(
                handler,
                MSG_TIMEOUT,
                r
            ), durationMs.toLong()
        )
    }

    private fun handleTimeout(record: SnackbarRecord) {
        synchronized(lock) {
            if (currentSnackBar === record || nextSnackBar === record) {
                cancelSnackBarLocked(record, TopSnackBar.Callback.DISMISS_EVENT_TIMEOUT)
            }
        }
    }

    companion object {
        private const val MSG_TIMEOUT = 0
        private const val SHORT_DURATION_MS = 1500
        private const val LONG_DURATION_MS = 2750
        private var sSnackBarManager: SnackbarManager? = null
        val instance: SnackbarManager?
            get() {
                if (sSnackBarManager == null) {
                    sSnackBarManager = SnackbarManager()
                }
                return sSnackBarManager
            }
    }

    init {
        lock = Any()
        handler = Handler(Looper.getMainLooper(), Handler.Callback { message ->
            when (message.what) {
                MSG_TIMEOUT -> {
                    handleTimeout(message.obj as SnackbarRecord)
                    return@Callback true
                }
            }
            false
        })
    }
}