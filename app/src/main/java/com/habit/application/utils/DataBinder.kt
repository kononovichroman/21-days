package com.habit.application.utils

import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.habit.application.R
import com.habit.application.core.database.Status
import com.habit.application.core.database.TargetModel
import com.habit.application.core.reminder.TimeModel
import com.habit.application.core.reminder.TimeType
import com.habit.application.screen.main.MainViewModel
import com.habit.application.screen.main.adapter.ListTargetRvAdapter


/**
 * Created by Roman K. on 28.01.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */

object DataBinder {
//    @JvmStatic
//    @BindingAdapter("tools:rvListTargets")
//    fun setRvListTargets(recyclerView: RecyclerView, list: List<TargetModel>?) {
//        recyclerView.apply {
//            list?.let {
//                (recyclerView.adapter as ListTargetRvAdapter).setList(it)
//            }
//        }
//    }

    @JvmStatic
    @BindingAdapter("tools:setError")
    fun setError(textInput: TextInputLayout, string: String?) {
        textInput.error = string
    }

    @JvmStatic
    @BindingAdapter("tools:enabledEditText")
    fun setEditTextEnabled(textInput: TextInputEditText, state: MainViewModel.MainScreenState) {
        textInput.isEnabled = state == MainViewModel.MainScreenState.CREATE_TARGET
    }

    @JvmStatic
    @BindingAdapter("tools:setStatus")
    fun setStatus(textView: TextView, status: Status?) {
        status?.let {
            textView.apply {
                text = when (status) {
                    Status.ACTIVE -> {
                        this.resources.getString(R.string.main_screen_item_target_status_active)
                    }
                    Status.ERROR -> {
                        this.resources.getString(R.string.main_screen_item_target_status_error)

                    }
                    Status.COMPLETED -> {
                        this.resources.getString(R.string.main_screen_item_target_status_complete)
                    }
                }
            }
        }

    }
    @JvmStatic
    @BindingAdapter("tools:setDetailNotificationText")
    fun setDetailNotificationText(textView: TextView, timeModel: TimeModel?) {
        timeModel?.let {
            textView.apply {
                text = it.toString()
            }
        } ?: kotlin.run {

        }
        textView.apply {
            text = timeModel?.let {
                it.toString()
            } ?: kotlin.run {
                this.resources.getString(R.string.screen_detail_notification)
            }
        }

    }

    @JvmStatic
    @BindingAdapter("tools:setNotificationText")
    fun setNotificationText(textView: TextView, timeModel: TimeModel?) {
        textView.apply {
            timeModel?.let {
                text = when (timeModel.type) {
                    TimeType.TIME_TYPE_PM -> {
                        this.resources.getString(
                            R.string.screen_create_target_notification_time,
                            String.format("%01d", it.hour),
                            String.format("%02d", it.minute),
                            "PM"
                        )
                    }
                    TimeType.TIME_TYPE_AM -> {
                        this.resources.getString(
                            R.string.screen_create_target_notification_time,
                            String.format("%01d", it.hour),
                            String.format("%02d", it.minute),
                            "AM"
                        )

                    }
                    TimeType.TIME_TYPE_24 -> {
                        this.resources.getString(
                            R.string.screen_create_target_notification_time,
                            String.format("%02d", it.hour),
                            String.format("%02d", it.minute),
                            ""
                        )
                    }
                }
            } ?: kotlin.run {
                text = this.resources.getString(R.string.screen_create_target_notification)
            }
        }
    }
}