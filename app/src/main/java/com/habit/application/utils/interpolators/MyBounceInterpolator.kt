package com.habit.application.utils.interpolators

import android.view.animation.Interpolator
import kotlin.math.cos
import kotlin.math.pow


/**
 * Created by Roman K. on 21.03.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
class MyBounceInterpolator: Interpolator {
    private val amplitude = 0.16
    private val frequency = 10.0
    override fun getInterpolation(time: Float): Float {
        return (-1 * Math.E.pow(-time / amplitude) * cos(
            frequency * time
        ) + 1).toFloat()
    }
}