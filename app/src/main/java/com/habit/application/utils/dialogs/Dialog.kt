package com.habit.application.utils.dialogs

import android.os.Bundle
import android.view.Gravity
import android.view.View
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.dialog_layout.*
import com.habit.application.R


/**
 * Created by Roman K. on 05.02.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */

const val DIALOG_TAG = "dialog_tag"
private const val DIALOG_TITLE = "dialog_title"
private const val DIALOG_MSG = "dialog_msg"
private const val DIALOG_IS_CANCELABLE = "dialog_is_cancelable"
private const val DIALOG_BTN_POSITIVE = "dialog_btn_positive"
private const val DIALOG_BTN_NEGATIVE = "dialog_btn_negative"
private const val DIALOG_TEXT_IS_CENTER = "dialog_text_is_center"
private const val DIALOG_WITH_ANIM_SHOW = "dialog_with_anim_show"

class Dialog : DialogFragment() {
    private companion object {
        fun instance() =
            Dialog()

        fun instance(listener: Listener) =
            Dialog().apply {
                dialogListener = listener
            }
    }

    var dialogListener: Listener? = null

    data class Builder(
        private var title: Int? = null,
        private var titleStr: String? = null,
        private var message: Int? = null,
        private var messageStr: String? = null,
        private var isCanceled: Boolean = true,
        private var btnPos: Int? = null,
        private var btnNeg: Int? = null,
        private var textIsCenter: Boolean = false,
        private var withAnimShow: Boolean = false,
        private var listener: Listener? = null
    ) {
        fun title(@StringRes title: Int) = apply { this.title = title }
        fun message(@StringRes message: Int) = apply { this.message = message }
        fun title(title: String) = apply { this.titleStr = title }
        fun message(message: String) = apply { this.messageStr = message }
        fun btnPos(@StringRes btnPos: Int) = apply { this.btnPos = btnPos }
        fun btnNeg(@StringRes btnNeg: Int) = apply { this.btnNeg = btnNeg }
        fun setCancelable(isCancelable: Boolean) = apply { this.isCanceled = isCancelable }
        fun setTextIsCenter(textIsCenter: Boolean) = apply { this.textIsCenter = textIsCenter }
        fun setListener(listener: Listener) = apply { this.listener = listener }
        fun withAnimShow(withAnimShow: Boolean) = apply { this.withAnimShow = withAnimShow }
        fun build() = listener?.let {
            instance(it).apply {
                arguments = Bundle(9).apply {
                    title?.let { putInt(DIALOG_TITLE, it) }
                    titleStr?.let { putString(DIALOG_TITLE, it) }
                    message?.let { putInt(DIALOG_MSG, it) }
                    messageStr?.let { putString(DIALOG_MSG, it) }
                    btnPos?.let { putInt(DIALOG_BTN_POSITIVE, it) }
                    btnNeg?.let { putInt(DIALOG_BTN_NEGATIVE, it) }
                    putBoolean(DIALOG_IS_CANCELABLE, isCanceled)
                    putBoolean(DIALOG_TEXT_IS_CENTER, textIsCenter)
                    putBoolean(DIALOG_WITH_ANIM_SHOW, withAnimShow)
                }
            }
        } ?: kotlin.run {
            instance().apply {
                arguments = Bundle(6).apply {
                    title?.let { putInt(DIALOG_TITLE, it) }
                    message?.let { putInt(DIALOG_MSG, it) }
                    btnPos?.let { putInt(DIALOG_BTN_POSITIVE, it) }
                    btnNeg?.let { putInt(DIALOG_BTN_NEGATIVE, it) }
                    putBoolean(DIALOG_IS_CANCELABLE, isCanceled)
                    putBoolean(DIALOG_TEXT_IS_CENTER, textIsCenter)
                }
            }
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): android.app.Dialog {
        return activity?.let {
            AlertDialog.Builder(it)
                .setView(R.layout.dialog_layout)
                .create()
        } ?: run {
            super.onCreateDialog(savedInstanceState)
        }
    }

    override fun onStart() {
        super.onStart()
        if (dialog != null) {
            dialog?.setCanceledOnTouchOutside(
                arguments?.getBoolean(
                    DIALOG_IS_CANCELABLE
                ) ?: true
            )
            setContent()
            dialog?.dialogNegativeBtn?.setOnClickListener {
                dismiss()
            }
            dialog?.dialogPositiveBtn?.setOnClickListener {
                dialogListener?.onClickDialogPositiveBtn()
                dismiss()
            }

            val window = dialog?.window
            val wlp = window?.attributes
            arguments?.getBoolean(DIALOG_WITH_ANIM_SHOW)?.let {
                if (it){
                    wlp?.windowAnimations = R.style.DialogAnimation
                }
            }
            window?.setBackgroundDrawableResource(android.R.color.transparent)
            wlp?.gravity = Gravity.CENTER
            window?.attributes = wlp
//            dialog?.getWindow()?.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        }
    }

    private fun setContent() {
        arguments?.getBoolean(
            DIALOG_TEXT_IS_CENTER
        ).apply {
            if (this == true) {
                dialog?.dialogTitleTV?.gravity = Gravity.CENTER_HORIZONTAL
                dialog?.dialogMessage?.gravity = Gravity.CENTER_HORIZONTAL
            } else {
                dialog?.dialogTitleTV?.gravity = Gravity.START
                dialog?.dialogMessage?.gravity = Gravity.START
            }
        }

        arguments?.getInt(DIALOG_TITLE)?.let {
            if (it == 0) {
                arguments?.getString(DIALOG_TITLE)?.let {str ->
                    dialog?.dialogTitleTV?.text = str
                } ?: kotlin.run {
                    dialog?.dialogTitleTV?.visibility = View.GONE
                }
            } else {
                dialog?.dialogTitleTV?.text = getString(it)
            }
        }
        arguments?.getInt(DIALOG_MSG)
            ?.let {
                if (it == 0) {
                    arguments?.getString(DIALOG_MSG)?.let {str ->
                        dialog?.dialogMessage?.text = str
                    } ?: kotlin.run {
                        dialog?.dialogMessage?.visibility = View.GONE
                    }
                } else {
                    dialog?.dialogMessage?.text = getString(it)
                }
            }
        arguments?.getInt(DIALOG_BTN_POSITIVE)
            ?.let { dialog?.dialogPositiveBtn?.text = getString(it) }
        arguments?.getInt(DIALOG_BTN_NEGATIVE)
            ?.let { dialog?.dialogNegativeBtn?.text = getString(it) }
    }

    interface Listener {
        fun onClickDialogPositiveBtn()
    }
}