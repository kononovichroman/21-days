package com.habit.application.utils.rv.itemAnimator

import androidx.core.view.ViewPropertyAnimatorListener;
import androidx.recyclerview.widget.RecyclerView;

/**
 * Created by Roman K. on 12.02.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
interface AnimateViewHolder {
    fun preAnimateAddImpl(holder: RecyclerView.ViewHolder?)
    fun preAnimateRemoveImpl(holder: RecyclerView.ViewHolder?)
    fun animateAddImpl(
        holder: RecyclerView.ViewHolder?,
        listener: ViewPropertyAnimatorListener?
    )

    fun animateRemoveImpl(
        holder: RecyclerView.ViewHolder?,
        listener: ViewPropertyAnimatorListener?
    )
}