package com.habit.application.utils.topsnackbar

import android.view.animation.Interpolator
import com.habit.application.utils.interpolators.MyBounceInterpolator


/**
 * Created by kurt on 2015/06/08.
 */
object AnimationUtils {
    val INTERPOLATOR: Interpolator =
        MyBounceInterpolator()
}