package com.habit.application.core.managers.analytics

import android.app.Activity


/**
 * Created by Roman K. on 07.03.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
interface AnalyticsManagerAPI {
    fun setUserProperty(property: AnalyticsUserProperty, value: Any)
    fun logEvent(event: AnalyticsEvent, extraLineEvent: String? = null, bundle: List<String>? = null)
    fun currentScreen(activity: Activity, screenName: String)


    enum class AnalyticsUserProperty(
        val value: String
    ) {
        DAY_FIRST_LAUNCH("cohort_day"),
        WEEK_FIRST_LAUNCH("cohort_week"),
        MONTH_FIRST_LAUNCH("cohort_month"),
        CURRENT_VERSION_APP("version"),
        START_VERSION_APP("start_version"),
        COUNT_LAUNCH_APP("count_launch_app"),
        COUNT_ALL_TARGETS("count_targets_all"),
        COUNT_ACTIVE_TARGETS("count_targets_active"),
        COUNT_EXPIRED_TARGETS("count_targets_expired"),
        COUNT_SUCCESS_TARGETS("count_targets_success")
    }

    enum class AnalyticsEvent(
        val value: String
    ) {
        ONBOARDING_FIRST_SCREEN_DONE("onboarding_first_screen_done"),
        ONBOARDING_SECOND_SCREEN_DONE("onboarding_second_screen_done"),
        ONBOARDING_THIRD_SCREEN_DONE("onboarding_third_screen_done"),
        CLICK_SHOW_NOTIFICATION("click_show_notification"),
        CLICK_PRESET_SUNRISE("click_preset_sunrise"),
        CLICK_PRESET_SOLAR_NOON("click_preset_solar_noon"),
        CLICK_PRESET_SUNSET("click_preset_sunset"),
        CLICK_CHANGE_COUNT_DAYS("click_change_count_days"),
        CLICK_UPDATE_IN_APP("click_update_in_app"),
        CLICK_CONTACT_US("click_contact_us"),
        CLICK_RATE_APP("click_rate_app"),
        CLICK_PRIVACY_POLICY("click_privacy_policy"),
        CLICK_SHARE_APP("click_share_app"),
        RATE_APP_LATER("rate_app_later"),
        RATE_APP_NOW("rate_app_now"),
        RATE_APP_NOT_LIKE("rate_app_not_like"),
        SHOW_DELETE_TARGET("show_delete_target"),
        CLOSE_BOTTOM_LAYOUT_ICON("close_bottom_layout_icon"),
        CLOSE_BOTTOM_LAYOUT_BG("close_bottom_layout_bg")
    }
}