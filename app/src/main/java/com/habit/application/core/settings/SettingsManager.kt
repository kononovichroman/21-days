package com.habit.application.core.settings

import android.content.SharedPreferences
import android.util.Log
import com.habit.application.BuildConfig
import java.util.*


/**
 * Created by Roman K. on 09.02.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
private const val SP_KEY_FIRST_LAUNCH_DAY = "sp_key_first_launch_day"
private const val SP_KEY_FIRST_LAUNCH_WEEK = "sp_key_first_launch_week"
private const val SP_KEY_FIRST_LAUNCH_MONTH = "sp_key_first_launch_month"
private const val SP_KEY_LAUNCH_COUNT = "sp_key_launch_count"
private const val SP_KEY_START_VERSION = "sp_key_start_version"
private const val SP_KEY_COUNT_SHOW_RATE_APP = "sp_key_count_show_rate_app"
private const val SP_KEY_NEED_SHOW_RATE_APP = "sp_key_need_show_rate_app"
private const val SP_KEY_NEED_SHOW_RATE_APP_LAST_DATE = "sp_key_need_show_rate_app_last_date"
private const val SP_KEY_ONBOARDING_SHOW = "sp_key_onboarding_show"
private const val SP_KEY_HINT_RV_SHOW = "sp_key_hint_rv_show"
private const val COUNT_NEED_RATE_APP = 4
private const val COUNT_NEED_TIME_RATE_APP = 259200000
private const val SP_KEY_LAST_TIME_NEED_UPDATE = "sp_key_last_time_need_update"

class SettingsManager(private val sharedPreferences: SharedPreferences) : SettingsManagerAPI {
    override fun countLaunch(): Int {
        return sharedPreferences.getInt(SP_KEY_LAUNCH_COUNT, 0)
    }

    override fun incrementCountLaunch() {
        val count = sharedPreferences.getInt(SP_KEY_LAUNCH_COUNT, 0)
        sharedPreferences.edit().apply {
            putInt(SP_KEY_LAUNCH_COUNT, count + 1)
            Log.e("Settings", "count: ${count + 1}")
        }.apply()
    }

    override fun needShowRateApp(successDays: Int): Boolean {
        val lastShowDate = sharedPreferences.getLong(SP_KEY_NEED_SHOW_RATE_APP_LAST_DATE, 0L)
        val needShowSP = sharedPreferences.getBoolean(
            SP_KEY_NEED_SHOW_RATE_APP,
            true
        ) && (successDays + 1) % COUNT_NEED_RATE_APP == 0
        val needShow = needShowSP && (System.currentTimeMillis() - lastShowDate) > COUNT_NEED_TIME_RATE_APP
        if (needShow) {
            sharedPreferences.edit().apply {
                putLong(SP_KEY_NEED_SHOW_RATE_APP_LAST_DATE, System.currentTimeMillis())
            }.apply()
        }
        return needShow
    }


    override fun blockShowRateApp() {
        sharedPreferences.edit().apply {
            putBoolean(SP_KEY_NEED_SHOW_RATE_APP, false)
        }.apply()
    }

    override fun countShowRateApp(): Int {
        return sharedPreferences.getInt(SP_KEY_COUNT_SHOW_RATE_APP, 0)

    }

    override fun incrementCountShowRateApp() {
        val count = sharedPreferences.getInt(SP_KEY_COUNT_SHOW_RATE_APP, 0)
        sharedPreferences.edit().apply {
            putInt(SP_KEY_COUNT_SHOW_RATE_APP, count + 1)
        }.apply()
    }

    override fun isOnboardingShown(): Boolean {
        return sharedPreferences.getBoolean(SP_KEY_ONBOARDING_SHOW, false)

    }

    override fun setMarkOnboardingShow() {
        sharedPreferences.edit().apply {
            putBoolean(SP_KEY_ONBOARDING_SHOW, true)
        }.apply()
    }

    override fun isHintRVShown(): Boolean {
        return sharedPreferences.getBoolean(SP_KEY_HINT_RV_SHOW, false)
    }

    override fun setHintRVShow() {
        sharedPreferences.edit().apply {
            putBoolean(SP_KEY_HINT_RV_SHOW, true)
        }.apply()
    }

    override fun firstLaunchMeta() {
        val calendar = Calendar.getInstance()
        sharedPreferences.edit().apply {
            putInt(SP_KEY_START_VERSION, BuildConfig.VERSION_CODE)
            putInt(SP_KEY_FIRST_LAUNCH_DAY, calendar.get(Calendar.DAY_OF_YEAR))
            putInt(SP_KEY_FIRST_LAUNCH_WEEK, calendar.get(Calendar.WEEK_OF_YEAR))
            putInt(SP_KEY_FIRST_LAUNCH_MONTH, calendar.get(Calendar.MONTH) + 1)
        }.apply()
    }

    override fun startVersion(): Int {
        return sharedPreferences.getInt(SP_KEY_START_VERSION, 0)
    }

    override fun startUseAppDay(): Int {
        return sharedPreferences.getInt(SP_KEY_FIRST_LAUNCH_DAY, 0)
    }

    override fun startUseAppWeek(): Int {
        return sharedPreferences.getInt(SP_KEY_FIRST_LAUNCH_WEEK, 0)
    }

    override fun startUseAppMonth(): Int {
        return sharedPreferences.getInt(SP_KEY_FIRST_LAUNCH_MONTH, 0)
    }

    override fun timeShowedUpdateDialog(timestamp: Long) {
        sharedPreferences.edit().apply {
            putLong(SP_KEY_LAST_TIME_NEED_UPDATE, timestamp)
        }.apply()
    }

    override fun timeShowedUpdateDialog(): Long {
        return sharedPreferences.getLong(SP_KEY_LAST_TIME_NEED_UPDATE, 0)
    }
}