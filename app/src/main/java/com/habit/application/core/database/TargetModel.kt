package com.habit.application.core.database

import android.os.Parcel
import android.os.Parcelable


/**
 * Created by Roman K. on 28.01.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
data class TargetModel(
    val id: Long,
    val name: String,
    val completedDays: Int,
    val needDays: Int,
    val status: Status,
    val isCheckToday: Boolean,
    var hourNotification: Int?,
    var minuteNotification: Int?
): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readString().toString(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt().let { if (it >= 0) enumValues<Status>()[it] else Status.ERROR},
        parcel.readByte() != 0.toByte(),
        parcel.readInt(),
        parcel.readInt()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeString(name)
        parcel.writeInt(completedDays)
        parcel.writeInt(needDays)
        parcel.writeInt(status.ordinal)
        parcel.writeByte(if (isCheckToday) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TargetModel> {
        override fun createFromParcel(parcel: Parcel): TargetModel {
            return TargetModel(parcel)
        }

        override fun newArray(size: Int): Array<TargetModel?> {
            return arrayOfNulls(size)
        }
    }
}

enum class Status{
    ACTIVE, ERROR, COMPLETED
}