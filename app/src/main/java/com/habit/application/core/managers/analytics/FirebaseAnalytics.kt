package com.habit.application.core.managers.analytics

import android.app.Activity
import android.content.Context
import com.google.firebase.analytics.FirebaseAnalytics


/**
 * Created by Roman K. on 07.03.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
class FirebaseAnalytics(
    private val context: Context
) : AnalyticsManagerAPI {
    override fun setUserProperty(property: AnalyticsManagerAPI.AnalyticsUserProperty, value: Any) {
        firebaseAnalytics.setUserProperty(
            property.value,
            value.toString()
        )
    }

    override fun logEvent(event: AnalyticsManagerAPI.AnalyticsEvent, extraLineEvent: String?, bundle: List<String>?) {
        firebaseAnalytics.logEvent(extraLineEvent?.let { event.value + it } ?: kotlin.run { event.value }, null)
    }

    override fun currentScreen(activity: Activity, screenName: String) {
        firebaseAnalytics.setCurrentScreen(activity, screenName, screenName)
    }


    private val firebaseAnalytics by lazy {
        FirebaseAnalytics.getInstance(context)
    }
}