package com.habit.application.core.extensions

import android.content.res.Resources


/**
 * Created by Roman K. on 26.01.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */



/**
 * Convert PX to DP
 */
private val DP = Resources.getSystem().displayMetrics.density

fun Float.toDP(): Float {
    return DP * this
}