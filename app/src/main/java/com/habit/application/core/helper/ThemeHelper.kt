package com.habit.application.core.helper

import android.app.Activity
import androidx.annotation.BoolRes
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import com.gyf.immersionbar.ktx.immersionBar
import com.habit.application.R


/**
 * Created by Roman K. on 25.01.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
class ThemeHelper(private val activity: Activity) : ThemeHelperAPI {
    override fun themeMain() {
        activity.immersionBar {
//            statusBarColor("#A6FFFFFF")
            transparentStatusBar()
            statusBarDarkFont(getBoolean(R.bool.theme_main_status_dark_font))
            transparentNavigationBar()
            navigationBarDarkIcon(getBoolean(R.bool.theme_main_navbar_dark_icon))
            fitsSystemWindows(false)
            init()
        }
    }

    override fun themeMainBottomSheetOpen() {
        activity.immersionBar {
//            statusBarColor("#03000000")
            transparentStatusBar()
            statusBarDarkFont(getBoolean(R.bool.theme_main_bs_open_dark_font))
            transparentNavigationBar()
            navigationBarDarkIcon(getBoolean(R.bool.theme_main_bs_open_navbar_dark_icon))
            fitsSystemWindows(false)
            init()
        }
    }

    override fun whiteBars() {
        activity.immersionBar {
            statusBarColor(R.color.statusBar)
            statusBarDarkFont(getBoolean(R.bool.theme_main_status_dark_font))
            transparentNavigationBar()
            navigationBarDarkIcon(getBoolean(R.bool.white_bars_navbar_dark_icon))
            fitsSystemWindows(false)
            init()
        }
    }

    private fun getBoolean(@BoolRes idBool: Int): Boolean{
        return activity.resources.getBoolean(idBool)
    }
}