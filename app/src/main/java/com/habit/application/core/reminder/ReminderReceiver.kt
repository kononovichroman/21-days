package com.habit.application.core.reminder

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast
import com.habit.application.core.database.Status
import com.habit.application.core.database.TargetModel
import com.habit.application.core.database.repository.TargetRepositoryAPI
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.koin.core.KoinComponent
import org.koin.core.inject


/**
 * Created by Roman K. on 01.05.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
const val ACTION_SHOW_REMINDER = "com.habit.application.ACTION_SHOW_REMINDER"

class ReminderReceiver : BroadcastReceiver(), KoinComponent {
    private val reminderController: ReminderControllerAPI by inject()
    private val reminderScheduler: ReminderSchedulerAPI by inject()
    private val repository: TargetRepositoryAPI by inject()

    override fun onReceive(context: Context, intent: Intent?) {
        if (intent?.action.equals(Intent.ACTION_BOOT_COMPLETED)) {
//            Log.e("HABIT_TAG", "ACTION_BOOT_COMPLETED")
            reminderController.onBootCompleted()
            return
        }

        var target: TargetModel?

        val targetID = intent?.extras?.getLong(REMINDER_RECEIVER_TARGET_ID, -1L) ?: -1L
        if (targetID != -1L) {
            Log.e("HABIT_TG", "$REMINDER_RECEIVER_TARGET_ID : $targetID")
            GlobalScope.launch {
                target = repository.targetById(targetID)
                try {
                    when (intent?.action) {
                        ACTION_SHOW_REMINDER -> {
//                            Log.e("HABIT_TAG", "ACTION_SHOW_REMINDER: ${target.toString()}")
                            target?.let {
                                if (it.status == Status.ACTIVE && !it.isCheckToday) {
                                    reminderController.onShowReminder(it)
                                }
                            }
                        }
                        else -> {

                        }
                    }
                } catch (e: RuntimeException) {
                    Log.e("HABIT_TAG", "could not process intent", e);
                }

            }
        }


    }
}