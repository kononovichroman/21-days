package com.habit.application.core.reminder

import com.habit.application.core.database.TargetModel


/**
 * Created by Roman K. on 04.05.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
interface ReminderSchedulerAPI {
    fun schedule(target: TargetModel)
    fun scheduleAll()
    fun hideNotification(idTarget: Int)

}