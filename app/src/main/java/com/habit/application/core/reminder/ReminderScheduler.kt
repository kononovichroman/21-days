package com.habit.application.core.reminder

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.util.Log
import com.habit.application.core.database.TargetModel
import com.habit.application.core.database.repository.TargetRepositoryAPI
import com.habit.application.core.reminder.notifications.NotificationControllerAPI
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.util.*


/**
 * Created by Roman K. on 04.05.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
private const val ONE_DAY = 24 * 60 * 60 * 1000L
const val REMINDER_RECEIVER_TARGET_ID = "reminder_receiver_target_id"
class ReminderScheduler(private val context: Context): ReminderSchedulerAPI, KoinComponent {
    private val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
    private val repository: TargetRepositoryAPI by inject()
    private val notificationController: NotificationControllerAPI by inject()

    @Synchronized
    override fun schedule(target: TargetModel) {
//        Log.e("Habit_TAG", "schedule TOP")

        target.hourNotification?.let { hourNotif ->
            target.minuteNotification?.let { minuteNotif ->
                val nowCalendar = GregorianCalendar.getInstance()

                val notifCalendar = GregorianCalendar.getInstance()
//                Log.e("Habit_TAG", "timeNotificationsSelect: ${hourNotif} : $minuteNotif")
                notifCalendar.apply {
                    set(Calendar.HOUR_OF_DAY, hourNotif)
                    set(Calendar.MINUTE, minuteNotif)
                    set(Calendar.SECOND, 0)
                    set(Calendar.MILLISECOND, 0)
                }
                val needTime = if (notifCalendar.timeInMillis < nowCalendar.timeInMillis) {
                    notifCalendar.timeInMillis + ONE_DAY
                } else {
                    notifCalendar.timeInMillis
                }
                val intent = Intent(context, ReminderReceiver::class.java)
                intent.action = ACTION_SHOW_REMINDER
                intent.putExtra(REMINDER_RECEIVER_TARGET_ID, target.id)
                val pendingIntent = PendingIntent.getBroadcast(context, target.id.toInt(), intent, 0)
//                Log.e("Habit_TAG", "needTime: $needTime")
                alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, needTime, pendingIntent)
            }
        }

    }

    @Synchronized
    override fun scheduleAll() {
        GlobalScope.launch {
            val listTargets = repository.allTargets()
            listTargets.forEach {
                schedule(it)
            }
        }
    }

    override fun hideNotification(idTarget: Int) {
        notificationController.hideNotification(idTarget)
    }
}