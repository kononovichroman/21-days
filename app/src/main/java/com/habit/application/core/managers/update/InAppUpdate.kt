package com.habit.application.core.managers.update

import androidx.appcompat.app.AppCompatActivity
import com.google.android.play.core.appupdate.AppUpdateInfo
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.InstallStateUpdatedListener
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.InstallStatus
import com.google.android.play.core.install.model.UpdateAvailability
import com.habit.application.core.settings.SettingsManagerAPI


/**
 * Created by Roman K. on 22.05.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */

const val IN_APP_UPDATE_REQUEST_CODE = 8642
//private const val TIME_NEED_SHOW_DIALOG = 24
private const val TIME_NEED_SHOW_DIALOG = 1000L * 60 * 60 * 24

class InAppUpdate(
    private val activity: AppCompatActivity,
    private val settingsManager: SettingsManagerAPI
) : InAppUpdateAPI {
    override fun installUpdate() {
        appUpdateManager.completeUpdate()
        appUpdateManager.unregisterListener(listener)

    }

    override fun onStop() {
        appUpdateManager.unregisterListener(listener)
    }

    override var successfullyDownloaded: () -> Unit = {}


    private lateinit var appUpdateManager: AppUpdateManager
    private var listener: InstallStateUpdatedListener? = null
    override fun checkUpdate() {
        listener = InstallStateUpdatedListener { state ->
            if (state.installStatus() == InstallStatus.DOWNLOADED) {
                successfullyDownloaded.invoke()
            }
        }
        appUpdateManager = AppUpdateManagerFactory.create(activity)
        appUpdateManager.appUpdateInfo.addOnSuccessListener { appUpdateInfo ->
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE) {
                showDialogUpdate(appUpdateInfo)
            }
            if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                successfullyDownloaded.invoke()
                onStop()
            }
        }

    }

    private fun markShowedUpdate() {
        settingsManager.timeShowedUpdateDialog(System.currentTimeMillis())
    }

    private fun showDialogUpdate(appUpdateInfo: AppUpdateInfo) {
        if (System.currentTimeMillis() - getLastShowUpdate() > TIME_NEED_SHOW_DIALOG) {
            appUpdateManager.registerListener(listener)
            appUpdateManager.startUpdateFlowForResult(
                appUpdateInfo,
                AppUpdateType.FLEXIBLE,
                activity,
                IN_APP_UPDATE_REQUEST_CODE
            )
            markShowedUpdate()
        }
    }

    private fun getLastShowUpdate(): Long {
        return settingsManager.timeShowedUpdateDialog()
    }

}