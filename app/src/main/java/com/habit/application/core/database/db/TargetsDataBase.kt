package com.habit.application.core.database.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.habit.application.core.database.entity.TargetEntity
import com.habit.application.core.database.dao.TargetDao


/**
 * Created by Roman K. on 28.01.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */

@Database(entities = [TargetEntity::class], version = 2, exportSchema = false)
abstract class TargetsDataBase: RoomDatabase() {

    abstract fun targetDao(): TargetDao

    companion object {
        private val MIGRATION_1_2 = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE targets_table ADD COLUMN hourNotification INTEGER")
                database.execSQL("ALTER TABLE targets_table ADD COLUMN minuteNotification INTEGER")
            }
        }
        @Volatile
        private var INSTANCE: TargetsDataBase? = null

        fun getDB(context: Context): TargetsDataBase {
            val tempInstance = INSTANCE


            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(context.applicationContext, TargetsDataBase::class.java, "target_db")
                    .addMigrations(MIGRATION_1_2).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}