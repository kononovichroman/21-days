package com.habit.application.core.database.converter

import androidx.room.TypeConverter
import com.habit.application.core.database.Status


/**
 * Created by Roman K. on 28.01.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
class StatusTargetConverter {

    @TypeConverter
    fun fromStatus(status: Status): Int{
        return status.ordinal
    }

    @TypeConverter
    fun toStatus(value: Int): Status{
        return when(value){
            0 -> Status.ACTIVE
            1 -> Status.ERROR
            2 -> Status.COMPLETED
            else -> {
                Status.ERROR
            }
        }
    }


}