package com.habit.application.core.managers.analytics

import android.app.Activity
import org.koin.core.KoinComponent
import org.koin.core.inject


/**
 * Created by Roman K. on 07.03.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
class AnalyticsManager : AnalyticsManagerAPI, KoinComponent {
    private val firebaseAnalytics: FirebaseAnalytics by inject()
    private val analytics = arrayOf(firebaseAnalytics)

    override fun setUserProperty(property: AnalyticsManagerAPI.AnalyticsUserProperty, value: Any) {
        analytics.forEach {
            it.setUserProperty(property, value)
        }
    }

    override fun logEvent(event: AnalyticsManagerAPI.AnalyticsEvent, extraLineEvent: String?, bundle: List<String>?) {
        analytics.forEach {
            it.logEvent(event, extraLineEvent, bundle)
        }
    }

    override fun currentScreen(activity: Activity, screenName: String) {
        analytics.forEach {
            it.currentScreen(activity, screenName)
        }
    }


}