package com.habit.application.core.reminder


/**
 * Created by Roman K. on 02.05.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
data class TimeModel(
    val hour: Int,
    val minute: Int,
    val type: TimeType
) {
    override fun toString(): String {
        return when (type) {
            TimeType.TIME_TYPE_PM -> {
                "$hour:${String.format("%02d", minute)} PM"
            }
            TimeType.TIME_TYPE_AM -> {
                "$hour:${String.format("%02d", minute)} AM"
            }
            else -> {
                "${String.format("%02d", hour)}:${String.format("%02d", minute)}"
            }
        }
    }

    fun convertAmPmTo24(): TimeModel {
        val h: Int = when (type) {
            TimeType.TIME_TYPE_PM -> {
                if (hour < 12) {
                    hour + 12
                } else {
                    hour
                }
            }
            TimeType.TIME_TYPE_AM -> {
                if (hour < 12) {
                    hour
                } else {
                    0
                }
            }
            else -> {
                hour
            }
        }
        return TimeModel(h, minute, TimeType.TIME_TYPE_24)
    }

    fun convert24toAmPm() :TimeModel{
        val h: Int = when (type) {
            TimeType.TIME_TYPE_24 -> {
                when {
                    hour in 1..12 -> {
                        hour
                    }
                    hour > 12 -> {
                        hour - 12
                    }
                    else -> {
                        12
                    }
                }
            }
            else -> {
                hour
            }
        }
        val t: TimeType = when(type){
            TimeType.TIME_TYPE_24 -> {
                when (hour) {
                    in 0..11 -> {
                        TimeType.TIME_TYPE_AM
                    }
                    else -> {
                        TimeType.TIME_TYPE_PM
                    }
                }
            }
            else -> {
                type
            }
        }
        return TimeModel(h, minute, t)

    }
}

enum class TimeType { TIME_TYPE_24, TIME_TYPE_AM, TIME_TYPE_PM }