package com.habit.application.core.reminder

import com.habit.application.core.database.TargetModel


/**
 * Created by Roman K. on 01.05.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
interface ReminderControllerAPI {
    fun onShowReminder(target: TargetModel)
    fun onBootCompleted()
}