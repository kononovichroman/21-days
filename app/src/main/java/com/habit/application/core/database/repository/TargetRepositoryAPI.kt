package com.habit.application.core.database.repository

import com.habit.application.core.database.TargetModel
import com.habit.application.core.database.entity.TargetEntity
import com.habit.application.core.reminder.TimeModel


/**
 * Created by Roman K. on 28.01.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
interface TargetRepositoryAPI {
    suspend fun allTargets(): List<TargetModel>
    suspend fun targetById(id: Long): TargetModel?
    suspend fun insert(target: TargetEntity): Long
    suspend fun checkNewDay(id: Long)
    suspend fun setNotification(id: Long, timeModel: TimeModel)
    suspend fun setError(id: Long)
    suspend fun delete(id: Long)
    suspend fun repeat(id: Long)

    var insertNewTarget : (target: TargetModel) -> Unit

}