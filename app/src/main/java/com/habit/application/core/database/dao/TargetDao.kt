package com.habit.application.core.database.dao

import androidx.room.*
import com.habit.application.core.database.entity.TargetEntity


/**
 * Created by Roman K. on 28.01.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */

@Dao
interface TargetDao {

    @Query("SELECT * from targets_table ORDER BY id DESC")
    suspend fun getAllTargets(): List<TargetEntity>

    @Query("SELECT * from targets_table WHERE id=:id")
    suspend fun getTargetById(id: Long): TargetEntity?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(target: TargetEntity): Long

    @Delete
    suspend fun delete(target: TargetEntity)


}