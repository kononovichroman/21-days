package com.habit.application.core.settings


/**
 * Created by Roman K. on 09.02.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
interface SettingsManagerAPI {
    fun countLaunch(): Int
    fun incrementCountLaunch()
    fun needShowRateApp(successDays: Int): Boolean
    fun blockShowRateApp()
    fun countShowRateApp(): Int
    fun incrementCountShowRateApp()

    fun isOnboardingShown(): Boolean
    fun setMarkOnboardingShow()

    fun isHintRVShown(): Boolean
    fun setHintRVShow()

    fun firstLaunchMeta()
    fun startVersion(): Int
    fun startUseAppDay(): Int
    fun startUseAppWeek(): Int
    fun startUseAppMonth(): Int

    fun timeShowedUpdateDialog(timestamp: Long)
    fun timeShowedUpdateDialog(): Long
}