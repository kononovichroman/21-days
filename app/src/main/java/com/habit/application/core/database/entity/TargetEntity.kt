package com.habit.application.core.database.entity

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.habit.application.core.database.Status
import com.habit.application.core.database.converter.StatusTargetConverter


/**
 * Created by Roman K. on 28.01.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */

@Entity(tableName = "targets_table")
@TypeConverters(StatusTargetConverter::class)

data class TargetEntity (
    @PrimaryKey(autoGenerate = true)
    val id: Long? = null,
    @NonNull
    var name: String,
    @NonNull
    var needDays: Int = 21,
    @NonNull
    var successDays: Int,
    @NonNull
    var startTime: Long,
    @NonNull
    var lastCheckTime: Long,

    var hourNotification : Int?,

    var minuteNotification : Int?,

    var status: Status = Status.ACTIVE
)

