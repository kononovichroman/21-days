package com.habit.application.core.extensions

import androidx.databinding.Observable


/**
 * Created by Roman K. on 27.01.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
fun <T : Observable> T.addOnPropertyChanged(callback: (T) -> Unit) =
    addOnPropertyChangedCallback(
        object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(
                observable: Observable?, i: Int
            ) =
                callback(observable as T)
        })
