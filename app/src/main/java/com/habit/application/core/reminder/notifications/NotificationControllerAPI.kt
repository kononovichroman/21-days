package com.habit.application.core.reminder.notifications

import com.habit.application.core.database.TargetModel


/**
 * Created by Roman K. on 05.05.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
interface NotificationControllerAPI {
     fun showNotification(target: TargetModel)
     fun hideNotification(idTarget: Int)
}