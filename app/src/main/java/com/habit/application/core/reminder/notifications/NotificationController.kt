package com.habit.application.core.reminder.notifications

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.app.TaskStackBuilder
import androidx.core.content.ContextCompat
import com.habit.application.R
import com.habit.application.core.database.TargetModel
import com.habit.application.core.reminder.REMINDER_RECEIVER_TARGET_ID
import com.habit.application.screen.RootActivity
import com.habit.application.screen.detail.DETAIL_SCREEN_TARGET


/**
 * Created by Roman K. on 05.05.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
const val NOTIFICATION_CHANNEL_ID = "notify"

class NotificationController(private val context: Context) :
    NotificationControllerAPI {

    private val notificationManager = NotificationManagerCompat.from(context)

    override fun showNotification(target: TargetModel) {
        val openIntent = Intent(context, RootActivity::class.java)
        val bundle = Bundle().apply {
            putParcelable(DETAIL_SCREEN_TARGET, target)
        }

        openIntent.putExtra(REMINDER_RECEIVER_TARGET_ID, bundle)
        val openPendingIntent = TaskStackBuilder.create(context).run {
            addNextIntentWithParentStack(openIntent)
            getPendingIntent(target.id.toInt(), PendingIntent.FLAG_CANCEL_CURRENT)
        }
        val builder = NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_notification_reminder)
            .setContentTitle(target.name)
            .setContentText(context.resources.getString(R.string.notification_description))
            .setContentIntent(openPendingIntent)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
            .addAction(
                R.drawable.ic_notification_action_open,
                context.resources.getString(R.string.notification_action_btn),
                openPendingIntent
            )

        Log.e("HABIT_TAG", "showNotification id: ${target.id}")

        notificationManager.notify(target.id.toInt(), builder.build())
    }

    override fun hideNotification(idTarget: Int) {
        notificationManager.cancel(idTarget)
    }
}