package com.habit.application.core.helper


/**
 * Created by Roman K. on 25.01.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
interface ThemeHelperAPI {
    fun themeMain()
    fun themeMainBottomSheetOpen()
    fun whiteBars()
}