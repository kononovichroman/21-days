package com.habit.application.core.di

import android.app.Application
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import com.habit.application.core.database.db.TargetsDataBase
import com.habit.application.core.database.repository.TargetRepository
import com.habit.application.core.database.repository.TargetRepositoryAPI
import com.habit.application.core.helper.ThemeHelper
import com.habit.application.core.managers.analytics.AnalyticsManager
import com.habit.application.core.managers.analytics.AnalyticsManagerAPI
import com.habit.application.core.managers.analytics.FirebaseAnalytics
import com.habit.application.core.managers.update.InAppUpdate
import com.habit.application.core.reminder.ReminderController
import com.habit.application.core.reminder.ReminderControllerAPI
import com.habit.application.core.reminder.ReminderScheduler
import com.habit.application.core.reminder.ReminderSchedulerAPI
import com.habit.application.core.reminder.notifications.NotificationController
import com.habit.application.core.reminder.notifications.NotificationControllerAPI
import com.habit.application.core.settings.SettingsManager
import com.habit.application.core.settings.SettingsManagerAPI
import com.habit.application.screen.RootViewModel
import com.habit.application.screen.detail.DetailViewModel
import com.habit.application.screen.main.MainViewModel
import com.habit.application.screen.Router
import com.habit.application.screen.base.BaseFragment
import com.habit.application.screen.onboarding.OnboardingViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


/**
 * Created by Roman K. on 25.01.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */

val root = module {

    viewModel { (activity: AppCompatActivity) ->
        RootViewModel(
            get(),
            get(),
            Router(activity, null, get()),
            InAppUpdate(activity,get()),
            get()
        )
    }
}
val main = module {

    viewModel { (activity: AppCompatActivity, fragment : BaseFragment) ->
        MainViewModel(
            get(),
            ThemeHelper(activity),
            Router(activity, fragment, get()),
            get(),
            get(),
            get(),
            get()
        )
    }
}
val onboarding = module {

    viewModel { (activity: AppCompatActivity, fragment : BaseFragment) ->
        OnboardingViewModel(
            get(),
            Router(activity, fragment, get()),
            ThemeHelper(activity),
            get(),
            get()
        )
    }
}
val detail = module {

    viewModel { (activity: AppCompatActivity, fragment : BaseFragment) ->
        DetailViewModel(
            get(),
            ThemeHelper(activity),
            Router(activity, fragment, get()),
            get(),
            get(),
            get()
        )
    }
}

val repository = module {
    single { TargetsDataBase.getDB(get()) }
    single { get<TargetsDataBase>().targetDao() }
    single<TargetRepositoryAPI> { TargetRepository(get()) }
}

val analyticsManager = module {
    single<AnalyticsManagerAPI> { AnalyticsManager() }
}

val firebaseAnalytics = module {
    single { FirebaseAnalytics(get()) }
}
val reminderController = module {
    single<ReminderControllerAPI> { ReminderController(get()) }
}
val reminderScheduler = module {
    single<ReminderSchedulerAPI> { ReminderScheduler(get()) }
}
val notificationController = module {
    single<NotificationControllerAPI> {NotificationController(get())}
}

private fun provideSharedPreferences(app: Application): SharedPreferences {
    return EncryptedSharedPreferences.create(
        SP_FILE_NAME, MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC),
        app, EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
        EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
    )
}

val sharedPreferences = module {
    single { provideSharedPreferences(get()) }
}

val settingsManager = module {
    single<SettingsManagerAPI> { SettingsManager(get()) }
}

const val SP_FILE_NAME = "settings"

val modulesApp = listOf(main, detail, repository, sharedPreferences, settingsManager, root, onboarding, analyticsManager, firebaseAnalytics, reminderController, reminderScheduler, notificationController)