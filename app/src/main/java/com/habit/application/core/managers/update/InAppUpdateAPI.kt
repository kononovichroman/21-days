package com.habit.application.core.managers.update


/**
 * Created by Roman K. on 22.05.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
interface InAppUpdateAPI {
    fun checkUpdate()
    fun installUpdate()
    fun onStop()
    var successfullyDownloaded : () -> Unit
}