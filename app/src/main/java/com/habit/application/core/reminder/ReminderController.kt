package com.habit.application.core.reminder

import android.content.Context
import com.habit.application.core.database.TargetModel
import com.habit.application.core.reminder.notifications.NotificationControllerAPI
import org.koin.core.KoinComponent
import org.koin.core.inject


/**
 * Created by Roman K. on 01.05.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
class ReminderController(private val context: Context): ReminderControllerAPI, KoinComponent {
    private val reminderScheduler: ReminderSchedulerAPI by inject()
    private val notificationController: NotificationControllerAPI by inject()

    override fun onShowReminder(target: TargetModel) {
        notificationController.showNotification(target)
        reminderScheduler.scheduleAll()
    }

    override fun onBootCompleted() {
        reminderScheduler.scheduleAll()
    }


}