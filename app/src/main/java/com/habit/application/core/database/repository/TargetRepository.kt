package com.habit.application.core.database.repository

import android.util.Log
import com.habit.application.core.database.Status
import com.habit.application.core.database.TargetModel
import com.habit.application.core.database.entity.TargetEntity
import com.habit.application.core.database.dao.TargetDao
import com.habit.application.core.reminder.TimeModel
import com.habit.application.core.reminder.TimeType
import java.util.*
import kotlin.collections.ArrayList


/**
 * Created by Roman K. on 28.01.2020.
 * Copyright (c) 2020 ROMAN KONONOVICH. All rights reserved.
 */
class TargetRepository(private val targetDao: TargetDao): TargetRepositoryAPI {
    private val rawTarget = ArrayList<TargetEntity>()

    @Synchronized
    override suspend fun allTargets(): List<TargetModel> {
        val listTarget = ArrayList<TargetModel>()
        rawTarget.apply {
            this.clear()
            this.addAll(targetDao.getAllTargets())
        }
        Log.e("TEST", rawTarget.toString())
        rawTarget.filter { it.status != Status.COMPLETED}.forEach {targetEntity ->
            if (targetEntity.status == Status.ACTIVE){
                val dateInTarget = Calendar.getInstance()
                dateInTarget.timeInMillis = targetEntity.lastCheckTime
                val countDaysInTarget = dateInTarget.get(Calendar.DAY_OF_YEAR)

                val calendar = Calendar.getInstance()
                val countRealDays = calendar.get(Calendar.DAY_OF_YEAR)

                if (countRealDays - countDaysInTarget > 1 && targetEntity.successDays > 0){
                    targetEntity.id?.let {
                        listTarget.add(TargetModel(it, targetEntity.name, targetEntity.successDays, targetEntity.needDays, Status.ERROR, false, targetEntity.hourNotification, targetEntity.minuteNotification))
                        setError(it)
                    }
                } else {
                    targetEntity.id?.let {
                        listTarget.add(TargetModel(it, targetEntity.name, targetEntity.successDays, targetEntity.needDays, targetEntity.status, countRealDays == countDaysInTarget && targetEntity.successDays > 0, targetEntity.hourNotification, targetEntity.minuteNotification))
                    }
                }
            } else if (targetEntity.status == Status.ERROR) {
                targetEntity.id?.let {
                    listTarget.add(TargetModel(it, targetEntity.name, targetEntity.successDays, targetEntity.needDays, targetEntity.status, false, targetEntity.hourNotification, targetEntity.minuteNotification))
                }
            }
        }

        rawTarget.filter {  it.status == Status.COMPLETED }.forEach {targetEntity ->
            targetEntity.id?.let {
                listTarget.add(TargetModel(it, targetEntity.name, targetEntity.successDays, targetEntity.needDays, targetEntity.status, true, targetEntity.hourNotification, targetEntity.minuteNotification))
            }
        }
        
        return listTarget
    }

    @Synchronized
    override suspend fun targetById(id: Long): TargetModel? {
        targetDao.getTargetById(id)?.let {
            if (it.status == Status.ACTIVE){
                val dateInTarget = Calendar.getInstance()
                dateInTarget.timeInMillis = it.lastCheckTime
                val countDaysInTarget = dateInTarget.get(Calendar.DAY_OF_YEAR)

                val calendar = Calendar.getInstance()
                val countRealDays = calendar.get(Calendar.DAY_OF_YEAR)
                return TargetModel(id,it.name, it.successDays,it.needDays,it.status, countRealDays == countDaysInTarget && it.successDays > 0, it.hourNotification, it.minuteNotification)

            } else {
                return TargetModel(id,it.name, it.successDays,it.needDays,it.status, it.status == Status.COMPLETED, it.hourNotification, it.minuteNotification)
            }
        }
        return null
    }

    @Synchronized
    override suspend fun insert(target: TargetEntity): Long{
        val id = targetDao.insert(target)
        targetById(id)?.let { insertNewTarget.invoke(it) }
        return id
    }

    @Synchronized
    override suspend fun checkNewDay(id: Long) {
        rawTarget.find { it.id == id }?.let {
            if (it.needDays - it.successDays == 1){
                it.status = Status.COMPLETED
            }
            it.successDays = ++it.successDays
            it.lastCheckTime = System.currentTimeMillis()
            targetDao.insert(it)
        }
    }

    @Synchronized
    override suspend fun setNotification(id: Long, timeModel: TimeModel) {
        rawTarget.find { it.id == id }?.let {
            it.hourNotification = if (timeModel.type != TimeType.TIME_TYPE_24) {
                timeModel.convertAmPmTo24().hour
            } else {
                timeModel.hour
            }
            it.minuteNotification = timeModel.minute
            targetDao.insert(it)
        }
    }

    @Synchronized
    override suspend fun setError(id: Long) {
        rawTarget.find { it.id == id }?.let {
            it.status = Status.ERROR
            targetDao.insert(it)
        }
    }

    @Synchronized
    override suspend fun delete(id: Long) {
        rawTarget.find { it.id == id }?.let {
            targetDao.delete(it)
        }
    }

    @Synchronized
    override suspend fun repeat(id: Long) {
        rawTarget.find { it.id == id }?.let {
            it.successDays = 0
            it.startTime = System.currentTimeMillis()
            it.lastCheckTime = System.currentTimeMillis()
            it.status = Status.ACTIVE
            targetDao.insert(it)
        }
    }

    override var insertNewTarget: (target: TargetModel) -> Unit =  {}
}